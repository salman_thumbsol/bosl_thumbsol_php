<?php
require_once('../config.php');


if(!isset($_SESSION)){
	session_start();
}


$target_dir = '../uploads/'.$_SESSION['Company_ID']."/";
$cleanupTargetDir = true; // Remove old files
$maxFileAge = 5 * 3600; // Temp file age in seconds

// Create target dir
if (!file_exists($target_dir)) {
	@mkdir($target_dir);
}


$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
 
// Check if file already exists
if (file_exists($target_file)) {
   // echo "Sorry, file already exists.";
                header('location:'. $_SERVER["HTTP_REFERER"]. "&error=Sorry, file already exists.");

     $uploadOk = 0;
}
// Check file size
if ($_FILES["fileToUpload"]["size"] > 500000) {
   // echo "Sorry, your file is too large.";
            header('location:'. $_SERVER["HTTP_REFERER"]. "&error=Sorry, your file is too large.");


    $uploadOk = 0;
}
// Allow certain file formats
$allowed=array('jpg','jpeg','jpe','gif','png','bmp','tif','tiff','txt','csv','htm','html','mp3','wav','wma','pdf','rar','zip','doc','docx','xls','xla','xlw','docm','dotx','dotm','xlsm','xlsx','xlsb','ppt','pptx','pptm','ppsx','ppsm','potm','ods','odp','odt','odc','wp','wpd','eml','msg');
 
 if(!in_array($imageFileType , $allowed)){ 
    // echo "Sorry this file type is not allowed";
        header('location:'. $_SERVER["HTTP_REFERER"]. "&error=Sorry this file type is not allowed");

    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
   header('location:'. $_SERVER["HTTP_REFERER"]. "&error= Sorry, your file was not uploaded.");
  //  echo " Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
      //  echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
      
$newName= renameFile($_FILES["fileToUpload"]["name"]);


      $data["folder"]= $_GET["folder"];
      $data["System_ID"]= $bosl->checkId($_GET["id"]);
      $data["System_User"]= "client";
      $data["description"]= $_POST["description"]; //
      $data["File_Name"]= $newName;
      $data["company_ID"]= $_SESSION["Company_ID"];
      $data["datetime"] = time();
	  $data["user_id"] = $_SESSION['User_id'];
      $data["File_Name"]= $newName;
$db->insert($data,"uploads");
    	// redirect them back to where they came from
      header('location:'. $_SERVER["HTTP_REFERER"]);
    } else {
            header('location:'. $_SERVER["HTTP_REFERER"]. "&error= Sorry, there was an error uploading your file.");

       // echo " Sorry, there was an error uploading your file.";
    }
}

function renameFile($what){
	global $target_dir;
	$ext = explode(".",$what);
	$ext = end($ext);
	$name = $what."-".uniqid().".".$ext;


 //Unwanted:  {UPPERCASE} ; / ? : @ & = + $ , . ! ~ * ' ( )
    $name = strtolower($name);
    //Strip any unwanted characters
   $name = preg_replace("/[^a-z0-9_\s-.]/", "", $name);
    //Clean multiple dashes or whitespaces
    $name = preg_replace("/[\s-]+/", " ", $name);
    //Convert whitespaces and underscore to dash
   $name = preg_replace("/[\s_]/", "-", $name);
   
rename($target_dir."/". $_FILES["fileToUpload"]["name"], $target_dir."/".$name);
    return $name;



}