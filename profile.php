<!doctype html>

<!-- 
Rock Hammer by Andy Clarke
Version: 0.1
URL: http://stuffandnonsense.co.uk/projects/rock-hammer/
Apache License: v2.0. http://www.apache.org/licenses/LICENSE-2.0
-->
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html class="no-js lt-ie9 lt-ie8" lang="en"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html class="no-js lt-ie9" lang="en"><![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"><!--<![endif]-->

<head>
<title>Back Office Solution</title>

        <!-- Hammer reload -->
          <script>
            setInterval(function(){
              try {
                if(typeof ws != 'undefined' && ws.readyState == 1){return true;}
                ws = new WebSocket('ws://'+(location.host || 'localhost').split(':')[0]+':35353')
                ws.onopen = function(){ws.onclose = function(){document.location.reload()}}
                window.onbeforeunload = function() { ws = null }
                ws.onmessage = function(){
                  var links = document.getElementsByTagName('link');
                    for (var i = 0; i < links.length;i++) {
                    var link = links[i];
                    if (link.rel === 'stylesheet' && !link.href.match(/typekit/)) {
                      href = link.href.replace(/((&|\?)hammer=)[^&]+/,'');
                      link.href = href + (href.indexOf('?')>=0?'&':'?') + 'hammer='+(new Date().valueOf());
                    }
                  }
                }
              }catch(e){}
            }, 1000)
          </script>
        <!-- /Hammer reload -->
      
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="cleartype" content="on">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<meta name="description" content="">
<meta name="author" content="">

<!-- For all browsers -->
<link rel='stylesheet' href='js/tablesaw/dependencies/qunit.css'>
<link rel='stylesheet' href='js/tablesaw/stackonly/tablesaw.stackonly.css'>
<link rel='stylesheet' href='css/rock-hammer.css'>

<!-- JavaScript -->
<script src='js/vendor/head.load.min.js'></script>
<script src='js/vendor/modernizr-2.6.2-min.js'></script>
<script src='js/vendor/jquery-1.8.3-min.js'></script>
<script src='js/tablesaw/tablesaw.js'></script>
<script src='js/tablesaw/tablesaw-init.js'></script>

<!--[if (lt IE 9) & (!IEMobile)]>
<script src='js/vendor/selectivizr-min.js'></script>
<link rel='stylesheet' href='css/lte-ie8.css'>
<![endif]-->

<link rel="shortcut icon" href="img/favicon.ico">
<link rel="shortcut icon" href="img/favicon.png" sizes="32x32">
<!-- <link rel="apple-touch-icon-precomposed" href="img/l/apple-touch-icon-precomposed.png"> -->
<!-- <link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/m/apple-touch-icon-72x72-precomposed.png"> -->
<!-- <link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/h/apple-touch-icon-114x114-precomposed.png"> -->
<!-- <link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/h/apple-touch-icon-144x144-precomposed.png"> -->
<!--iOS -->
<!-- <meta name="apple-mobile-web-app-title" content="Rock Hammer"> -->
<!-- <meta name="viewport" content="initial-scale=1.0"> (Use if apple-mobile-web-app-capable below is set to yes) -->
<!-- <meta name="apple-mobile-web-app-capable" content="yes"> -->
<!-- <meta name="apple-mobile-web-app-status-bar-style" content="black"> -->
<!-- Startup images -->
<!-- <link rel="apple-touch-startup-image" href="img/startup/startup-320x460.png" media="screen and (max-device-width:320px)"> -->
<!-- <link rel="apple-touch-startup-image" href="img/startup/startup-640x920.png" media="(max-device-width:480px) and (-webkit-min-device-pixel-ratio:2)"> -->
<!-- <link rel="apple-touch-startup-image" href="img/startup/startup-640x1096.png" media="(max-device-width:548px) and (-webkit-min-device-pixel-ratio:2)"> -->
<!-- <link rel="apple-touch-startup-image" sizes="1024x748" href="img/startup/startup-1024x748.png" media="screen and (min-device-width:481px) and (max-device-width:1024px) and (orientation:landscape)"> -->
<!-- <link rel="apple-touch-startup-image" sizes="768x1004" href="img/startup/startup-768x1004.png" media="screen and (min-device-width:481px) and (max-device-width:1024px) and (orientation:portrait)"> -->
<!-- Windows 8 / RT -->
<meta name="msapplication-TileImage" content="img/h/apple-touch-icon-144x144-precomposed.png">
<meta name="msapplication-TileColor" content="#000">

</head>

<body class="default" id="bos">
<a href="#navigation-toggle" class="navigation-toggle">Menu</a>
<nav id="navigation-toggle" role="navigation">
    <div class="nav-section">
        <a href="/" class="logo" title="Back Office Solutions"><img src="img/logo-white.png" /></a>
        <ul>
        <li class='current-parent'><a class='current-parent' href="index.html""><ion-icon name="people"></ion-icon> BOS People</a></li>
        <li><a href="vault.html"><ion-icon name="key"></ion-icon> BOS Vault</a></li>
        <li><a href="#"><ion-icon name="list-box"></ion-icon> BOS Pipeline</a></li>
        <li><a href="#"><ion-icon name="calculator"></ion-icon> BOS Reconciler</a></li>
        <li class="dropdown"><a href="#" class="profile">Profile <ion-icon name="arrow-dropdown"></ion-icon></a>
			<ul class="dropdown-content">
				<li><a href="#" class="profile">Logout</a></li>
			</ul>
        </li>
        </ul>
    </div>
</nav>

<div class="container">

<!-- Next ============================== -->

<section>


<div class="l-wrap">
<div class="l-full-width">
	<div class="page-title">
		<h2>Profile</h2>
	</div>
		<form class="profile">

		
		<p><label for="title">Title</label><br />
		<input type="text" id="title" placeholder="Mr"></p>

		<p><label for="first-name">First Name</label><br />
		<input type="text" id="first-name" placeholder="David"></p>

		<p><label for="surname">Surname</label><br />
		<input type="text" id="surname" placeholder="Goffin"></p>

		<p><label for="time-zone">Time Zone</label><br />
		<select name="time-zone" id="time-zone">
			<option value="Zone 1">Zone 1</option>
			<option value="Zone 2">Zone 2</option>
			<option value="Zone 3">Zone 3</option>
			<option value="Zone 4">Zone 4</option>
			<option value="Zone 5">Zone 5</option>
			<option value="Zone 6">Zone 6</option>
			<option value="Zone 7">Zone 7</option>
			<option value="Zone 8">Zone 8</option>
		</select></p>

		<p><label for="Date Of Birth">Date Of Birth</label><br />
			<div class="dob">
				<select name="day" id="day">
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
					<option value="6">6</option>
					<option value="7">7</option>
					<option value="8">8</option>
					<option value="9">9</option>
					<option value="10">10</option>
					<option value="11">11</option>
					<option value="12">12</option>
					<option value="13">13</option>
					<option value="14">14</option>
					<option value="15">15</option>
					<option value="16">16</option>
					<option value="17">17</option>
					<option value="18">18</option>
					<option value="19">19</option>
					<option value="20">20</option>
					<option value="21">21</option>
					<option value="22">22</option>
					<option value="23">23</option>
					<option value="24">24</option>
					<option value="25">25</option>
					<option value="26">26</option>
					<option value="27">27</option>
					<option value="28">28</option>
					<option value="29">29</option>
					<option value="30">30</option>
					<option value="31">31</option>
				</select>
				<select name="month" id="month">
					<option value="January">January</option>
					<option value="February">February</option>
					<option value="March">March</option>
					<option value="April">April</option>
					<option value="May">May</option>
					<option value="June">June</option>
					<option value="July">July</option>
					<option value="August">August</option>
					<option value="September">September</option>
					<option value="November">November</option>
					<option value="October">October</option>
					<option value="December">December</option>
				</select>
				<select name="year" id="year">
					<option value="1950">1950</option>
					<option value="1951">1951</option>
					<option value="1952">1952</option>
					<option value="1953">1953</option>
					<option value="1954">1954</option>
					<option value="1955">1955</option>
					<option value="1956">1956</option>
					<option value="1957">1957</option>
				</select>
			</div>

		<p><label for="address">Address</code></label><br>
		<textarea id="address" placeholder="Address"></textarea>

		<p><label for="postcode">Postcode</label><br />
		<input type="text" id="postcode" placeholder="Postcode"></p>

		<p><label for="email-primary">Email (Primary)</label><br />
		<input type="email" id="email-primary" placeholder="Email (Primary)"></p>

		<p><label for="email-secondary">Email (Secondary)</label><br />
		<input type="email" id="email-secondary" placeholder="Email (Secondary)"></p>

		<p><label for="tel">Telephone</label><br />
		<input type="tel" id="tel" placeholder="Telephone"></p>

		<p><label for="tel">Mobile</label><br />
		<input type="tel" id="tel" placeholder="Mobile"></p>

		<p><label for="Country">Country</label><br />
			<div class="country">
				<select name="country" id="country">
					<option>Please Select</option>
					<option value="AFG">Afghanistan</option>
					<option value="ALA">Åland Islands</option>
					<option value="ALB">Albania</option>
					<option value="DZA">Algeria</option>
					<option value="ASM">American Samoa</option>
				</select>
			</div>

		<div class="folder-names">
			<p><label for="folder-1">Folder 1</label><br /><input id="folder-1" type="text" class="folder-1" placeholder="Folder 1 Name"></p>
			<p><label for="folder-2">Folder 2</label><br /><input id="folder-2" type="text" class="folder-2" placeholder="Folder 2 Name"></p>
			<p><label for="folder-3">Folder 3</label><br /><input id="folder-3" type="text" class="folder-3" placeholder="Folder 3 Name"></p>
			<p><label for="folder-4">Folder 4</label><br /><input id="folder-4" type="text" class="folder-4" placeholder="Folder 4 Name"></p>
			<p><label for="folder-5">Folder 5</label><br /><input id="folder-5" type="text" class="folder-5" placeholder="Folder 5 Name"></p>
		</div>

		<ul><li><label class="checkbox"><input type="checkbox"> I’ve read and agree to the <a href="#cookie-policy" data-toggle="modal">Cookie Policy</a>, <a href="#terms-conditions" data-toggle="modal">Terms & Conditions</a>, <a href="#privacy-policy" data-toggle="modal">Privacy Policy</a> and <a href="#security-policy" data-toggle="modal">Security Policy</a>.</label></li></ul>

		<button class="btn btn-primary">Save Profile</button>

		<!-- Cookie Policy Modal -->
			<div id="cookie-policy" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="cookie-policy" aria-hidden="true">
			<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			<h3 id="modal-title">Cookie Policy</h3>
			</div>
			<div class="modal-body">
			<p>Cookie policy text...</p>

			</div>
			<div class="modal-footer">
			<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
			</div>
		</div> <!-- / Cookie Policy Modal -->
		<!-- Terms & Conditions Modal -->
			<div id="terms-conditions" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="terms-conditions" aria-hidden="true">
			<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			<h3 id="modal-title">Terms & Conditions</h3>
			</div>
			<div class="modal-body">
			<p>Terms & Conditions text...</p>

			</div>
			<div class="modal-footer">
			<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
			</div>
		</div> <!-- / Terms & Conditions Modal -->
		<!-- Privacy Policy Modal -->
			<div id="privacy-policy" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="privacy-policy" aria-hidden="true">
			<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			<h3 id="modal-title">Privacy Policy</h3>
			</div>
			<div class="modal-body">
			<p>Privacy policy text...</p>

			</div>
			<div class="modal-footer">
			<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
			</div>
		</div> <!-- / Privacy Policy Modal -->
		<!-- Security Policy Modal -->
			<div id="security-policy" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="security-policy" aria-hidden="true">
			<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			<h3 id="modal-title">Security Policy</h3>
			</div>
			<div class="modal-body">
			<p>Security policy text...</p>

			</div>
			<div class="modal-footer">
			<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
			</div>
		</div> <!-- / Security Policy Modal -->

		</form>
</div>

</div><!-- l-summary -->

</div><!-- l-wrap -->

</section><!-- section -->


<!-- Final ============================== -->

<footer role="contentinfo">
<small>
</small>
</footer>
</div><!-- container -->

<!-- Load libraries -->
<script>
head.js(
// Place scripts to be loaded here, in the order the need to be executed
// Typically libraries are needed first, such as jQuery (as the plugins 
// depend on jQuery to execute). The below configuration is what Rock 
// Hammer uses, but customise this for your own needs.
// Notice the use of @path here. This is because we need the file names, 
// not the script tags being generated. Ensure all filenames you wish
// head.js to load are surrounded with quotes. Also, this is a list, so
// ensure that all but the last item are followed by a comma.

// Plugins
"js/vendor/jquery/jquery.scrollTo.min.js",

// Load bootstrap-transition first so that nice glides/fades 
// etc for the other bootstrap plugins work
"js/vendor/bootstrap/bootstrap-transition.js",
// "js/vendor/bootstrap/bootstrap-carousel.js",
// "js/vendor/bootstrap/bootstrap-tooltip.js",

// Popover has a dependency on tooltip, so make sure and include 
// bootstrap-tooltip regardless in order for popovers to work
// "js/vendor/bootstrap/bootstrap-popover.js",
"js/vendor/bootstrap/bootstrap-modal.js",
"js/vendor/bootstrap/bootstrap-collapse.js",

// Responsive data tables
// "js/rwd-table.js",

// Remove any navigation patterns below that arent used
// "js/nav-patterns/nav-toggle.js",
// "js/nav-patterns/left-nav-flyout.js",
"js/nav-patterns/responsive-nav.min.js",
// "js/nav-patterns/responsive-breadcrumb.js",

// Functionality only used by Rock Hammer - this should be removed in production projects!
// "js/navigation-manager.js"
);

// Google Analytics code
// Set your account ID appropriately by replacing UA-XXXXX-X
var _gaq=[['_setAccount','UA-XXXXX-X'],['_trackPageview']];
(function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
s.parentNode.insertBefore(g,s)}(document,'script'));
</script>
<script src="https://unpkg.com/ionicons@4.1.2/dist/ionicons.js"></script>
</body>
</html>