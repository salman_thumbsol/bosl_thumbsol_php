<?php $current_page_name="home"; ?>
<?php require_once('layouts/header.php'); ?>

<div class="row">
    <div class="strapline">
        <h1>Home of the BOS Vault</h1>
        <h2>Privacy at its Core</h2>
    </div>
</div >

<div class="row">
    <div class="wearebos">
        <p>Hello, We are BOS ! A big statement maybe but we know you're always busy. Let us take the strain for you and make your life a whole lot easier. Consider these system components and look at the screen shots... </p>
    </div>
</div>
<section class="animate">
    <div class="row"><!--js--wp-1-->
        <div class="col span-1-of-4 box component1">
            <i class="icon ion-ios-people icon-big black-bgrd"></i>
            <h3>BOS People</h3>
            <p class="summary">
                Never forget that great lead, stay in touch on any laptop or device and keep your prospects hot. . .
            </p>
        </div>
        <div class="col span-1-of-4 box component2">
            <i class="icon ion-ios-key icon-big black-bgrd"></i>
            <h3>BOS Vault</h3>
            <p class="summary">
                Keep communications encrypted and secure, simply send people an invite to unlock their special vault  . . .
            </p>
        </div>
        <div class="col span-1-of-4 box component1 ">
            <i class="icon ion-ios-list-box icon-big black-bgrd"></i>
            <h3>BOS Pipeline</h3>
            <p class="summary">
                Build your own work flow procedure, stay informed and keep your people securely in the loop . . .
            </p>
        </div>
        <div class="col span-1-of-4 box component2">
            <i class="icon ion-ios-calculator icon-big black-bgrd"></i>
            <h3>BOS Reconcile</h3>
            <p class="summary">
                Check it, reconcile it and take the confusion out of multi  &dash; currency product denominations. . .
            </p>
        </div>
    </div>

</section>
<div class="row">
    <div class="col span-4-of-4 box component1">
        <i class="icon ion-ios-key icon-small black-bgrd"></i>
        <h3>BOS Vault</h3>
        <p>
            Insert screen shots for the item in focus - default Vault
        </p>
    </div>
</div>

<?php require_once('layouts/footer.php') ?>
