/* MYA-UK-7 @ 26.02.2018 16.48.39 */
/* passwordstrengthv2.js, 3,239 bytes, 22.02.2018 12.07.51 */
var PasswordStrengthMeter = function(options) {
    function PasswordStrengthMeter(options) {
        this.options = options || {}, this.minLength = options.minLength || 7, this.maxLength = options.maxLength || 50, this.$el = $(this.options.el), this.$wrapper = $(options.targetEl).length ? $(options.targetEl) : this.$el.parent().find(".strength-meter-holder"), 0 == this.$wrapper.length && (this.$wrapper = $(".strength-meter-holder")), this.$progressBar = this.$wrapper.find(".strength-band"), this.$textBarWrapper = this.$wrapper.find(".strength-text"), this.$textBar = this.$textBarWrapper.find(".strength-text__bold"), this.$liveRegion = this.$wrapper.find(".aria-live-region"), this.strongLabel = this.$wrapper.find(" .strong-label").html(), this.goodLabel = this.$wrapper.find(" .good-label").html(), this.weakLabel = this.$wrapper.find(".weak-label").html(), this.emptyLabel = this.$wrapper.find(".empty-label").html(), this.init()
    }

    function debounce(func, wait, immediate) {
        var timeout;
        return function() {
            var context = this,
                args = arguments,
                later = function() {
                    timeout = null, immediate || func.apply(context, args)
                },
                callNow = immediate && !timeout;
            clearTimeout(timeout), timeout = setTimeout(later, wait), callNow && func.apply(context, args)
        }
    }

    function checkLength(password, minLength) {
        return password.length >= minLength
    }

    function lowerCase(password) {
        return password.match(/[a-z]/)
    }

    function upperCase(password) {
        return password.match(/[A-Z]/)
    }

    function checkNumber(password) {
        return password.match(/[0-9]/)
    }

    function checkSpecial(password) {
        return password.match(/([~,!,@,#,$,%,\^,&,\*,\(,\),_,\+,\{,\},\",\:,\|,<,>,?,\`,\-,=,\[,\],\\,\;,\',\,,\.,\/])/)
    }
    return PasswordStrengthMeter.prototype.init = function() {
        this.$el.length && (this.$el.on("keyup", this.updateProgress.bind(this)), this.updateProgress())
    }, PasswordStrengthMeter.prototype.updateProgress = function() {
        var strength = this.check();
        this.$progressBar.removeClass("weak strong good"), 1 == strength ? (this.$progressBar.addClass("weak"), this.$textBarWrapper.html(this.weakLabel)) : 2 == strength ? (this.$progressBar.addClass("good"), this.$textBarWrapper.html(this.goodLabel)) : 3 == strength ? (this.$progressBar.addClass("strong"), this.$textBarWrapper.html(this.strongLabel)) : this.$textBarWrapper.html(this.emptyLabel), this.updateAccesibilty()
    }, PasswordStrengthMeter.prototype.check = function() {
        var password = this.$el.val().trim(),
            strength = 0;
        return (lowerCase(password) || upperCase(password) || checkNumber(password)) && strength++, lowerCase(password) && upperCase(password) && strength++, (lowerCase(password) || upperCase(password)) && checkNumber(password) && strength++, checkSpecial(password) && strength++, checkLength(password, this.minLength) || (strength = 1), 0 == password.length && (strength = 0), strength > 3 && (strength = 3), strength
    }, PasswordStrengthMeter.prototype.updateAccesibilty = debounce(function() {
        var text = this.$textBarWrapper.text();
        "" == this.$wrapper.find(".strength-text__bold").text() && (text = this.$wrapper.find(".weak-label").text()), this.$liveRegion.text(text)
    }, 200), PasswordStrengthMeter
}();
$(function() {
    var strengthMeter = new PasswordStrengthMeter({
        el: document.getElementById($("#hdnPasswordControl").val()),
        minLength: $("#hdnPwdMinLength").val(),
        targetEl: "#brsr-strength-meter"
    });


    $("body ").on("click", ".panel-body button.btn", function() {
        $("#brsr-strength-meter").is(":visible") && strengthMeter.updateProgress()
    })


    $("body ").on("click", "#setpassword", function() {
        if($(".strength-band").hasClass('strong')){

        }else{
            alert('Please enter in a stronger password');
            return false;
        }
    })

});