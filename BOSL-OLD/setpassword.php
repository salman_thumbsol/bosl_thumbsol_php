<?php
require_once('config.php');
$password_mismatch=false;
$success=false;
if(!isset($_REQUEST['ticket']))
    die("");
if(isset($_GET['ticket']) && !isset($_POST['ticket'])){
    $hash = $_GET['ticket'];
    $check = $db->fetchRow("select * from entity where password_hash='" . $hash . "'");
    if ($check) {
        if($check['password']!='')
            die('ticket already used');
        $date1 = $check['verify_date'];
        $date2 = date('Y-m-d H:i:s');
        $seconds = strtotime($date2) - strtotime($date1);
        $hours = $seconds / 60 /  60;
        if($hours>1)
            die('ticket expired');
    }else{
        die('invalid ticket');
    }
}

if(isset($_POST['password']) && isset($_POST['confpassword'])){
    $password=$_POST['password'];
    $confirm_password=$_POST['confpassword'];
    if($password!=$confirm_password){
        $password_mismatch=true;
    }else {
        $hash = $_POST['ticket'];
        $check = $db->fetchRow("select * from entity where password_hash='" . $hash . "' ");
        if ($check) {
            $user_id = $check['entity_id'];
            $new_password = password_hash($password, PASSWORD_DEFAULT);
            $db->update(['password' => $new_password,'status'=>1], 'entity', ['entity_id' => $user_id]);
            $success=true;
        }
    }
}
$meta_title="Set Password";
$header_js_css=<<< HTML
       <link rel="stylesheet" href="css/passwordstrength.css" />
HTML;
require_once ('layouts/header.php');
?>
<section class="row log-in">
    <div class="">
      <h3>Set Password</h3>
    </div>
<br>
    <form class="login-form" id="loginform" method="post">
        <? if($password_mismatch==true):?>
            <p class="error">Password Mismatch</p>
        <? endif;?>
        <? if($success){?>
            <p>Password created successfully click <a href="login.php">here</a> to login </p>
        <? }else{ ?>
                <input type="hidden" name="ticket" value="<?=isset($_GET['ticket'])?$_GET['ticket']:''?>" /><br>
                 <p>
                   <label for="password">Password</label><br>
                   <input type="password" placeholder="Enter Password" id="txtSetNewPassword" aria-describedby="password-hint" aria-required="true" autocomplete="off" name="password" required>
                 </p>
                 <p>
                     <label for="password">Re Enter</label><br>
                     <input type="password" placeholder="Enter Confirm Password" aria-describedby="password-hint" aria-required="true" autocomplete="off" name="confpassword" required>
                 </p>
                <span id="passinput">
                    <div class="strength-meter-holder">
                        <div class="strength-band">
                            <span class="color-band">&nbsp;</span>
                            <span class="color-band">&nbsp;</span>
                            <span class="color-band">&nbsp;</span>
                        </div>
                    </div>
                </span>

                <input type="submit" id="passbutt" value="Set password" class="submit-btn">
                <input type="submit" id="setpassword" value="Set memorable word and complete process" class="button default" style="display:none">
                <input type="hidden" id="hdnPasswordControl" value="txtSetNewPassword">
        <?php } ?>
        </form>
    <br>
</section>
<?php
$footer_js=<<< HTML
    <script src="js/passwordstrength.js"></script>
HTML;
require_once ('layouts/footer.php');
?>
