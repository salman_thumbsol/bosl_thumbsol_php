<?php  $current_page_name='products'; ?>
<?php require_once ('layouts/header.php') ?>

        <script>
            function Msg1(){
                document.getElementById('bbo').style.display='none';
                document.getElementById('custom').style.display='none';
                document.getElementById('bespoke').style.display='none';
                document.getElementById('bosvault').style.display='block';
            }
            function Msg2(){
                document.getElementById('bosvault').style.display='none';
                document.getElementById('custom').style.display='none';
                document.getElementById('bespoke').style.display='none';
                document.getElementById('bbo').style.display='block';
            }
            function Msg3(){
                document.getElementById('bbo').style.display='none';
                document.getElementById('bosvault').style.display='none';
                document.getElementById('bespoke').style.display='none';
                document.getElementById('custom').style.display='block';
            }
            function Msg4(){
                document.getElementById('bosvault').style.display='none';
                document.getElementById('custom').style.display='none';
                document.getElementById('bespoke').style.display='none';
                document.getElementById('bespoke').style.display='block';
            }
        </script>
        <div class="row" id="">
            <div class="col span-1-of-5">
                
                <input type="button" class="products" onclick="Msg1()" value="The BOS Vault">
                <input type="button" class="products" onclick="Msg2()" value="Broker Back Office">
                <input type="button" class="products" onclick="Msg3()" value="Custom BBO">
                <input type="button" class="products" onclick="Msg4()" value="Bespoke Systems">
                        <!--<button class="products" type="button" onclick="document.getElementById('custom').style.display='block'">Custom BBO
                    </button>
                
                <button class="products" type="button" onclick="document.getElementById('bespoke').style.display='block'">Bespoke Systems
                </button>-->
                
            </div>
                <div class="col span-4-of-5 animate" id="bosvault" style="display:none">
                        <div class="">The BOS VAULT</div> 
                            <p class="white-text no-small">BBO is cloud based software that allows you, the Independent Financial Advisor, to manage your business from any computer or device that is connected to the internet. You can manage your contact folders and prospect lists with a CRM system designed specifically for your industry.</p>
                        <br class="no-small no-med">
                        <p class="white-text no-small" >With BBO you can make appointments, submit business, and manage your client files, pay employees, prepare your accounts, reconcile your bank statements and so much more.</p><br class="no-med">
                        <p class="white-text no-small" >BBO is a very powerful system; there are no limits to the number of people that can use Broker Back Office in your organisation, it doesn’t matter whether you work on your own or represent a large organisation, BBO is just what you need to maintain all your records.
                        </p><br class="no-small">
                </div>
                <div class="col span-4-of-5 animate" id="bbo" style="display:none">
                        <div class="">Broker Back Office (BBO)<br class="no-small no-med"></div>
                            <p class= "white-text no-small">A dedicated business management system that until now, only large companies could afford. We developed it just for small IFA firms working in the offshore financial services industry. It is cloud based so the development costs are shared with other IFA's
                            </p>
                                <h6 class= "white-text">PERFECT FOR SMALL COMPANIES OF 3 to 20 USERS.
                                <a href="https://broker-backoffice.com">........for more click here</a></h6>
                </div>
                <div class="col span-4-of-5 animate" id="custom" style="display:none">    
                    <div class ="bbo no-med for-small">Custom BBO<br class="no-med"></div>
                        <p class= "white-text no-small">In 2014 as our client base became larger and more demanding we introduced the concept of Custom BBO. The idea was a simple one. Take the BBO IP code, place it on separate URL then customise it to reflect your individual way of working.
                        </p>
                            <h6 class= "white-text">PERFECT FOR MEDIUM SIZED COMPANIES OF 20 to 75 USERS.
                            <a href="https://broker-backoffice.com">........for more click here</a></h6>
                    <div class ="bbo no-large for-small">Proprietary Systems<br class="no-med"></div>
                            <h6 class= "white-text">FOR LARGER COMPANIES AND PROPRIETORY SYSTEMS OF 75 or MORE USERS 
                            <a href="">........contact us here</a></h6>
                    
                    
                </div>
                <div class="col span-4-of-5 animate" id="bespoke" style="display:none">    
                    <div class ="bbo no-med for-small">Bespoke systems<br class="no-med"></div>
                        <p class= "white-text no-small">In 2014 as our client base became larger and more demanding we introduced the concept of Custom BBO. The idea was a simple one. Take the BBO IP code, place it on separate URL then customise it to reflect your individual way of working.
                        </p>
                            <h6 class= "white-text">PERFECT FOR MEDIUM SIZED COMPANIES OF 20 to 75 USERS.
                            <a href="https://broker-backoffice.com">........for more click here</a></h6>
                <div class ="bbo no-large for-small">Proprietary Systems<br class="no-med"></div>
                            <h6 class= "white-text">FOR LARGER COMPANIES AND PROPRIETORY SYSTEMS OF 75 or MORE USERS 
                            <a href="">........contact us here</a></h6>
                    
                    
                </div>
        </div>
       <!--<script> maybe use an array?
            function getProduct () {
                var products = ["bosvault", "bbo", "custom", "bespoke"]; 
                
                return products (1);
                
            }
           
        
        </script>-->

<?php require_once ('layouts/footer.php') ?>