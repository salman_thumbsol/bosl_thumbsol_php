<?php  $current_page_name='login'; ?>
<? require_once ('action_login.php') ?>
<?php

$header_js_css=<<< HTML
    
    <script>
    function openSubmit() {
        // Get the checkbox
        var submitCheck = document.getElementById("submitCheck");
        // Get the output element
        var submitBtn = document.getElementById("submitBtn");
    
        // If the checkbox is checked, display the output checkBox
        if (submitCheck.checked == true){
            submitBtn.style.display = "block";
        } else {
            submitBtn.style.display = "none";
        }
    }
    </script>
HTML;
?>
<? require_once ('layouts/header.php') ?>
        <section class="row log-in">
                <div class="col span-1-of-4 box">
                    <i class="icon ion-ios-key icon-big black-bgrd"></i><br>
                    <h3>Login to open your BOS Vault</h3>
                </div>
              <form class="animate" method="post" action="/login.php" style="clear: both">
                  <?php if($error!=''){?>
                      <p class="error"><?=$error?></p>
                  <?php } ?>
                <p><label for="email-primary">Email Address</label><br>
                    <input type="email" id="email" name="email" placeholder="Email Address" required></p>
                
                <p><label for="password">Password</label><br>
                    <input type="password" placeholder="Enter Password" name="password" required></p>

                <p><input type="checkbox" id="submitCheck" onclick="openSubmit()">  I confirm that I have read and agree to the Terms and Conditions, Cookie Policy and Privacy Policy.</p>
                <br>
                <button class= "submit-btn hide" id="submitBtn" type="submit">Login</button>
              </form><br>

                <div class="row" style="background-color:#f1f1f1">
                  <button class="submit-btn" id="forgot-btn" onclick="document.getElementById().style.display='none'" >Forgot Password</button>
                </div>

        </section>

    <div id="forgotPasswordModal" class="modal">
        <!-- The forgot password modal Content-->
        <div class="modal-content modal-content-sm">
            <span class="close">&times;</span>
            <h3>Forgotten Your Password?</h3>
            <p> Tell us your email address and we will send you and email to reset it.</p>
            <form onsubmit="return submitforgotpassword()" id="forgot-password-form">
                <p><label for="email-primary">Email Address</label><br>
                    <input type="email" id="forgot-email" name="email" placeholder="Email Address" required></p>

                <p class="hide" id="email-not-found-err">
                    Email address not found, you need to register with us first. Click <a href="register.php">here</a> to register<br>
                </p>
                <p class="hide" id="forgot-success-message">
                    Email sent successfully. please check your email<br>
                </p>
                <button class= "submit-btn " id="sendforgotBtn" type="submit">Send</button>
            </form>
        </div>
    </div>
<?php
$footer_js=<<< HTML
<script>
    var modal = document.getElementById('forgotPasswordModal');
    // Get the button that opens the modal
    var btn = document.getElementById("forgot-btn");
    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];
    // When the user clicks the button, open the modal
    btn.onclick = function() {
        modal.style.display = "block";
    };
    // When the user clicks on <span> (x), close the modal
    span.onclick = function() {
        modal.style.display = "none";
    }
    
    function submitforgotpassword(){
            $('#email-not-found-err').addClass('hide');
           $.post("forgot_password.php",{email:$('#forgot-email').val()},function(data){
               console.log(data);
               if(data.status=='ok'){
                   $('#forgot-success-message').removeClass('hide');
               }else{
                   $('#email-not-found-err').removeClass('hide');
               }
           },'json');
        return false;
    }
</script>
HTML;
?>

<? require_once ('layouts/footer.php') ?>