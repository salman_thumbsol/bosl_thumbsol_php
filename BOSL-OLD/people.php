<?php
include "config.php";

$rows=$db->fetchAll('select * from entity where role_id=10 and parent_id='.$_SESSION['user_id']);
?>
<?php require_once ('layouts/header_people.php') ?>
<a href="#navigation-toggle" class="navigation-toggle">Menu</a>

<nav id="subnav" role="subnav">
	<div class="subnav-header">
		<!<ion-icon name="people"></ion-icon>
	</div>
	<ul>
		<li><a href="#add-contact" data-toggle="modal">Add Contact</a></li>
		<li><a href="#">Second Link</a></li>
		<li><a href="#">Third Link</a></li>
		<li><a href="#">Fourth Link</a></li>
	</ul>
	<!-- Add Contact Form --->

<div id="add-contact" class="modal hide fade modal-secondary" tabindex="-1" role="dialog" aria-labelledby="add-contact" aria-hidden="true">
	<form action="contact_action.php" method="post" id="contact-form" onsubmit="return savecontact();">
	<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			<h3 id="modal-title">Add Contact</h3>
		</div>
		<div class="modal-body">
			<p>Use the form below to add a contact.</p>
            <div class="form-response"></div>
			<p>Title</p>
			<p>
				<select type="text" name="title" id="title">
					<option>Mr</option>
					<option>Miss</option>
					<option>Mrs</option>
					<option>Ms</option>
				</select>
			</p>
			<p><label for="first_name">First Name</label><br>
			<input type="text" id="first_name" name="firstname" placeholder="First Name" required></p>
			<p><label for="surname">Surname</label><br>
			<input type="text" id="surname" name="surname" placeholder="Surname" required></p>
			<p><label for="email">Email Address</label><br>
			<input type="email" id="email" name="email" placeholder="Email Address" required></p>
			<p><label for="tel">Telephone Number</label><br>
			<input type="tel" id="mobile" name="mobile" placeholder="000-00-000-000" required></p>
			<p><label for="clients_notes">Client Notes</code></label><br>
			<textarea id="clients_notes" name="clients_notes"> </textarea></p>
		</div>
		<div class="modal-footer">
			<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
		    <button type="submit" name="submit" id="submit" class="btn btn-primary">Save</button>
			<!-- <button class="btn btn-error">Delete</button> -->
		</div>
	</form>
</div>
</nav>

<div class="container">

<!-- Next ============================== -->

<section>


<div class="l-wrap">
<div class="l-full-width">

<div class="table-wrapper">

<div class="tablesaw-overflow">
			<table class="tablesaw" data-tablesaw-mode="columntoggle" data-tablesaw-sortable data-tablesaw-sortable-switch data-tablesaw-minimap data-tablesaw-mode-switch>
				<thead>
					<tr>
						<th scope="col" data-tablesaw-sortable-col data-tablesaw-sortable-default-col data-tablesaw-priority="persist">Name</th>
						<th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="2">Email</th>
						<th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="3">Phone</th>
						<th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="5">Contract</th>
						<th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="1">Client Details</th>
						<th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4">Archive</th>
					</tr>
				</thead>
				<tbody>
                <?php foreach ($rows as $row):?>
					<tr id="row-<?=$row['entity_id']?>">
						<td class="title"><?php echo $row['firstname'];?></td>
						<td><?php echo $row['email']; ?></td>
						<td><?php echo $row['mobile']; ?></td>
						<td class="send-access-btns"><a href="javascript:void(0)" onclick="openaccessmodal(<?=$row['entity_id']?>,'<?=$row['email']?>','<?=$row['firstname'].' '.$row['surname']?>')" class="btn btn-primary btn-small">Send Access</a></td>
						<td><a href="#client-details" data-toggle="modal" class="btn btn-small">Client Details</a></td>
						<td><a href="#" class="btn btn-small">Archive</a></td>
					</tr>
                <?php endforeach; ?>
				</tbody>
			</table>
		</div><!-- /tablesaw-overflow -->

	<!-- Send Access -->
	<div id="send-access" class="modal hide fade modal-secondary" tabindex="-1" role="dialog" aria-labelledby="send-access" aria-hidden="true">
	<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	<h3 id="modal-title">Send Access</h3>
	</div>
	<div class="modal-body">
    <div class="form-response"></div>
	<p>You're about to send access to <strong id="access_modal_name"></strong> at the email address <strong id="access_modal_email"></strong>. Are you sure?</p>
	</div>
	<div class="modal-footer">
	<button class="btn" data-dismiss="modal" aria-hidden="true">No, cancel</button>
	<a href="#" id="send_access_yes" class="btn btn-primary">Yes, please send</a>
	</div>
	</div>

	<!-- Client Details -->
	<div id="client-details" class="modal hide fade modal-secondary" tabindex="-1" role="dialog" aria-labelledby="client-details" aria-hidden="true">
	<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	<h3 id="modal-title">Client Details: Novak Djokovic</h3>
	</div>
	<div class="modal-body">

		<div class="tabbable">
		<ul class="nav nav-tabs">
		<li class="active"><a href="#tab1" data-toggle="tab">Details</a></li>
		<li><a href="#tab2" data-toggle="tab">Portfolio</a></li>
		<li><a href="#tab3" data-toggle="tab">Tasks</a></li>
		<li><a href="#tab4" data-toggle="tab">Activity</a></li>
		<li><a href="#tab5" data-toggle="tab">Notes</a></li>
		</ul>

		<div class="tab-content">
		<div class="tab-pane active" id="tab1">
    			<p>Use the form below to update details.</p>
    			<form>

    				<p>Title</p>
    				<p>
    				<select>
    				<option>Mr</option>
    				<option>Miss</option>
    				<option>Mrs</option>
    				<option>Ms</option>
    				</select>
    				</p>

    			<p><label for="text">First Name</label><br>
    			<input type="text" id="first-name" placeholder="Novak"></p>

    			<p><label for="text">Surname</label><br>
    			<input type="text" id="surname" placeholder="Djokovic"></p>

    			<p><label for="email">Email Address</label><br>
    			<input type="email" id="email" placeholder="novak@novakdjokovic.com"></p>

    			<p><label for="tel">Telephone Number</label><br>
    			<input type="tel" id="tel" placeholder="000-00-000-000"></p>

    			<p><label for="textarea">Client Notes</code></label><br>
    			<textarea id="textarea">Severely off-form at the moment.</textarea></p>

    			</form>
    			<button class="btn" data-dismiss="modal" aria-hidden="true">Clear</button>
    			<button class="btn btn-primary">Update</button>
    		</div><!-- tab-pane -->

		<!-- Next -->

		<div class="tab-pane" id="tab2">
		<p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec varius ante sed mi malesuada, ac sollicitudin nisi vulputate. Mauris scelerisque, odio id cursus finibus, nulla diam condimentum nulla, nec condimentum neque velit a justo.</p>
		</div><!-- tab-pane -->
		<!-- Next -->

		<div class="tab-pane" id="tab3">
		<p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec varius ante sed mi malesuada, ac sollicitudin nisi vulputate. Mauris scelerisque, odio id cursus finibus, nulla diam condimentum nulla, nec condimentum neque velit a justo.</p>
		</div><!-- tab-pane -->
		<!-- Next -->

		<div class="tab-pane" id="tab4">
		<p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec varius ante sed mi malesuada, ac sollicitudin nisi vulputate. Mauris scelerisque, odio id cursus finibus, nulla diam condimentum nulla, nec condimentum neque velit a justo.</p>
		</div><!-- tab-pane -->
		<!-- Next -->

		<div class="tab-pane" id="tab5">
		<p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec varius ante sed mi malesuada, ac sollicitudin nisi vulputate. Mauris scelerisque, odio id cursus finibus, nulla diam condimentum nulla, nec condimentum neque velit a justo.</p>
		</div><!-- tab-pane -->
		</div><!-- tab-content -->
		</div><!-- tabbable -->
	</div>
	<div class="modal-footer">
	<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
	<button class="btn btn-primary">Save</button>
	<!-- <button class="btn btn-error">Delete</button> -->
	</div>
	</div>
</div>

</div><!-- l-summary -->

</div><!-- l-wrap -->

</section><!-- section -->


<!-- Final ============================== -->

    <section>
        <div class="l-wrap">
            <footer role="contentinfo">
                <small>
                </small>
            </footer>
        </div>
    </section>

</div><!-- container -->


<!-- Load libraries -->
<script>
head.js(
// Place scripts to be loaded here, in the order the need to be executed
// Typically libraries are needed first, such as jQuery (as the plugins 
// depend on jQuery to execute). The below configuration is what Rock 
// Hammer uses, but customise this for your own needs.
// Notice the use of @path here. This is because we need the file names, 
// not the script tags being generated. Ensure all filenames you wish
// head.js to load are surrounded with quotes. Also, this is a list, so
// ensure that all but the last item are followed by a comma.

// Plugins
"js/vendor/jquery/jquery.scrollTo.min.js",

// Load bootstrap-transition first so that nice glides/fades 
// etc for the other bootstrap plugins work
"js/vendor/bootstrap/bootstrap-transition.js",
// "js/vendor/bootstrap/bootstrap-carousel.js",
// "js/vendor/bootstrap/bootstrap-tooltip.js",

// Popover has a dependency on tooltip, so make sure and include 
// bootstrap-tooltip regardless in order for popovers to work
// "js/vendor/bootstrap/bootstrap-popover.js",
"js/vendor/bootstrap/bootstrap-modal.js",
"js/vendor/bootstrap/bootstrap-collapse.js",

// Responsive data tables
// "js/rwd-table.js",

// Remove any navigation patterns below that arent used
// "js/nav-patterns/nav-toggle.js",
// "js/nav-patterns/left-nav-flyout.js",
"js/nav-patterns/responsive-nav.min.js",
// "js/nav-patterns/responsive-breadcrumb.js",

// Functionality only used by Rock Hammer - this should be removed in production projects!
// "js/navigation-manager.js"
);

// Google Analytics code
// Set your account ID appropriately by replacing UA-XXXXX-X
var _gaq=[['_setAccount','UA-XXXXX-X'],['_trackPageview']];
(function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
s.parentNode.insertBefore(g,s)}(document,'script'));
</script>
<script src="https://unpkg.com/ionicons@4.1.2/dist/ionicons.js"></script>

<script>
$(function () {
  console.log('hi')
  $('.nav-tabs a').on('click',function(e) {
    e.preventDefault();
    var link = $(this).attr('href').replace('#','');
    $('.nav-tabs a').each(function() {
      if($(this).attr('href')==='#'+link) {
        $(this).parent().addClass('active');
      } else {
        $(this).parent().removeClass('active');
      }
    })
    $('.tab-pane').each(function() {
      if($(this).attr('id')===link) {
        $(this).addClass('active');
      } else {
        $(this).removeClass('active');
      }
    })
  })
})
function openaccessmodal(id,email,name) {
    $('#access_modal_name').html(name);
    $('#access_modal_email').html(email);
    $('#send_access_yes').attr('href','send_access.php?id='+id);
    $('#send-access').modal();
}

function savecontact(){
    $('#contact-form .form-response').html('');
    $.post('/contact_action.php',$('#contact-form').serialize(),function(data){
        if(data!=null){
             $('#contact-form .form-response').html('<div class="alert alert--'+data.status+'">'+data.message+'</div>');
             setTimeout(function () {
                 $('#contact-form .form-response').html('');
             },2000);
        }
        $('#contact-form .modal-body').scrollTo(0,0);
    },'json').fail(function(e){

    });
    return false;
}
$(document).on('click','#send_access_yes',function(e){
    e.preventDefault();
    $('#send-access .form-response').html('');
    $.get($(this).attr('href'),function(data){
        $('#send-access .form-response').html('<div class="alert alert--'+data.status+'">'+data.message+'</div>');
        setTimeout(function () {
            $('#send-access .form-response').html('');
        },2000);
    },'json');
});
</script>
</body>
</html>