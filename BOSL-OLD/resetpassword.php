<?php
require_once('config.php');
$password_mismatch=false;
$success=false;
if(!isset($_REQUEST['token']))
    die("");
if(isset($_GET['token']) && !isset($_POST['token'])){
    $token = $_GET['token'];
    $check = $db->fetchRow("select * from password_resets where token='" . $token . "' and status=0");
    if ($check) {
        $date1 = $check['created_at'];
        $date2 = date('Y-m-d H:i:s');
        $seconds = strtotime($date2) - strtotime($date1);
        $hours = $seconds / 60 /  60;
        if($hours>24)
            die('token expired');
    }else{
        die('invalid token');
    }
}

if(isset($_POST['password']) && isset($_POST['confpassword'])){
    $password=$_POST['password'];
    $confirm_password=$_POST['confpassword'];
    if($password!=$confirm_password){
        $password_mismatch=true;
    }else {
        $token = $_POST['token'];
        $token_check = $db->fetchRow("select * from password_resets where token='" . $token . "' and status=0");
        if($token_check){
            $email=$token_check['email'];

            $user = $db->fetchRow("select * from entity where email='" . $email . "' ");
            if ($user) {
                $user_id = $user['entity_id'];
                $new_password = password_hash($password, PASSWORD_DEFAULT);
                $db->update(['password' => $new_password,'status'=>1], 'entity', ['entity_id' => $user_id]);
                $db->update(['status'=>1], 'password_resets', ['email' => $email]);
                $success=true;
            }


        }

    }
}
$meta_title="Reset Password";
$header_js_css=<<< HTML
       <link rel="stylesheet" href="css/passwordstrength.css" />
HTML;
require_once ('layouts/header.php');
?>
<section class="row log-in">
    <div class="">
        <h3>Reset Password</h3>
    </div>
    <br>
    <form class="login-form" id="loginform" method="post">
        <? if($password_mismatch==true):?>
            <p class="error">Password Mismatch</p>
        <? endif;?>
        <? if($success){?>
            <p>Password created successfully click <a href="login.php">here</a> to login </p>
        <? }else{ ?>
            <input type="hidden" name="token" value="<?=isset($_GET['token'])?$_GET['token']:''?>" /><br>
            <p>
                <label for="password">Password</label><br>
                <input type="password" placeholder="Enter Password" id="txtSetNewPassword" aria-describedby="password-hint" aria-required="true" autocomplete="off" name="password" required>
            </p>
            <p>
                <label for="password">Re Enter</label><br>
                <input type="password" placeholder="Enter Confirm Password" aria-describedby="password-hint" aria-required="true" autocomplete="off" name="confpassword" required>
            </p>
            <span id="passinput">
                    <div class="strength-meter-holder">
                        <div class="strength-band">
                            <span class="color-band">&nbsp;</span>
                            <span class="color-band">&nbsp;</span>
                            <span class="color-band">&nbsp;</span>
                        </div>
                    </div>
                </span>

            <input type="submit" id="passbutt" value="Set password" class="submit-btn">
            <input type="submit" id="setpassword" value="Set memorable word and complete process" class="button default" style="display:none">
            <input type="hidden" id="hdnPasswordControl" value="txtSetNewPassword">
        <?php } ?>
    </form>
    <br>
</section>
<?php
$footer_js=<<< HTML
    <script src="js/passwordstrength.js"></script>
HTML;
require_once ('layouts/footer.php');
?>
