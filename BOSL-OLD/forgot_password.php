<?php
require_once ('config.php');
if(isset($_POST['email'])) {
    $email = $_POST['email'];
    $root_url = ROOT_URL;
    $user = $db->fetchRow("select * from entity where email='" . $email . "' and `status`=1");
    if ($user) {
        $first_name = $user['firstname'];
        $last_name = $user['surname'];
        $token = $password_hash = password_hash($email . uniqid() . date('ymdhis'), PASSWORD_DEFAULT);
        $db->insert(['email' => $email, 'token' => $token, 'created_at' => date('Y-m-d H:i:s')], 'password_resets');
        $mail_html = <<< HTML
        <p>click <a href='$root_url/resetpassword.php?token=$token'>here</a> to reset your password</p>
HTML;
        require 'libs/PHPMailer/PHPMailerAutoload.php';
        $mail = new PHPMailer;
        $mail->isSMTP();
        $mail->SMTPDebug = MAIL_DEBUG;
        $mail->Debugoutput = 'html';
        $mail->Host = MAIL_HOST;
        $mail->Port = MAIL_PORT;
        $mail->SMTPSecure = MAIL_ENCRYPTED;
        $mail->SMTPAuth = true;
        $mail->Username = MAIL_USERNAME;
        $mail->Password = MAIL_PASSWORD;
        $mail->setFrom(FROM_EMAIL, FROM_NAME);
        $mail->addAddress($email, $first_name . ' ' . $last_name);
        $mail->Subject = 'BOSL Forgotten Password';
        $mail->msgHTML($mail_html);
        $mail->AltBody = $mail_html;
        if (!$mail->send()) {
            echo json_encode(['status' => 'failed']);exit();
        }
        echo json_encode(['status'=>'ok']);exit();
    }

    echo json_encode(['status' => 'failed']);exit();
}

?>