var contact={
    initContactVault:function(){
        $(document).on('click','.toggle-long-message',function (e) {

            e.preventDefault();
            var status=$(this).attr('data-status');
            var message_id=$(this).attr('data-id');
            if($(this).closest('.note-single').find('.long-message').hasClass('hide')){
                $(this).closest('.note-single').find('.short-message').addClass('hide').fadeOut();
                $(this).closest('.note-single').find('.long-message').removeClass('hide').fadeIn();
                if(status=='0'){
                    $.post('/api/message.php',{'id':message_id,'action':'mark_read'},function (data) {
                        contact.refresh_counter();
                    },'json');
                }
            }else{
                $(this).closest('.note-single').find('.short-message').removeClass('hide').fadeIn();
                $(this).closest('.note-single').find('.long-message').addClass('hide').fadeOut();
            }
        });

        $(document).on('click','.hide-message',function (e) {
            e.preventDefault();
            var _this=this;
            var message_id=$(this).attr('data-id');
            $.post('/api/message.php',{'id':message_id,'action':'hide'},function (data) {
                //$('.message-count-new').html(data.count);
                if(data.status=='ok') {
                    $(_this).closest('.note-single').fadeOut();
                }
            },'json');
        });

        $(document).on('click','.show-message',function (e) {
            e.preventDefault();
            var _this=this;
            var message_id=$(this).attr('data-id');
            $.post('/api/message.php',{'id':message_id,'action':'show'},function (data) {
                if(data.status=='ok') {
                    $(_this).closest('.note-single').fadeOut();
                   contact. refresh_counter();
                }
            },'json');
        });

        $(document).on('click','.reply-message',function(e){
            e.preventDefault();
            $(this).closest('.note-single').find('.reply-area').toggleClass('hide');
        });
        $(document).on('click','.mark-unread-message',function (e) {
            e.preventDefault();
            var _this=this;
            var message_id=$(this).attr('data-id');
            $.post('/api/message.php',{'id':message_id,'action':'mark_unread'},function (data) {
                if(data.status=='ok') {
                    //$(_this).closest('.note-single').fadeOut();
                    contact.refresh_counter();
                }
            },'json');
        });
    },
    refresh_counter: function(){
            $.post('/api/message.php',{'action':'refresh_message_counter'},function(data){
            if(data.status=='ok'){
                var counters=data.counters;
                var total_unread=0;
                counters.forEach(function(obj) {
                    $('.message-count-new-'+obj.folder_id).html(obj.unread);
                    total_unread+=parseInt(obj.unread);
                    $('.message-count-archived-'+obj.folder_id).html(obj.hide);

                });
                $('.message-count-new').html(total_unread);
            }
        },'json').done(function(){

        })
    },
    reply: function(form){
        $.post('/api/message.php',$(form).serialize(),function(data){
            if(data.status=='ok'){
                $('.notes-log').prepend(data.message);
                $(form).find('.reply-textbox').val('');
            }
        },'json').done(function(){

        });
        return false;
    },
    refresh_messages:function(folder_id){
        $.post('/api/message.php',{'action':'refresh_message','folder_id':folder_id},function(data){
            if(data.status=='ok'){
                $('#notes-log-'+folder_id).prepend(data.html);
            }
        },'json').done(function(){
        })
    }
};