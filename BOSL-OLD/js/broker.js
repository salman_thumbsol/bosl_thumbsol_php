
/********* init broker profile page *************/
function initProfile(){
    $(function () {
        Dropzone.autoDiscover = false;
        $(".folder-messages #id_dropzone").dropzone({
            maxFiles: 2000,
            url: "/api/message.php",
            success: function (file, response) {
                console.log(response);
            }
        });
        $('.nav-tabs a').on('click',function(e) {
            e.preventDefault();
            var link = $(this).attr('href').replace('#','');
            $('.nav-tabs a').each(function() {
                if($(this).attr('href')==='#'+link) {
                    $(this).parent().addClass('active');
                } else {
                    $(this).parent().removeClass('active');
                }
            })
            $('.tab-pane').each(function() {
                if($(this).attr('id')===link) {
                    $(this).addClass('active');
                } else {
                    $(this).removeClass('active');
                }
            })
        })
    });

    $(document).on('click','#send_access_yes',function(e){
        e.preventDefault();
        $(this).html('Sending...');
        $('#send_access_yes').blur();
        $('#send-access .form-response').html('');
        $.post($(this).attr('href'),function(data){
            $('#send-access .form-response').html('<div class="alert alert--'+data.status+'">'+data.message+'</div>');
            if(data.status=='success'){
                $('#send-invite-btn-'+data.id).hide();
            }
            setTimeout(function () {
                $('#send-access .form-response').html('');
                $('#send-access').modal('hide');
                $('#send_access_yes').html('Yes, please send');
            },2000);
        },'json');
    });



//update contact
    $(document).on('click','.client-detail-btn',function(e){
        e.preventDefault();
        var id=$(this).attr('data-id');
        $.post('/api/contact.php',{id:id,action:'get'},function(data){
            if(data!='failed') {
                $('#client-details').html(data);
                $('#client-details').modal();
            }
        });

    });

    $(document).on('click','#close-add-contact-modal',function(e){
        e.preventDefault();
        e.stopPropagation();
       
        var f = document.getElementById('contact-form');
        var fname=$('#first_name').val();
        if(fname!=''){
             var c=confirm('Do you want to save?');
                if(c==true){
                    if (!f.checkValidity()) {
                        if (f.reportValidity) {
                            f.reportValidity();                    
                        }
                    }else{
                        $('#submit-addc-contact-form').trigger('click');
                    }
                }else{
                    $('#contact-form')[0].reset();
                    $('#contact-form').find('.form-response').html('');
                    $('#add-contact').modal('hide'); 
                }         
                   
                   
                }else{                
                    $('#add-contact').modal('hide'); 
                }

    });


    $(document).on('click','#close-add-level-modal',function(e){
        e.preventDefault();
        e.stopPropagation();

        var f = document.getElementById('add-level-1-user-form');
        var fname=$('#level_first_name').val();
        if(fname!=''){
            var c=confirm('Do you want to save?');
            if(c==true){
                if (!f.checkValidity()) {
                    if (f.reportValidity) {
                        f.reportValidity();
                    }
                }else{
                    $('#submit-add-level1-form').trigger('click');
                }
            }else{
                $('#add-level-1-user-form')[0].reset();
                $('#add-level-1-user-form').find('.form-response').html('');
                $('#add-level-1-user').modal('hide');
            }


        }else{
            $('#add-level-1-user').modal('hide');
        }

    });

}
/******** open access model ********/
function openaccessmodal(id,email,name) {
    $('#access_modal_name').html(name);
    $('#access_modal_email').html(email);
    $('#send_access_yes').attr('href','/api/contact.php?action=send_access&id='+id);
    $('#send-access').modal();
}

/******** save contact ********/
function savecontact(){
    $('#contact-form .form-response').html('');
    $.post('/api/contact.php',$('#contact-form').serialize(),function(data){
        if(data!=null){
            if(data=='failed') {
                $('#contact-form .form-response').html('<div class="alert alert--error">Email Already taken please try with another</div>');
            }else{
                $('#contact-form .form-response').html('<div class="alert alert--success">Contact created successfully</div>');
                $('#contacts-tbody').prepend(data);
                setTimeout(function(){
                    location.reload();
                    $('#add-contact').modal('hide');
                    $('#contact-form .form-response').html('');
                    $('#contact-form')[0].reset();

                },1000);
            }

        }
        $('#contact-form .modal-body').scrollTo(0,0);
    }).fail(function(e){

    });
    return false;
}



function saveleveloneuser(){
    $('#add-level-1-user-form .form-response').html('');
    $.post('/api/co-broker.php',$('#add-level-1-user-form').serialize(),function(data){
        if(data!=null){
            if(data=='failed') {
                $('#add-level-1-user-form .form-response').html('<div class="alert alert--error">Email Already taken please try with another</div>');
            }else if(data=='limit_exceded'){
                $('#add-level-1-user-form .form-response').html('<div class="alert alert--error">You cannot create more than 3 Level One Users</div>');
            }else{
                $('#add-level-1-user-form .form-response').html('<div class="alert alert--success">User created successfully</div>');
                           
                setTimeout(function(){
                    location.reload();
                    $('#add-level-1-user').modal('hide');
                    $('add-level-1-user-form .form-response').html('');
                    $('#add-level-1-user-form')[0].reset();

                },1000);
            }

        }
        $('#add-level-1-user-form .modal-body').scrollTo(0,0);
    }).fail(function(e){

    });
    return false;
}

function submitAddContact(e){
    //var code = (e.keyCode ? e.keyCode : e.which);
   //return false;
}
/********* update contact *************/

function updatecontact() {

    var id=$('#update_entity_id').val();
    $('#update-contact-form .form-response').html('');
    $.post('/api/contact.php',$('#update-contact-form').serialize(),function(data){
        if(data!=null){
            if(data=='failed') {
                $('#update-contact-form.form-response').html('<div class="alert alert--error">Email Already taken please try with another</div>');
            }else{
                $('#update-contact-form .form-response').html('<div class="alert alert--success">Contact updated successfully</div>');
                $('#row-'+id).replaceWith(data);
                setTimeout(function(){
                    location.reload();
                    $('#client-details').modal('hide');
                    $('#update-contact-form .form-response').html('');
                    $('#update-contact-form')[0].reset();
                },1000);
            }

        }
        $('#client-details .modal-body').scrollTo(0,0);
    }).fail(function(e){

    });
    return false;
}
///end update

/********* save notes *************/
function savenotes() {
    $.post('/api/contact.php',$('#notes-form').serialize(),function(data){
        $('#notes-container').html(data);
    });
    return false;
}

/********* save messages *************/
function savemessage(form){
    if(dz && dz[0].dropzone.getAcceptedFiles().length>0){
       dz[0].dropzone.processQueue();
    }else {
        $.post('/api/message.php', $(form).serialize(), function (data) {
            //$(form).closest('.tab-pane').find('.messages-list').find('u').prepend(data.html);
            $('#vault-messsage-list-' + data.message.folder_id).prepend(data.html);
            $(form).find('textarea').val("");
            $('.vault-messsage-list').scrollTop(0);
        }, 'json');
    }
    return false;
}

/********* init broker vault page *************/

function initVault(){
    Dropzone.autoDiscover = false;
    $(".folder_detail .document_dropzone").dropzone({
        uploadMultiple:false,
        autoProcessQueue: false,
        addRemoveLinks:true,
        url: "/api/message.php",
        init: function() {
            dzClosure = this; // Makes sure that 'this' is understood inside the functions below.
            folder_id=this.element.dataset.folder_id;
            // for Dropzone to process the queue (instead of default form behavior):
            document.getElementById("save-folder-"+folder_id).addEventListener("click", function(e) {
                // Make sure that the form isn't actually being sent.
                e.preventDefault();
                e.stopPropagation();
                dzClosure.processQueue();
            });

            //send all the form data along with the files:
            this.on("sendingmultiple", function(data, xhr, formData) {
                formData.append('folder_id', this.element.dataset.folder_id);
                formData.append('action', 'upload_file');
                formData.append('message',$('#message-text-'+folder_id).val());
            });
        },
        sending:function(file, xhr, formData){
            formData.append('folder_id', dzClosure.element.dataset.folder_id);
            formData.append('action', 'upload_file');
            formData.append('message',$('#message-text-'+folder_id).val());
        },
        success: function (file, response) {
            console.log(response);
        }
    });

    $('.folders-table .new-entry').click(function(e){
        e.preventDefault();
        var html=$(this).html();
        var folder_id=$(this).attr('data-folder-id');

        $(this).closest('tr').next().find('.folder_detail').slideToggle();
        $('#select2-'+folder_id).select2();
        if(html=='Add a User'){
            $(this).html("Cancel");
        }else{
            $(this).html("Add a User");
        }
    });

    $('.nav-tabs a').on('click',function(e) {
        e.preventDefault();
        var link = $(this).attr('href').replace('#','');
        $('.nav-tabs a').each(function() {
            if($(this).attr('href')==='#'+link) {
                $(this).parent().addClass('active');
            } else {
                $(this).parent().removeClass('active');
            }
        });
        $('.tab-pane').each(function() {
            if($(this).attr('id')===link) {
                $(this).addClass('active');
            } else {
                $(this).removeClass('active');
            }
        })
    });

    $(document).on('change','#agree-policy',function(){
        if($(this).is(":checked")){
            $('#submit-profile-btn').removeClass('display_none');
        }else{
            $('#submit-profile-btn').addClass('display_none');
        }
    });

    $(document).on('change','input[name="permission"]',function(){
        var v = $(this).val();
        var folder_id=$(this).attr('data-folder-id');
        $('#submit-btn-'+folder_id).removeClass('display_none');
        if(v=='choose_in_file'){
            $('#choose_in_file_section-'+folder_id).removeClass('hide');
            $('#select2-'+folder_id).select2();
            $('#select2-'+folder_id).select2('open');
        }else{
            $('#choose_in_file_section-'+folder_id).addClass('hide');
        }
    });

}

/*********** update folder ***********/
function updatefolder(form){

    var folder_id=$(form).attr('data-folder-id');
    $.post('/api/folder.php',$(form).serialize(),function(data){
        if(data.status=='success'){
            $(form).find('.resp').html('<p class="alert alert--success">'+data.message+'</p>');
            $('#folder-status-'+folder_id).html(data.folder_status);
            $('#folder-name-'+folder_id).html(data.folder_name);
            if(data.folder_status=='Applied To All'){
                $('#folder-add-user-'+folder_id).html('');
            }
        }else{
            $(form).find('.resp').html('<p class="alert alert--error">'+data.message+'</p>');
        }
        setTimeout(function(){
            $(form).find('.resp').html('');
        },1000);
    },'json');
    return false;
}

var editinterval=null;
var dz=null;

function refresh_vault_message(entity_id){
    $.post('/api/message.php',{'action':'refresh_vault_message',id:entity_id},function(data){
        if(data.status=='ok'){
            var items=data.items;
            for(var i=0;i<items.length;i++){
                var item=items[i];
                // if ($('#message-'+item.parent_id).find("ul").length ==0) {
                //     $('#message-'+item.parent_id).append('<ul></ul>');
                // }
                $('#vault-messsage-list-'+item.folder_id).prepend(item.html);
                // $('#message-'+item.parent_id).find('ul').append(item.html);
            }
            // $('.vault-messsage-list').animate({"scrollTop": $('.vault-messsage-list')[0].scrollHeight}, "slow");
        }
    },'json').done(function(){
    })
}
function initeditfolder(entity_id){

    if(editinterval!=null){
        clearInterval(editinterval);
    }
    editinterval=setInterval(function(){
        refresh_vault_message(entity_id);
    },5000);

    $('#vault-tabs a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        var folder_id=$(this).attr('data-folder-id');
        var entity_id=$(this).attr('data-entity-id');

        try{
            // dz = $('#id_dropzone_'+folder_id)[0].dropzone;
            if (dz) {
                dz[0].dropzone.destroy();
            }
        }catch(err){}

        dz = $("#id_dropzone_"+folder_id).dropzone({
            uploadMultiple:false,
            autoProcessQueue: false,
            addRemoveLinks:true,
            url: "/api/message.php",
            dataType:'json',
            maxFiles:1,
            init: function() {
                dzClosure = this;
                this.on("maxfilesexceeded", function(file) {
                    this.removeAllFiles();
                    this.addFile(file);
                });

                // for Dropzone to process the queue (instead of default form behavior):
                // document.getElementById("save-in-folder-btn-"+folder_id).addEventListener("click", function(e) {
                //     // Make sure that the form isn't actually being sent.
                //     e.preventDefault();
                //     e.stopPropagation();
                //     dzClosure.processQueue();
                // });

                //send all the form data along with the files:
                this.on("sendingmultiple", function(data, xhr, formData) {
                    formData.append('folder_id', folder_id);
                    formData.append('entity_id', entity_id);
                    formData.append('action', 'send_vault_message');
                    formData.append('message',$('#message-text-'+folder_id).val());
                });
            },
            sending:function(file, xhr, formData){
                formData.append('folder_id', folder_id);
                formData.append('entity_id', entity_id);
                formData.append('action', 'send_vault_message');
                formData.append('message',$('#message-text-'+folder_id).val());
            },
            success: function (file, response) {
                var data=JSON.parse(response);
                $('#vault-messsage-list-'+data.message.folder_id).prepend(data.html);
                $(this).closest('form').find('textarea').val("");
                $('.vault-messsage-list').scrollTop(0);
            },
            complete: function(file) {
                this.removeAllFiles(true);
                $('#message-text-'+folder_id).val('');
            }
        });
        if(!$(this).hasClass('waypoint')){
            new BOSLWayPoint(folder_id,entity_id).init();
            $(this).addClass('waypoint');
        }
    });
}
/***** set message read */
$(document).on('hover','.vault-messsage-list li:not(.read)',function(){
    var message_id=$(this).attr('data-id');
    $.post('/api/message.php',{'id':message_id,'action':'mark_read'},function (data) {
        if(data.status=='ok'){
            $('.vault-messsage-list').find('#message-'+message_id).addClass('read');
        }
    },'json');
});

