/******** save contact ********/
function savecontact() {
    $('#contact-form .form-response').html('');
    $.post('/api/contact.php', $('#contact-form').serialize(), function (data) {
        if (data != null) {
            if (data == 'failed') {
                $('#contact-form .form-response').html('<div class="alert alert--error">Email Already taken please try with another</div>');
            } else {
                $('#contact-form .form-response').html('<div class="alert alert--success">Contact created successfully</div>');
                $('#contacts-tbody').prepend(data);
                setTimeout(function () {
                    location.reload();
                    $('#add-contact').modal('hide');
                    $('#contact-form .form-response').html('');
                    $('#contact-form')[0].reset();

                }, 1000);
            }
        }
        $('#contact-form .modal-body').scrollTo(0, 0);
    }).fail(function (e) {

    });
    return false;
}

function BOSLWayPoint(folder_id,entity_id,user_type='broker'){
    this.folder_id=folder_id;
    this.entity_id = entity_id;
    this.limit=5;
    this.v=false;
    this.loading=false;
    this.element='#vault-messsage-list-'+folder_id;
    this.scrollElement=this.element;
    this.user_type=user_type;
    this.params=[];
    this.setElement=function(element){
        this.element=element;
        this.scrollElement=this.element;
    };
    this.setUserType=function(user_type){
        this.user_type=user_type;
    };
    this.setScrollElement=function(element){
        this.scrollElement=element;
    };
    this.setExtraParams=function(param){
        this.params.push(param);
    };
    this.init=function(){
        var _this=this;
        $(_this.element).html('');
        this.load();
        $(_this.scrollElement).scroll(function(){
            var e = $(_this.element+' > li:last');
            var vNow = _this.isElementVisible(e);
            if (!vNow) {
                _this.v = false;
                return;
            }
            if (vNow && ! _this.v) {
                _this.v = true;
                if(!_this.loading) {
                    _this.load();
                }
            }
        });
    };
    this.load=function(){
        var _this=this;
        var last_element=$(_this.element+' > li:last');
        var last_id=$(last_element).attr('data-id');
        _this.loading=true;
        var data={'action':'getContactMessages',entity_id:this.entity_id,last_id:last_id,limit:this.limit,folder_id:this.folder_id,user_type:this.user_type};
        this.params.forEach(function(param){
            data[param.id] = param.value;
        });
        $.get('/api/message.php',data,function(res){
            if(res.status=='ok') {
                $(_this.element).append(res.html);
            }
        },'json').done(function(){}).complete(function(){
            _this.loading=false;
        });
    };
    this.isElementVisible = function(elem)
    {
        var $elem = $(elem);
        var $window = $(window);
        var docViewTop = $window.scrollTop();
        var docViewBottom = docViewTop + $window.height();
        var elemTop = $elem.offset().top;
        var elemBottom = elemTop + $elem.height();
        return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
    }
}

