
<?php
$current_page_name='products';
$header_js_css=<<< HTML
        <script>
            function Msg1(){
                document.getElementById('bbo').style.display='none';
                document.getElementById('custom').style.display='none';
                document.getElementById('bespoke').style.display='none'; 
                document.getElementById('bosvault').style.display='block';
            }
            function Msg2(){
                document.getElementById('bosvault').style.display='none';
                document.getElementById('custom').style.display='none';
                document.getElementById('bespoke').style.display='none'; 
                document.getElementById('bbo').style.display='block';
            }
            function Msg3(){
                document.getElementById('bbo').style.display='none';
                document.getElementById('bosvault').style.display='none';
                document.getElementById('bespoke').style.display='none'; 
                document.getElementById('custom').style.display='block';
            }
            function Msg4(){
                document.getElementById('bosvault').style.display='none'; 
                document.getElementById('custom').style.display='none';
                document.getElementById('bespoke').style.display='none'; 
                document.getElementById('bespoke').style.display='block';
            }
        </script>    

HTML;
require_once ('layouts/header.php')
?>

<div class="row" id="">
    <div class="col span-1-of-5">

        <input type="button" class="products" onclick="Msg1()" value="The BOS Vault">
        <input type="button" class="products" onclick="Msg2()" value="Broker Back Office">
        <input type="button" class="products" onclick="Msg3()" value="Custom BBO">
        <input type="button" class="products" onclick="Msg4()" value="Bespoke Systems">
        <!--<button class="products" type="button" onclick="document.getElementById('custom').style.display='block'">Custom BBO
    </button>

<button class="products" type="button" onclick="document.getElementById('bespoke').style.display='block'">Bespoke Systems
</button>-->

    </div>
    <div class="col span-4-of-5 animate" id="bosvault" style="display:block">
        <div class="">The BOS VAULT</div>
        <p class="">With the onset of the European Union Gerneral Data Protection Rules (EU GDPR) in May 2018, it became essential to communicate any sensitive data, documents and files across the internet using encryption to keep privacy at the core of any system you use. This includes email. It isn't widely understood that most email communications are the technical equivalent of sending a postcard through the normal post. Simply put, such communications can be read by others in transit! It was with this in mind that we developed a product called the Vault. </p>

        <p class="">The Vault uses a technique known as PGP (Pretty Good Privacy). PGP was developed in the 1990's by a man called Phil Zimmermann and has become the mainstay of private communications over the internet ever since. The beauty as with any great idea lies is in its simplicity. Here's how it works and to make it easy to understand we will use a physical world example.</p>

        <p class="" >Let's assume that you have a treasure map and I want you to send it to me. If you just send it via a courier someone could intercept it, open it, and find treasure without me ever getting your message. Not just the courier himself but anyone who handles the map between you and me could do this.</p>

        <p class ="">Worrying? Of course it is. <a href="">Click here for the BOS Vault solution.</a></p>

    </div>
    <div class="col span-4-of-5 animate" id="bbo" style="display:none">
        <div class="">Broker Back Office (BBO)<br class="no-small no-med"></div>

        <p class= "">BBO is a dedicated business management system specifically aimed at the international independent financial services sector (IFA's).</p>

        <p class= "">The system was developed for small IFA firms working in the offshore financial services arena. The idea that being cloud based, the cost of development and upkeep are shared with many small IFA's, thereby providing a useful tool without having to worry about the development time and cost to themselves. </p>

        <p class ="">Today BBO serves many IFA's across Europe, Africa, Asia and South America and continues to develop based on input and ideas from its customer base. BBO is the perfect platform for start-ups and companies with up to 20 staff.</p>
        <p class="">The system was conceived in the early 2000's but became the an Isle of Man based company in 2011 when BOSL became its sole owner. </p>

        <p class ="">If you would like to know more. <a href="">Click here for a short video presentation.</a></p>

    </div>
    <div class="col span-4-of-5 animate" id="custom" style="display:none">
        <div class ="bbo">Custom BBO</div>
        <p class= "">As the BBO client base became larger and more demanding we introduced the concept of Custom BBO. At the time, many of our clients were becoming multi-jurisdictional and with the onset of tighter regulations and different compliance procedures, needed their systems to be developed in such a way and to reflect their own individual way of working. Our solution to these growing demands was a simple one.<p/>

        <p class="">In 2014, and for a select few of our clients and the time, we took the backbone of the BBO system, placed it on a separate URL and then used it to develop some uniques systems based on individual corporate clients requirements. The custom BBO idea was so successful that more that half of our clients prefer this way of doing things. It is a perfect solution for companies that have between 20 and 75 staff.</p>

        <p>Even so, a few of our clients need even more than this and for those large companies we now develop Proprietary or "Bespoke" systems</p>

    </div>
    <div class="col span-4-of-5 animate" id="bespoke" style="display:none">
        <div class ="bbo no-med for-small">Bespoke systems<br class="no-med"></div>
        <p class= "">We are capable of building you pretty much anything you want so come and talk to us and we will give you come great ideas!
        </p>

        <a href="">........contact us here</a>


    </div>
</div>
<?php require_once ('layouts/footer.php'); ?>