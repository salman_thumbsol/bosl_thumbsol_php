<?php require_once ('register_action.php'); ?>
<?php $meta_title='BOSL Registration'; ?>
<?php  $current_page_name='register'; ?>
<?php require_once('layouts/header.php') ?>
<section class="row">
    <h2 style="text-align: center" >Register here for free access to the Vault</h2>
      <form class="profile" method="post" action="register.php">
          <?php if(isset($errors)){
              foreach($errors as $error){?>
                <p class="error"><?=$error ?></p>
            <?php }
            } ?>

          <?php if($success==1){?>
                Registration Successfull
          <?php } ?>
          <div>
              <div class="col span-1-of-3">
                    <p><label for="title">Title</label><br />
                    <input type="text" id="title" name="title" placeholder="Mr Mrs etc" value="<?=$title?>" required /></p>
              </div>
               <div class="col span-1-of-3">
                    <p><label for="firstname">First Name</label><br />
                        <input class="col 1-of-3" type="text" name="first-name" value="<?=$first_name?>" id="first-name" placeholder="First Name" required></p>
              </div>
              <div class="col span-1-of-3">
                    <p><label for="surname">Surname</label><br />
                    <input type="text" id="surname" name="surname" placeholder="Surname" value="<?=$surname?>" required></p>
              </div>
          </div>
          <div>
              <div class="col span-1-of-3">
                    <p><label for="email-primary">Email (Primary)</label><br />
                    <input type="email" id="email" name="email" placeholder="Email (Primary)" value="<?=$email?>" required>

                    </p>
              </div>
              <div class="col span-1-of-3">
                    <p><label for="email-primary">Email (Verify)</label><br />
                    <input type="text" id="confemail" name="confemail" onblur="confirmEmail()" value="<?=$confemail?>" placeholder="Re-type email to confirm" required/></p>
              </div>
              <div class="col span-1-of-3">
                    <p><label for="tel">Telephone</label><br />
                    <input type="tel" id="mobile" name="telephone"  placeholder="Telephone" value="<?=$telephone?>" required></p>
              </div>
          </div>

          <p><input type="checkbox" id="submitCheck" <?= $submit_check==''?'':'checked="checked"'?> name="submit_check" onclick="openSubmit()">  I confirm that I have read and agree to the Terms and Conditions, Cookie Policy and Privacy Policy.</p><br>
          <p><a href="verification.php">Resend Verification Email</a><br><br>
          <button class="submit-btn animate <?= $submit_check==''?'hide':''?>" id="submitBtn">Submit to Register</button><br>
    </form>
</section>
        
<!-- Tand C cookies and Privacy The Modals -->  
        
        <section class="row">
        
<!-- The T and C Modal First--> 
           
<!-- Trigger/Open The Modal -->
            <button class="submit-btn animate" id="tandcBtn">Terms and Conditions Etc</button>
            
<!--The Modal-->
                <div id="tandcModal" class="modal animate">

                <!-- The T and C Modal Content-->      

                <div class="row modal-content">
                    <span class="close">&times;</span>
                        <div class="col span-1-of-3>">
                            <h3>Terms and Conditions of Use</h3>
                        </div>
                        <div class="col span-1-of-3>">
                            <h3>Privacy Policy</h3>
                        </div>
                        <div class="col span-1-of-3>">
                            <h3>Cookies Policy</h3>
                        </div>
                    <div class="col span-3-of-3>">
                        <h4>"Applications"</h4>
                            <h5>means any applications developed, owned or made available by the Company for use on any Device;</h5>
                                <h4>"Device"</h4> 
                                    <h5>means any machine, equipment or electronic device of any description used for communication, to access the internet, or to store, access or process information including without limitation, computers, tablets, mobile phones, smartphones, iPhones and iPads; and</h5>
                                <h4>"Website"</h4>
                                    <h5>means <span class="http"></span>.</h5>
                                        <h5>In consideration of Back Office Solutions Ltd. (the "Company"), providing to you access to and use of the Company's Website and Applications you agree to and agree to be bound by the following terms and conditions and the Privacy Policy (the "Terms and Conditions") in respect of your access to the Website and Applications. If you do not agree to any of these Terms and Conditions please select the "I Decline" option on the Login page and leave or exit the Website and Applications. Your continued use of the Website and Applications will be deemed to be acceptance in full of these Terms and Conditions.</h5>
                                <h4>The Terms and Conditions are as follows:</h4>
                                    <h5>1. You will be registered to use the Website and Applications and a profile will be created for you and/or your business to facilitate your access to and use of the Website and Applications. In being registered and registering for use of the Website and Applications, you agree to provide true, accurate, current and complete information about you/your business as requested. Your profile, data and any registration information you provide to create your profile will be protected as described in our Privacy Policy.</h5>
                                        <h5>2. You confirm that you are the person to whom the log-in name and password in respect of this Website and Applications was issued. or a duly authorised representative of that person who has been authorised to access this Website and Applications on their behalf. You confirm that you will not authorise access or give your password to any third party unless you have obtained the written approval of the Company.  You undertake to keep the log-in name and password, issued to you in respect of this Website and Applications, confidential and not to disclose it to any third party. You undertake to the Company that you will notify us as soon as possible if such log-in name and password has been disclosed to any third party or if you become aware of a breach of security.  In the event the Company becomes aware that a third party not authorised to use your log in name and password has logged into your account, we reserve the right to cancel your account and take necessary action against you as may be permitted by law.  You agree to indemnify the Company and hold the Company harmless against any losses, claims, damages, costs that may be suffered by the Company by the use of your account by a third party.</h5>
                                            <h5>3. You must not use our Website and Applications, in any way which is unlawful, illegal, fraudulent or harmful, or in connection with any unlawful, illegal, fraudulent or harmful purpose or activity, or in any manner inconsistent with the Terms and Conditions.</h5>
                                            <h5>4. The material provided through our Website and Applications, is not intended as investment advice or an offer or solicitation to buy, sell or provide investment products, investment services or any goods or services in which we may trade or have an interest. The information provided through Website and Applications, is for indicative information purposes only and you should consult your legal, tax, accounting, investment or other professional adviser before taking any action, decision or course of action.</h5>
                                            <h5>5. Access to and use of information contained in our Website and Applications may be prohibited in certain jurisdictions and our Website and Applications should not be accessed and the information contained on our Website and Applications should not be used in any jurisdiction where such prohibitions apply. You must ensure that the use of the material and information on our Website and Applications does not contravene any such prohibitions and it is your responsibility to be informed and to observe all applicable laws and regulations of any relevant jurisdiction before using or, as appropriate, continuing to use our Website and Applications. You agree and undertake to defend, indemnify and keep the Company, its affiliates, directors, officers, employees and agents indemnified from and against all losses, claims, actions, demands, liabilities, costs, expenses and damages of any nature whatsoever and whether or not reasonably foreseeable or avoidable, caused by, or in connection in any way with any contravention of any applicable laws or regulations.</h5>
                                            <h5>6. While all reasonable efforts have been made to ensure that the information on our Website and Applications is accurate at the time it is made available, such information is provided on an "as is", "where is" and "where available" basis for indicative purposes only, without warranty of any kind, either express or implied, including but not limited to warranties of title or implied warranties of satisfactory quality or fitness for a particular purpose or otherwise, other than those warranties which are implied by and incapable of exclusion, restriction, or modification under applicable law.</h5>
                                            <h5>7. The Company does not accept responsibility for updating information and material contained on our Website and Applications and therefore it should not be assumed that the information contained will necessarily be accurate, complete or up-to-date at any point in time. Users should independently verify the factual accuracy, assumptions, calculations or completeness of all statements and material contained on our Website and Applications. You should not rely on the accuracy of any information on our Website and Applications and if you require up-to-date, accurate information you should contact the Company or individual responsible for the accuracy of the information.</h5>
                                            <h5>8. Access to and use of our Website and Applications is at the users' sole risk. As protecting your data is of utmost importance to us, the Company has adopted high levels of security and encryption with the goal of effectively protecting your data. However, data, including any personal data transmitted through the internet may be subject to interference from third parties and we cannot guarantee the security of data or information accessed by way of our Website and Applications.</h5>
                                            <h5>9. The Company aims to adopt practices to ensure round the clock availability and access to the Website and Applications. However, the Company does not represent or warrant that the Website and Applications will be available and meet your requirements, that access will not be interrupted, that there will be no delays, failures, errors or omissions or loss of transmitted information, that no viruses or other contaminating or destructive properties will be transmitted or that no damage will occur to your computer system. You have sole responsibility for adequate protection of data and/or equipment and for undertaking reasonable and appropriate precautions to scan for computer viruses or other destructive properties.</h5>
                                            <h5>10.Subject to any applicable laws, in light of the foregoing warnings you acknowledge and agree that the Company, its affiliates, any of its respective directors, officers, employees and agents will not be liable in any way to you or to any third party:</h5>
                                            <h5>10.1 for any losses, claims, actions, demands, liabilities, costs, expenses and damages of any nature whatsoever and whether or not reasonably foreseeable or avoidable, caused by and which arises out of and in connection with, in any way, your use of the Website and Applications and/or the information contained on the Website and Applications and referred to in the Website and Applications, including without limitation, for breach of contract, in tort, for negligence, under an indemnity, for breach of warranty or otherwise; and</h5>
                                            <h5>10.2 for any consequential, exemplary, indirect or special damages including, without limitation, loss of profit, loss of goodwill or reputation, loss of business, loss of revenue, capital expenditure or otherwise resulting from any claim (including, and without limitation, a claim for breach of contract, in tort, for negligence under an indemnity, for breach of warranty or otherwise) related to your use of the Website and Applications and/or any of the information contained on it or referred to in it; and</h5>
                                            <h5>10.3 for any loss, claim, action, demand, liability, cost, expense and damage of any nature whatsoever and whether or not reasonably foreseeable or avoidable, which arises out of and in connection with, timelines, accuracy or completeness of the information or material provided in the Website and Applications; and</h5>
                                            <h5>10.4 for any loss, claim, action, demand, liability, cost, expense and damage of any nature whatsoever and whether or not reasonably foreseeable or avoidable, which arises out of and in connection with, any breach of security, data and loss of confidentiality and data.</h5>
                                            <h5>11. You agree that all warranties, conditions, terms, undertakings and representations express or implied, statutory or otherwise, are hereby excluded to the fullest extent permitted by law. The contractual rights which you enjoy by virtue of the Sale of Goods Act, 1979 (as amended) and the Supply of Goods and Services Act, 1982 (as amended) are in no way prejudiced by anything contained in these Terms and Conditions, save to the fullest extent permitted by law.</h5>
                                            <h5>12. You acknowledge that in accessing the Website and Applications and the information contained or referred to in the Website and Applications, you are acting in the course of your business, trade or profession and that you are not acting as a "consumer" as such term is defined in any applicable laws including (without limitation) the Sale of Goods Act, 1979 (as amended) the Supply of Goods and Services Act, 1982 (as amended), the Unfair Contract Terms Act ,1977 (as amended), the European Communities (Unfair Terms Consumer Contracts) Regulations, 1995 or otherwise.</h5>
                                            <h5>13. You agree, at your own expense, to defend, indemnify and keep the Company, its affiliates and their respective directors, officers, employees and agents indemnified from and against all losses, claims, actions, demands, liabilities, costs, expenses and damages of any nature whatsoever and whether or not reasonably foreseeable or avoidable incurred by us, caused by, and in connection in any way with:</h5>
                                            <h5>13.1 your access to the Website and Applications, or any links on the Website and Applications and/or your use of the information contained or referred to in the Website and Applications and any customer and client of yours that uses the Website and Applications;</h5>
                                            <h5>13.2 the access to the Website and Applications by any third party using your log-in name and password and the use by any third party of any information contained or referred to in the Website and Applications obtained, directly or indirectly, from you and/or by using your log-in name and password;</h5>
                                            <h5>13.3 your failure and any of your customers' and clients' failure to comply with any of the Terms and Conditions set out herein;</h5>
                                            <h5>13.4 a claim that any use of the Website and Applications by you or someone using your log-in name and password infringes any copyright, trademark or other intellectual property right of any third party;</h5>
                                            <h5>13.5 any deletions, additions, insertions or alterations to, or any unauthorised use of, the Website and Applications by you or someone using your log-in name and password; and</h5>
                                            <h5>13.6 any misrepresentation or breach of representation, warranty or covenant made by you contained herein;
                                            For the avoidance of doubt, you also agree to pay any and all costs, damages and expenses (including reasonable attorneys' fees) and costs awarded against, incurred by, in connection with access and arising from any loss, claim, action, demand, liability, cost, expense and damage of any nature whatsoever and whether or not reasonably foreseeable or avoidable in connection with the indemnities in this clause.</h5>
                                            <h5>14. You acknowledge and agree that the Website and Applications are owned and controlled by the Company, except as otherwise expressly stated, and these are protected by copyright laws and international treaties. You acknowledge and agree that all intellectual property rights (including, without limitation copyright, patents, database rights, trademarks, service marks, designs, know-how, trade secrets and other proprietary rights) which subsist in the Website and Applications and/or the information referred to in the Website and Applications is and will remain the Company's property (or the property of our licensors) and your right to use it is limited to use for your own internal business purposes and you have no right to copy it, sub-licence it, disclose it to third parties, transfer, link to, reproduce, frame, alter, create derivative works or republish all or any portion of the Website and Applications for any commercial or public purpose without the Company's written consent. You further acknowledge and agree that you do not acquire any ownership rights by downloading copyrighted material.</h5>
                                            <h5>15. The Website and Applications may, from time to time, contain links to other web sites. The Company is not responsible for the privacy policies or content available on any other websites linked to the Website and Applications. Access to and use of such other websites is at your own risk and subject to any terms and conditions applicable to such access/use. You should satisfy yourself as to the identity of the operators of those sites and review the terms and conditions, privacy policies and disclaimers of such sites when you visit them. By providing hyperlinks to other websites, the Company shall not be deemed to endorse, recommend, approve, guarantee or introduce any third parties or the service/products they provide on their website, or to have any form of cooperation with such third parties and websites. The Company makes no representations or warranties regarding the accuracy, functionality or performance of any third party software that may be used in connection with the Website and Applications.</h5>
                                            <h5>16. Your access to the Website and Applications will terminate immediately without notice upon the occurrence of any of the following events:</h5>
                                            <h5>A) if the Company reasonably determines, that you have failed to comply with any provision of the Terms and Conditions, or</h5>
                                            <h5>B) in the event of non-payment of any fees owed to the Company.</h5>
                                            <h5>The Company reserves the right to terminate the Website and Applications or your access to it in the above mentioned situations only without giving you any notice thereof. You agree that you will not be entitled to any compensation in the above mentioned situations for any such termination including, without limitation for any loss, claim, action, demand, liability, cost, expense and damage of any nature whatsoever and whether or not reasonably foreseeable or avoidable, including without limitation, any loss of profits, loss of goodwill or reputation, loss of business, loss of revenue, capital expenditure or otherwise.</h5>
                                            <h5>17. All of these Terms and Conditions are distinct and severable and if any of these Terms and Conditions are held unenforceable, illegal or void in whole or in part by any court, regulatory authority or other competent authority, it will to that extent be deemed not to form part of these Terms and Conditions and the enforceability, legality and validity of the remainder of these Terms and Conditions will not be affected.</h5>
                                            <h5>18. These Terms and Conditions may be amended, modified or varied at any time and from time to time by us by giving you 60 days' notice. Your use of the Website and Applications, or any use of the information or material on the Website and Applications after the expiration of the 60 day notice period from the posting of amendments, modifications or variations to the Terms and Conditions will constitute your acceptance of the Terms and Conditions as amended, modified or varied. If, at any time, you do not wish to accept the Terms and Conditions, please notify the Company within the 60 day notice period and we will take the necessary steps to terminate your services and access to the Website and Applications.. The Company also reserves the right to modify the Website and Applications at any time and from time to time at the Company's sole discretion.</h5>
                                            <h5>19. These Terms and Conditions supersede all prior representations, arrangements, understandings and agreements between you and the Company relating to your access to the Website and Applications and use of the information contained or referred to in it and the Terms and Conditions set forth the entire, complete and exclusive agreement and understanding between you and the Company relating to such access and use. You acknowledge and confirm that you have not relied on any representation, arrangement, understanding or agreement (whether written or oral) not expressly set out in these Terms and Conditions.</h5>
                                            <h5>20. You may not assign, sub-licence, encumber or otherwise deal in your right to access the Website and Applications and use of the information contained or referred to in it, without the Company's prior written consent.</h5>
                                            <h5>21. The use of the Website and Applications, these Terms and Conditions and all relationships created by them shall be governed by the laws of England and Wales and you agree that the courts of England and Wales shall have non-exclusive jurisdiction to determine any matter or dispute in connection or arising out of the Website and Applications and these Terms and Conditions or their performance.</h5>
                                            <h5>22. You confirm that you have read and understood these Terms and Conditions and, where you believe necessary, have received independent legal or other professional advice in respect of them.</h5>-->
                            <h5>23. You agree that the exclusions and limitations of liabilities set out in the Terms and Conditions are fair and reasonable. If you do not accept them, please select the "I Decline" option below and please cease using the Website and Applications and the information contained or referred to in the Website and Applications immediately.</h5> 
                            <h3 id="cookies">Cookies Policy</h3>
                            <h4>What is a Cookie?</h4><h5>A cookie is a small amount of data, which often includes a unique identifier that is sent to your computer or mobile phone browser from a website's computer and is stored on your device's hard drive. Cookies perform a variety of functions, from enabling certain functionalities on a website to recording information about your online preferences and allowing us to tailor the website to your interests. We use a combination of cookies to help us provide you with a personalised service, and also to help make our website, products and services better for you. The cookies used on our Website and Applications have been categorised on the basis of the categories found in the ICC UK Cookie guide issued in April 2012, and are explained in detail below.</h5>

                            <h4>Cookies Used on our Website</h4><h5>We may use any of the following different types of cookies on our Website and Applications from time to time:</h5>

                            <h4>Strictly Necessary Cookies</h4><h5>These cookies are essential in order to enable you to move around our Website and Applications and use their features, such as accessing secure areas of our Website and Applications. Without these cookies, services you have asked for cannot be provided.</h5>

                            <h4>Performance Cookies</h4><h5>These cookies collect information about how visitors use our Website and Applications, for instance which pages visitors go to most often, and if they get error messages from web pages. These cookies don't collect information that identifies a visitor. All information these cookies collect is aggregated and therefore anonymous. They are only used to improve the way our Website and Applications work.</h5>

                            <h4>Functionality Cookies</h4><h5>These cookies allow our Website and Applications to remember choices you make (such as your user name, language or the region you are in) and provide enhanced, more personal features. For instance, a website may be able to provide you with local weather reports or traffic news by storing in a cookie the region in which you are currently located. These cookies can also be used to remember changes you have made to text size, fonts and other parts of web pages that you can customise. They may also be used to provide services you have asked for such as watching a video or commenting on a blog. The information these cookies collect may be anonymised and they cannot track your browsing activity on other websites.</h5>
							
							<h4>Analytical Cookies</h4><h5>These cookies allow us to recognise and count the number of visitors to our Website and Applications, and to see how visitors move around the Website and Applications, when they're using them. This helps us to improve the way our Website and Applications work.The most common form of using analytical cookies is when Google Analytics tracking uses cookies in order to provide meaningful reports about site visitors. However, Google Analytics cookies do not collect personal data about website visitors.</h5>

							<h4>Third Party Content</h4><h5>Our Website and Applications may contain third party cookies and content in addition to cookies placed by us, and these are discussed below:</h5>

							<h4>Third Party Targeting Cookies or Advertising Cookies</h4><h5>These cookies are used to deliver adverts more relevant to you and your interests. They are also used to limit the number of times you see an advertisement as well as help measure the effectiveness of the advertising campaign. They are usually placed by advertising networks with the website operator's permission. They remember that you have visited a website and this information is shared with other organisations such as advertisers. Quite often targeting or advertising cookies will be linked to site functionality provided by the website operator.</h5>

							<h4>Third Party Websites</h4><h5>We sometimes embed content and sharing tools from other third party websites in our Website and Applications, and if so, you may be presented with cookies from those third party sites. We do not control these third party cookies. You should check the relevant third party website(s) for more information about these cookies and how you may disable them.</h5>

							<h4>How to Manage your Cookies</h4><h5>Most web browsers allow some control of most cookies through the browser settings. To find out more about cookies, including how to see what cookies have been set on our Website and Application and how to manage your preferences regarding the cookies you want to retain or delete please visit the following website which will provide you with detailed information regarding how to opt out of using cookies: www.allaboutcookies.org.</h5><h5>Additional information regarding cookies and opting-out is available at www.youronlinechoices.co.uk and www.allaboutcookies.org</h5>

       
        
                <h3 id="privacy">Privacy Policy</h3>
                            <h5>Back Office Solutions Ltd. (the "Company", "we") respects your concerns about privacy. The Company is not acting as a "data controller" but as a “data processor” as such term is defined in the REGULATION (EU) 2016/679 and has compiled this Privacy Policy to set out the principles that govern the use of your personal data. By using the Website and Applications, you agree to the principles set out below.</h5>
                            <h4>"Applications"</h4> <h5>means any applications developed, owned or made available by the Company for use on any Device.</h5>
                            <h4>"Device"</h4><h5>means any machine, equipment or electronic device of any description used for communication, to access the internet, or to store, access or process information including without limitation, computers, tablets, mobile phones, smartphones, iPhones and iPads; and
                            "Website" means <span class="http"></span></h5>

                            <h4>1. The Information We Collect and How We Use It</h4>

                            <h5>The information gathered by the Company from the Website and Applications falls into two categories: Tracking information gathered as users navigate through the Website and Applications, and Information voluntarily supplied to us by users and visitors to the Website and Applications.</h5>

                            <h5>When you register and use the Website and Applications, our web server will record users' IP addresses whichis standard procedure for any web server. Certain data such as which pages users of the website access or visit and information voluntarily given by users such as email addresses will be stored and used to administer the Website and Applications and otherwise constantly improve our content by tracking group information that describes the habits, usage patterns and demographics of our customers. None of this information will be revealed to any third party without your consent.</h5>

                            <h4>2. Our Use of Cookies and Other Information Gathering Technologies</h4>

                            <h5>In order to analyse site traffic and to provide personalised services, we make use of cookies. (See Cookies Policy for details)</h5>

                            <h4>3. How We Protect Your Information</h4>

                            <h5>The Company respects your right to privacy on the internet and will not sell or make available in any way individually identifiable personal information without your permission. However, no data transmission over the internet can be guaranteed to be totally secure and therefore we cannot ensure or warrant the security of any information you transmit to the Website and Applications.</h5>

                            <h5>You transmit any information via the Website and Applications at your own risk and you will not hold the Company or any of its representatives responsible for any breach of security or for any damages you or others may suffer as a result of the loss of confidentiality of any such information.</h5>

                            <h5>You agree that the Company, its affiliates, any of its respective directors, officers, employees and agents will not be liable in any way to you or to any third party for any losses, claims, actions, demands, liabilities, costs, expenses and damages of any nature whatsoever and whether or not reasonably foreseeable or avoidable, caused by, and which arises out of and in connection with a breach of this Privacy Policy and Cookies Policy.</h5>

                            <h5>The Company will disclose your personal information as necessary to: (a) comply with a court order or other legal process; (b) protect our rights or property; or (c) enforce our Terms and Conditions, Privacy Policy and Cookies Policy.</h5>

                            <h4>4. Third Party Websites</h4>

                            <h5>The Website and Applications may contain links to certain third party websites. The Company is not responsible for the privacy policies, cookies policies and content of these websites. It is your responsibility to read and understand the privacy policy, cookies policy and terms of the operator of the third party websites you visit.</h5>

                            <h4>5. Your Consent</h4>

                            <h5>If you do not agree to us processing your information then please select the "I Decline" option below and leave or exit the Website and Applications. By selecting “I Accept”  or by submitting your information you consent to the use of that information as set out in this policy. You are responsible for checking this page periodically for changes and updates to the Privacy Policy and/or Cookies Policy. Your use of the Website and Applications following any posted changes to the Privacy Policy and/or Cookies Policy will be deemed an acceptance of such changes.</h5>

                            <h4>6. Protection of Children’s Privacy</h4>

                            <h5>Our Website and Applications is not directed to individuals under the age of eighteen (18), and we request that these individuals do not provide personal data through our Website and Applications. We do not knowingly collect information from children under 18 without parental consent. </h5>

                            <h4>7. Your choices regarding your information</h4>

                            <h5>You may request that we delete your data at any time by contacting us using the form opposite or send an e-mail to admin@back-office-solutions.im. If you would like to access or have a copy of the personal information we hold about you, please contact us at and please provide as much information as you can about the information you are looking for so that we can deal with your request as quickly as possible. </h5>

                            <h4>8. Updating your details</h4>

                            <h5>If any of the information that you have provided changes, for example if you change your e-mail address, name or payment details or any other information provided by you or if you have any concern that the personal or account information maintained on the Website and Applications is incorrect, please contact us using the form opposite or send an e-mail to admin@back-office-solutions.im and we will review and update our records.</h5>

                            <h4>10. How to Contact the Company</h4>

                            <h5>We welcome your views about our Privacy Policy. If you would like to contact us with any queries or comments please send an e-mail to admin@back-office-solutions.im.</h5>  
                    </div>
                    </div>
                </div>
                <div id="privacyModal" class=modal>

                <!-- The Privacy Modal Content-->      

                <div class="modal-content">
                    <span class="close">&times;</span>
                    <h3>Privacy Policy</h3>
                    </div>
            </div>

        </section>


<?php
    $footer_js=<<< HTML
    <script>
function openSubmit() {
    // Get the checkbox
    var submitCheck = document.getElementById("submitCheck");
    // Get the output element
    var submitBtn = document.getElementById("submitBtn");

    // If the checkbox is checked, display the output checkBox
    if (submitCheck.checked == true){
        submitBtn.style.display = "block";
    } else {
        submitBtn.style.display = "none";
    }
    }
    // Get the modal
    var modal = document.getElementById('tandcModal');
    
    // Get the button that opens the modal
    var btn = document.getElementById("tandcBtn");
    
    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];
    
    // When the user clicks the button, open the modal 
    btn.onclick = function() {
        modal.style.display = "block";
    }
    
    // When the user clicks on <span> (x), close the modal
    span.onclick = function() {
        modal.style.display = "none";
    }
    
    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    };
    
    function confirmEmail() {
        var email = document.getElementById("email").value;
        var confemail = document.getElementById("confemail").value;
        if(email != confemail) {
            alert('Error: Email Addresses do not Match!!');
            document.getElementById("confemail").value='';
        }
    }
    </script>

HTML;
?>

<?php require_once('layouts/footer.php') ?>