<?php  $current_page_name='gdpr'; ?>
<?php require_once ('layouts/header.php') ?>
        <section class="row">
            <div class="col span-1-of-5"><h2>See how the BOS Vault can encrypt all all your sensitive correspondence.</h2></div>
                
            <div class="col span-4-of-5">
            <h3>EU GDPR Compliance</h3>
                <p>Compliance of the European Union General Data Protection Rules is required by any organisation that either does business in the EU or collects, processes and stores personal information of EU citizens. The Rules became enforceable in May 2018 and are more stringent than preceding legislation and compliance is compulsory, each member state does not have to ratify them into its own law.</p>
                <p>The Rules are not that prescriptive in terms of the technology controls required, rather stating that appropriate organisational and technological controls must be in place to protect sensitive data. Encryption is of course essential but organisations must understand their business operations and their data movement to best determine which controls, either technical or procedural can deliver the most effective method for ensuring compliance.</p>
            <h3>Privacy by Design</h3>
                <p>The Rules also introduce the concept of "Privacy by Design". This should ensure that all strategies and controls put in place for ensuring compliance, must take the need for data privacy at the core of the system design. Whilst not an exhaustive list companies need to appoint a Data Protection Officer to ensure that everyone in their organisation understands their responsibilities regarding the use of personal data. They must also and keep a Breach Register to record any breaches of the Rules. The Rules define the difference between Data Processors (system designers and providers of software) and Data Controllers (those that use the data within the systems).</p>
            <h3>Controllers and Processors</h3> 
                <p>The Rules define the difference between Data Processors (system designers and providers of software) and Data Controllers (those that use the data within the systems). Back Office Solutions Ltd (BOSL) is by definition, a Data Processor. The responsibility of BOSL is to ensure that the technical controls are in place such as encryption, firewalls, password security etc and Data Controllers are the individual users within our client companies. It is important that each user understands their responsibilites under the Rules to keep client data safe and then act accordingly when using our systems.</p>
                
            
            </div>
        
        </section>
<?php require_once ('layouts/footer.php') ?>