<?php
if(!isset($_SESSION)){
    session_start();
}
require_once ('../config.php');

$action=$_REQUEST['action'];

$message_obj=new Message();
if($action=='send_vault_message'){
    $folder_id=$_POST['folder_id'];
    $body=$_POST['message'];
    $user_id=$_SESSION['user_id'];
    $document=null;
    if(!empty($_FILES['file'])){
        // File path configuration
        $targetDir = dirname(__FILE__)."/../uploads/";
        $path = $_FILES['file']['name'];
        $path_details=pathinfo($path);
        $ext = pathinfo($path, PATHINFO_EXTENSION);
        $fileName=date('YmdHis').'.'.$ext;
        $targetFilePath = $targetDir.$fileName;
        // Upload file to server
        if(move_uploaded_file($_FILES['file']['tmp_name'], $targetFilePath)){
            $document=$fileName;
        }
    }
    $id= $db->insert(['folder_id'=>$folder_id,'message'=>$body,'document_path'=>$document,'created_at'=>date('Y-m-d H:i:s'),'sender_id'=>$user_id],'folders_items');
    $message_obj->setParticipantDetails($id);
    //$message_obj->markloaded($user_id,$id);
    $message_obj->markread($user_id,$id);
    //$message=$db->fetchRow('SELECT folders_items.*,entity.`firstname`,entity.surname FROM folders_items INNER JOIN entity ON folders_items.sender_id=entity.entity_id  WHERE folders_items.id='.$id);

    //ob_start();
    //include('../modules/people/broker/views/_message.php');
    //$html = ob_get_contents();
    // ob_end_clean();

    echo json_encode(['html'=>$html,'message'=>'','status'=>'ok']);exit();
}

if($action=='refresh_message'){
    $folder_id=$_POST['folder_id'];
    $user_id=$_SESSION['user_id'];
    $sql="SELECT fi.*,fd.mesage_read,fd.hide,e.firstname,e.surname FROM folders_items fi INNER JOIN folders_items_details fd ON fi.id=fd.folder_item_id ";
    $sql.="INNER JOIN entity e ON fi.sender_id=e.entity_id WHERE fi.folder_id=$folder_id  AND fd.user_id=$user_id AND loaded=0 and fd.hide=0";
    $messages=$db->fetchRows($sql);

    $html='';

    if($messages) {
        $html ='';
        foreach ($messages as $message) {
            $message_obj->markloaded($user_id,$message['id']);           
            ob_start();
            include('../modules/vault/contact/views/_message.php');
            $html .= ob_get_contents();
            ob_end_clean();
        }
        echo json_encode(['html'=>$html, 'status'=>'ok']);exit();
    }else{
        echo json_encode(['html'=>'','status'=>'failed']);exit();
    }
}

if($action=='refresh_message_counter'){
    $user_id=$_SESSION['user_id'];
    $sql="SELECT COUNT(*) AS c,folder_id,(SELECT COUNT(*) FROM folders_items_details ";
    $sql.="WHERE mesage_read=0 AND user_id=".$user_id." AND folder_id=fd.`folder_id`)AS unread ,(SELECT COUNT(*) FROM folders_items_details ";
    $sql.="WHERE hide=1 AND user_id=".$user_id." AND folder_id=fd.`folder_id`)AS hide FROM folders_items_details fd ";
    $sql.="WHERE user_id=".$user_id." GROUP BY folder_id";

    $folders_items=$db->fetchAll($sql);
    echo json_encode(['status'=>'ok','counters'=>$folders_items]);exit();
}

if($action=='reply'){
    $parent_id=$_POST['parent_id'];
    $message=$_POST['message'];
    $user_id=$_SESSION['user_id'];
    $folder_id=$_POST['folder_id'];
    $id= $db->insert(['folder_id'=>$folder_id,'message'=>$message,'created_at'=>date('Y-m-d H:i:s'),'parent_id'=>$parent_id,'sender_id'=>$user_id],'folders_items');

    $message_obj->setParticipantDetails($id);
    $message_obj->markloaded($user_id,$id);  
    $message_obj->markread($user_id,$id);    

    $sql="SELECT fi.*,fd.mesage_read,fd.hide,e.firstname,e.surname FROM folders_items fi INNER JOIN folders_items_details fd ON fi.id=fd.folder_item_id ";
    $sql.="INNER JOIN entity e ON fi.sender_id=e.entity_id WHERE fi.folder_id=$folder_id  AND fd.user_id=$user_id and fi.id=$id AND fd.hide=0 order by id desc";
    $message=$db->fetchRow($sql);
    ob_start();
    include('../modules/vault/contact/views/_message.php');
    $html = ob_get_contents();
    ob_end_clean();
    echo json_encode(['message'=>$html,'status'=>'ok']);exit();
}

if ($action=='refresh_vault_message') {
    $contact_id=$_POST['id'];
    $user_id=$_SESSION['user_id'];

    $sql="SELECT fi.*,fd.mesage_read,fd.hide,e.firstname,e.surname FROM folders_items fi INNER JOIN folders_items_details fd ON fi.id=fd.folder_item_id ";
    $sql.="INNER JOIN entity e ON fi.sender_id=e.entity_id WHERE  fd.user_id=$user_id AND fd.loaded=0 AND fd.hide=0 order by id desc";
    $messages=$db->fetchAll($sql);

    $html='';
    $items=[];
    if (count($messages)>0) {
        foreach ($messages as $message) {
            $message_obj ->markloaded($user_id, $message['id']);
            ob_start();
            include('../modules/people/broker/views/_message.php');
            $html .= ob_get_contents();
            $message['html']=$html;
            ob_end_clean();
            $items[]=$message;
        }
        echo json_encode(['html' => $html,'items'=>$items, 'status' => 'ok']);exit();
    }
    echo json_encode(['html'=>'','status'=>'failed']);exit();
}

if ($action=='mark_read') {
    $message_id=$_POST['id'];
    $message=$message_obj->getMessage($message_id);
    $user_id=$_SESSION['user_id'];
    if ($message) {
        $message_obj ->markread($user_id, $message_id);
        echo json_encode(['status'=>'ok']);exit();
    }
    echo json_encode(['status'=>'failed']);exit();
}

if ($action=='mark_unread') {
    $message_id=$_POST['id'];
    $message=$message_obj->getMessage($message_id);
    $user_id=$_SESSION['user_id'];
    if ($message) {
        $message_obj ->markunread($user_id, $message_id);
        echo json_encode(['status'=>'ok']);exit();
    }
    echo json_encode(['status'=>'failed']);exit();
}

if ($action=='hide') {
    $message_id=$_POST['id'];
    $user_id=$_SESSION['user_id'];
    $message=$message_obj->getMessage($message_id);
    if ($message) {
        $message_obj ->hidemessage($user_id, $message_id);
        echo json_encode(['status'=>'ok']);exit();
    }
    echo json_encode(['status'=>'failed']);exit();
}

if ($action=='show') {
    $message_id=$_POST['id'];
    $user_id=$_SESSION['user_id'];
    $message=$message_obj->getMessage($message_id);
    if ($message) {
        $message_obj -> showmessage($user_id, $message_id);
        echo json_encode(['status'=>'ok']);exit();
    }
    echo json_encode(['status'=>'failed']);exit();
}

if ($action=='getContactMessages') {
    $folder_id=$_GET['folder_id'];
    $entity_id=$_GET['entity_id'];
    $limit=$_GET['limit'];
    $user_type=$_GET['user_type'];
    $last_id=null;
    $hide=0;
    if(isset($_GET['last_id']) && $_GET['last_id']!=''){
        $last_id=$_GET['last_id'];
    }
    if(isset($_GET['hide']) && $_GET['hide']!=''){
        $hide=$_GET['hide'];
    }
    $user_id=$_SESSION['user_id'];

    $sql="SELECT fi.*,fd.mesage_read,fd.hide,e.firstname,e.surname FROM folders_items fi INNER JOIN folders_items_details fd ";
    $sql.="ON fi.id=fd.folder_item_id INNER JOIN entity e ON fi.sender_id=e.entity_id WHERE fi.folder_id=".$folder_id."  AND fd.hide=".$hide." AND fd.user_id=".$user_id;
    if($last_id!=null) {
        $sql .= " AND fi.id<" . $last_id;
    }
    $sql.=" ORDER BY fi.id DESC LIMIT ".$limit;
    $messages=$db->fetchRows($sql);
    $html='';
    if($messages) {
        $html ='';
        foreach ($messages as $message) {
            $message_obj->markloaded($user_id,$message['id']);
            ob_start();
            if($user_type=='contact'){
                include('../modules/vault/contact/views/_message.php');
            }else if($user_type=='broker') {
                include('../modules/people/broker/views/_message.php');
            }else if($user_type=='co-broker') {
                include('../modules/people/co-broker/views/_message.php');
            }
            $html .= ob_get_contents();
            ob_end_clean();
        }
        echo json_encode(['html'=>$html,'status'=>'ok']);exit();
    }else{
        echo json_encode(['html'=>'','status'=>'failed']);exit();
    }
}