<?php
if(!isset($_SESSION)){
    session_start();
}
require_once ('../config.php');
//require('api.php');
// define the table
$table = "entity";
$primaryKey = "entity_id";
// call new instant of class
//$contact = new api;

//$result = $contact->insert($table);

//echo $result;

$action=$_REQUEST['action'];

if($action=='get'){
    if(isset($_POST['id'])) {
        $id=$_POST['id'];
        $parent_id=$_SESSION['user_id'];
        $user=$db->fetchRow('select * from entity where entity_id='.$parent_id);
        if($user && $user['role_id']==15){
            $parent_id=$user['parent_id'];
        }

        $row=$db->fetchRow("select * from entity where parent_id=$parent_id and entity_id=".$id);
        if($row){
            unset($row['password']);
            unset($row['password_hash']);
            ob_start();
            if($user && $user['role_id']==15){
                include('../modules/people/co-broker/controllers/edit_contact.php');
                include('../modules/people/co-broker/views/edit_contact.php');
            }else{
                include('../modules/people/broker/controllers/edit_contact.php');
                include('../modules/people/broker/views/edit_contact.php');
            }
            $content = ob_get_contents();
            ob_end_clean();
            echo $content;exit();
        }
    }
    echo 'failed';exit();
}
if($action=='insert'){

    if(isset($_POST['email'])) {
        $email = $_POST['email'];
        $user=$db->fetchRow("select * from entity where email='".$email."'");
        if($user){
            echo 'failed';exit();
        }
        $title = $_POST['title'];
        $time_zone=$_POST['time_zone'];
        $first_name = $_POST['firstname'];
        $surname = $_POST['surname'];
        $mobile = $_POST['mobile'];
        $user_id=$_SESSION['user_id'];
        $user=$db->fetchRow('select * from entity where entity_id='.$user_id);

        $parent_id=$user_id;
        $clients_notes = $_POST['clients_notes'];
        $data = ['title' => $title,'time_zone'=>$time_zone, 'firstname' => $first_name, 'surname' => $surname, 'email' => $email, 'mobile' => $mobile, 'clients_notes' => $clients_notes, 'role_id'=>10,'parent_id'=>$_SESSION['user_id'],'verify_date'=>date('Y-m-d H:i:s')];
        if($user){
            if($user['role_id']==15){
                $data['parent_id']=$user['parent_id'];
                $data['created_by']=$user_id;
                $parent_id=$user['parent_id'];
            }
        }
        $id=$db->insert($data, 'entity');
        $row=$db->fetchRow('select * from entity where entity_id='.$id);
        $folder_id = $db->insert(['name'=>'Messages','user_id'=>$id,'created_at'=>date('Y-m-d H:i:s'),'is_default'=>1],'folders');
        $db->insert(['folder_id'=>$folder_id,'user_id'=>$id,'created_at'=>date('Y-m-d H:i:s')],'user_folders');
        $db->insert(['folder_id'=>$folder_id,'user_id'=>$parent_id,'created_at'=>date('Y-m-d H:i:s')],'user_folders');
        $co_brokers=$db->fetchAll('select * from entity where parent_id='.$parent_id.' and role_id=15 ');
        foreach( $co_brokers as $co_broker){
            $db->insert(['folder_id'=>$folder_id,'user_id'=>$co_broker['entity_id'],'created_at'=>date('Y-m-d H:i:s')],'user_folders');
        }
        $folders=$db->fetchAll("SELECT * FROM `folders` WHERE user_id=".$parent_id." AND permission='apply_to_all' " );
        foreach($folders as $folder){
            $db->insert(['folder_id'=>$folder['id'],'user_id'=>$id,'created_at'=>date('Y-m-d H:i:s')],'user_folders');
        }
        require_once ('../modules/people/broker/views/_contact.php');
    }

}

//update function
if($action=='update')
{

    if(isset($_POST['entity_id'])) {
        $entity_id=$_POST['entity_id'];
        $parent_id=$_SESSION['user_id'];

        $user=$db->fetchRow('select * from entity where entity_id='.$parent_id);

        if($user){
            if($user['role_id']==15){
                $parent_id=$user['parent_id'];
            }
        }

        $row=$db->fetchRow('select * from entity where parent_id='.$parent_id.' and entity_id='.$entity_id);

        if($row){
            $title = $_POST['title'];
            $time_zone=$_POST['time_zone'];
            $first_name = $_POST['firstname'];
            $surname = $_POST['surname'];
          //  $email = $_POST['email'];
            $mobile = $_POST['mobile'];
            $clients_notes = $_POST['clients_notes'];

            $db->update(['title' => $title,'time_zone'=>$time_zone, 'firstname' => $first_name, 'surname' => $surname, 'mobile' => $mobile, 'clients_notes' => $clients_notes], 'entity',['entity_id'=>$entity_id]);

            require_once ('../modules/people/broker/views/_contact.php');
        }else{
            echo 'failed';exit();
        }
    }
}
//end update function


if($action=='send_access'){
    if(isset($_GET['id']) && is_numeric($_GET['id'])) {
        $user_id = $_GET['id'];
        $parent_id=$_SESSION['user_id'];
        $root_url=ROOT_URL;
        $cuser=$db->fetchRow('select * from entity where entity_id ='.$parent_id);
        if($cuser && $cuser['role_id']==15){
            $parent_id=$cuser['parent_id'];
        }
        $sql = "select * from entity where entity_id={$user_id} and parent_id={$parent_id} ";
        $user = $db->fetchRow($sql);
        if (!$user) {
            echo json_encode(['status'=>'error','message'=>'User not found']);exit();
        }else{
            $status=$user['status'];
            $first_name=$user['firstname'];
            $mobile=$user['mobile'];
            $surname=$user['surname'];
            $email=$user['email'];
            $password_hash  = password_hash($mobile.uniqid().date('ymdhis'),PASSWORD_DEFAULT);
            // date_default_timezone_set('Etc/UTC');
            $db->update(['password_hash'=>$password_hash,'verify_date'=>date('Y-m-d H:i:s')],'entity',['email'=>$email]);

            $mail_html = <<< HTML
        <p>Dear $first_name</p>

        <p> In order to ensure that all communications between us are kept secure and confidential I would like to introduce you to the BOS Vault. All messages and documents between us will be kept here and you can login at any time once you have set up your password. However, if I post anything important you will receive a notification from me similar to this one asking you to click the link and login.</p>
        
        <p>Please ensure that you do not give your password away to anyone else, no matter what the circumstances. It will be a breach of the Terms and Conditions if you do so, and contrary to many privacy laws. The password will be encrypted and only known to you. We will never ask you for your password, it will be a breach of our Security Policy to do but if you forget it there is a facility to reset it on the login page. It would also be a good idea to place this URL in your safe senders list to avoid any change of these messages ending up in your junk folder.</p>
        
        <p>Welcome to the BOS Vault. Please click this link below to set your password.</p>
        
        <p><a href='$root_url/setpassword.php?ticket=$password_hash'>$root_url/setpassword.php?ticket=$password_hash</a></p>
        
        <p>This is a secure email message from Back Office Solutions Ltd to ($first_name $surname). If this is not you please click here to send a secure message to admin@back-office-solutions.im</p>
HTML;

            require dirname(__FILE__).'/../libs/PHPMailer/PHPMailerAutoload.php';
            $mail = new PHPMailer;
            $mail->isSMTP();
            $mail->SMTPDebug = MAIL_DEBUG;
            $mail->Debugoutput = 'html';
            $mail->Host = MAIL_HOST;
            $mail->Port = MAIL_PORT;
            $mail->SMTPSecure = MAIL_ENCRYPTED;
            $mail->SMTPAuth = true;
            $mail->Username = MAIL_USERNAME;
            $mail->Password = MAIL_PASSWORD;
            $mail->setFrom(FROM_EMAIL, FROM_NAME);
            $mail->addAddress($email, $first_name.' '.$surname);
            $mail->Subject = 'Important Message from Back Office Solutions Ltd';
            $mail->msgHTML($mail_html);
            $mail->AltBody = $mail_html;
            if (!$mail->send()) {

            } else {

            }
            echo json_encode(['status'=>'success','message'=>'Email sent successfully','id'=>$user['entity_id']]);exit();

        }
    }
}

if($action=='save_notes'){
    $entity_id=$_POST['entity_id'];
    $notes=$_POST['notes'];
    $author_id=$_SESSION['user_id'];
    $db->insert(['user_id'=>$entity_id,'author_id'=>$author_id,'notes'=>$notes,'created_at'=>date('Y-m-d H:i:s')],'notes');

    $notes=$db->fetchAll('select notes.* , entity.firstname from notes inner join entity on notes.author_id=entity.entity_id where notes.user_id='.$entity_id);
    require_once ('../modules/people/broker/views/notes.php');exit();
}