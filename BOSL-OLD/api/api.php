<?
include('../config.php');

if(SITE_STATE!="dev"){
	// SECUIRTY ONLY ALLOW INTERNAL CALLS
	// This is to check if the request is coming from a specific domain
    
    // 
	$ref = $_SERVER['HTTP_REFERER'];
	$refData = parse_url($ref);

	if($refData['host'] !== $root) {
	  // Output string and stop execution
	  die("You are not allowed access to this API");
	}

// FURTHER SECURITY this is turned off if the site is set to dev

	$key = strip_tags($_GET["key"]);
	$password= "apibbost4$rwars";
	$token = md5($key."".$password);

	if($token != $_GET["token"]){
	die("You do not have access to the API");
	}
}

        // general cleaning
        $_POST  = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        $_POST=array_map('trim',$_POST);
        // remove tags
        $_POST = array_map( 'strip_tags', $_POST );

         // general cleaning
        $_GET  = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
        $_GET=array_map('trim',$_GET);
        // remove tags
        $_POST = array_map( 'strip_tags', $_POST );



    foreach($_POST as $key => $value){

        $_POST[$key] = mb_convert_encoding( $value, 'UTF-8', 'UTF-8');
        $_POST[$key] = htmlentities($value, ENT_QUOTES, 'UTF-8');
      
    }

    foreach($_GET as $key => $value){

        $_GET[$key] = mb_convert_encoding( $value, 'UTF-8', 'UTF-8');
        $_GET[$key] = htmlentities($value, ENT_QUOTES, 'UTF-8');
      
    }

// ========================================================



     // remove the / in the func call
    if(isset($_GET["func"])){
        $_GET["func"] = ltrim($_GET["func"],'/');
    }
    // if an id has been passed and it is read
    $passid = explode("/", $_GET["func"]);
    if(count($passid)>1){

        // make sure the primarykey value is an int
        $pkey=  preg_replace("[^0-9]","",$passid[1]);
        $_GET["func"] = $passid[0];
    }

    // if there are fields for read
    if($passid[2] && $_GET["func"]=='read'){
        $fields = $passid[2];
    }

    // display fields for rows
    if($passid[1] && $_GET["func"]=='rows'){
        $fields = $passid[1];
    }



 //======================================================


class api {
	//read individual results
    public function read($table, $primaryKey){ 
    	global $db,$fields,$pkey;

        if(!isset($fields)){
            $fields="*";
        }
             //  print 'select '.$fields.' from '.$table.' where '.$primaryKey.' ='. $_GET["id"];
    	      $rs= $db->fetchRow('select '.$fields.' from '.$table.' where '.$primaryKey.' ='. $pkey);

              return json_encode($rs,JSON_PRETTY_PRINT);

    }

    // create a list
    public function rows($table,$where){
print $where;
// security check
if(!$_SESSION["Company_ID"]){
die("You need to log in");
}
		global $db,$fields;

    	if(!isset($fields)){
    		$fields="*";
    	}

    	if(!isset($where)){
    		$where="";
    	}else{
    		$where = " WHERE ".$where;
    	}
         file_put_contents("__audit.txt", 'select '.$fields.' from '.$table.''.$where);
     	   $rs=	$db->fetch('select '.$fields.' from '.$table.''.$where);
       
 	   return json_encode($rs,JSON_PRETTY_PRINT);

    }

    //edit results
    public function update($table,$primaryKey, $pkey){
        global $db;

        $pkey = $_POST[$primaryKey];

        unset($_POST["primaryKey"]);
        unset($_GET["fields"]);

        

        // set data
        $data = $_POST;

        $rs= $db->update($data,$table,$primaryKey.'=' . $pkey);
         file_put_contents("update.txt", $db->previousQuery());

         if($rs){
            return true;
        }

    }

    // add new details
    public function insert($table){
        global $db;

        unset($_POST["primaryKey"]);
        unset($_GET["fields"]);

        $data = $_POST;

        $rs= $db->insert($data,$table);
         file_put_contents("insert.txt", $db->previousQuery());
        if($rs){
            return $rs;
        }

    }

 



}
 