<?php
if(!isset($_SESSION)){
	session_start();
}

require_once ('../config.php');

$action=$_REQUEST['action'];

if($action=='update'){
	$folder_id=$_POST['id'];
	$folder_name=$_POST['folder_name'];
	$permission=$_POST['permission'];
    $contacts_ids = $_POST['contacts'];
    $user_id=$_SESSION['user_id'];
   
    if($user['role_id']==15){
        $user_id=$user['parent_id'];
    }
    $user=$db->fetchRow('select * from entity where entity_id='.$user_id);
    $folder=$db->fetchRow('select * from folders where id='.$folder_id.' and user_id='.$user_id);

    $contacts=$db->fetchAll('select entity_id, firstname,surname from entity where parent_id='.$user_id.' and role_id=10');

    $check=$db->fetchRow("select * from folders where name ='".$folder_name."' and id!=".$folder_id );
    if(!$check) {
        $db->delete('user_folders',['folder_id'=>$folder_id]);
        $db->update([
            'name' => $folder_name,
            'permission' => $permission
        ], 'folders', ['id' => $folder_id]);

        if ($permission == 'choose_in_file') {
            foreach ($contacts_ids as $contacts_id) {
                $db->insert(['folder_id' => $folder_id, 'user_id' => $contacts_id, 'created_at' => date('Y-m-d H:i:s')], 'user_folders');
            }
        } else {
            foreach ($contacts as $contact) {
                $db->insert(['folder_id' => $folder_id, 'user_id' => $contact['entity_id'], 'created_at' => date('Y-m-d H:i:s')], 'user_folders');
            }
        }
        $co_brokers=$db->fetchAll('select * from entity where parent_id='.$user_id.' and role_id=15 order by entity_id desc');
       
        $db->insert(['folder_id' => $folder_id, 'user_id' => $user_id, 'created_at' => date('Y-m-d H:i:s')], 'user_folders');

        foreach($co_brokers as $co_broker){
            $db->insert(['folder_id' => $folder_id, 'user_id' => $co_broker['entity_id'], 'created_at' => date('Y-m-d H:i:s')], 'user_folders');
        }
        echo json_encode(['status'=>'success','message'=>'Folder Updated successfully','folder_name'=>$folder_name,'folder_status'=>$permission=='apply_to_all'?'Applied To All':'Choose in file']);exit();
    }else{
        echo json_encode(['status'=>'failed','message'=>'Folder name already exists']);exit();
    }

    echo json_encode(['status'=>'failed','message'=>'failed']);exit();
}




            