<?php
if(!isset($_SESSION)){
    session_start();
}
require_once ('../config.php');
//require('api.php');
// define the table
$table = "entity";
$primaryKey = "entity_id";
// call new instant of class
//$contact = new api;

//$result = $contact->insert($table);

//echo $result;

$action=$_REQUEST['action'];

if($action=='add-level-one-user'){

    $user_id=$_SESSION['user_id'];

    $count=$db->fetchRow('select count(*) as c from entity where parent_id='.$user_id.' and role_id=15');
    if($count['c']>=3){

        echo 'limit_exceded';exit();
    }
    if(isset($_POST['email'])) {
        $email = $_POST['email'];
        $user=$db->fetchRow("select * from entity where email='".$email."'");
        if($user){
            echo 'failed';exit();
        }
        $title = $_POST['title'];
        $time_zone=$_POST['time_zone'];
        $first_name = $_POST['firstname'];
        $surname = $_POST['surname'];
        $mobile = $_POST['mobile'];

        $clients_notes = $_POST['clients_notes'];
        $id=$db->insert(['title' => $title,'time_zone'=>$time_zone, 'firstname' => $first_name, 'surname' => $surname, 'email' => $email, 'mobile' => $mobile, 'clients_notes' => $clients_notes, 'role_id'=>15,'parent_id'=>$_SESSION['user_id'],'verify_date'=>date('Y-m-d H:i:s')], 'entity');
        $row=$db->fetchRow('select * from entity where entity_id='.$id);
        //$folder_id = $db->insert(['name'=>'Messages','user_id'=>$id,'created_at'=>date('Y-m-d H:i:s'),'is_default'=>1],'folders');
        //$db->insert(['folder_id'=>$folder_id,'user_id'=>$id,'created_at'=>date('Y-m-d H:i:s')],'user_folders');

        $folders=$db->fetchRows("SELECT * FROM `user_folders` WHERE user_id=".$user_id);
        foreach($folders as $folder){
            $db->insert(['folder_id'=>$folder['folder_id'],'user_id'=>$id,'created_at'=>date('Y-m-d H:i:s')],'user_folders');
        }
        echo 'success';exit();
    }
    echo 'failed';exit();

}