<?php
require_once('config.php');

$errors=[];
$title = '';
$first_name ='';
$surname = '';
$email = '';
$confemail='';
$telephone = '';
$submit_check='';
$success=0;
if(isset($_POST['email'])) {
    $title = $_POST['title'];
    $first_name = $_POST['first-name'];
    $surname = $_POST['surname'];
    $email = $_POST['email'];
    $confemail=$_POST['confemail'];
    $telephone = $_POST['telephone'];
    $submit_check=$_POST['submit_check'];
    if($email!=$confemail){
        $errors['email']='Email doesn\'t match with confirm email ';
    }else {
        $password_hash = password_hash($telephone . uniqid() . date('ymdhis'), PASSWORD_DEFAULT);

        $data = ['title' => $title,
            'firstname' => $first_name,
            'surname' => $surname,
            'email' => $email,
            'mobile' => $telephone,
            'role_Id' => 4,
            'password_hash' => $password_hash,
            'created_at' => date('Y-m-d H:i:s'),
            'verify_date' => date('Y-m-d H:i:s'),
            'time_zone'=>'Europe/London'
        ];
        $root_url = ROOT_URL;
        $check = $db->fetchRow("select * from entity where email='" . $email . "'");
        if ($check) {
            $errors['email'] = 'Email already taken please try with another email';
        }
    }
    if(count($errors)==0){
        $res = $db->insert($data, 'entity');
       // date_default_timezone_set('Etc/UTC');

        $mail_html = <<< HTML
        <p>Dear $first_name</p>

        <p>In order to ensure that all communications between us are kept secure and confidential I would like to introduce you to the BOS Vault. All messages and documents between us will be kept here and you can login at any time once you have set up your password. However, if I post anything important you will receive a notification from me similar to this one asking you to click the link and login.</p>
        
        <p>Please ensure that you do not give your password away to anyone else, no matter what the circumstances. It will be a breach of the Terms and Conditions if you do so, and contrary to many privacy laws. The password will be encrypted and only known to you. We will never ask you for your password, it will be a breach of our Security Policy to do but if you forget it there is a facility to reset it on the login page. It would also be a good idea to place this URL in your safe senders list to avoid any change of these messages ending up in your junk folder.</p>
        
        <p>Welcome to the BOS Vault. Please click this link below to set your password.</p>
        
        <p><a href='$root_url/setpassword.php?ticket=$password_hash'>$root_url/setpassword.php?ticket=$password_hash</a></p>
        
        <p>This is a secure email message from Back Office Solutions Ltd to ($first_name $surname). If this is not you please click here to send a secure message to admin@back-office-solutions.im</p>
HTML;

        require 'libs/PHPMailer/PHPMailerAutoload.php';
        $mail = new PHPMailer;
        $mail->isSMTP();
        $mail->SMTPDebug = MAIL_DEBUG;
        $mail->Debugoutput = 'html';
        $mail->Host = MAIL_HOST;
        $mail->Port = MAIL_PORT;
        $mail->SMTPSecure = MAIL_ENCRYPTED;
        $mail->SMTPAuth = true;
        $mail->Username = MAIL_USERNAME;
        $mail->Password = MAIL_PASSWORD;
        $mail->setFrom(FROM_EMAIL, FROM_NAME);
        $mail->addAddress($email, $first_name.' '.$surname);
        $mail->Subject = 'Important Message from Back Office Solutions Ltd';
        $mail->msgHTML($mail_html);
        $mail->AltBody = $mail_html;
        if (!$mail->send()) {
           //echo "Mailer Error: " . $mail->ErrorInfo;
        } else {

        }

        $_SESSION['__register_first_name']=$first_name;
        header('location: reg-complete.php');
    }
}