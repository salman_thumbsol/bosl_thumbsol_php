<? require_once (dirname(__FILE__).'/../config.php'); ?>
<?php
$menus=[
    '4'=>[ //for role 4
        '/modules/profile/broker/'=>'Profile',
        '/reconcile.html'=>'Reconcile',
        'pipeline.php'=>'Pipeline',
        '/modules/vault/broker/'=>'Vault',
        '/modules/people/broker/'=>'People'
    ],



    '10'=>[ // for role 10
        '/modules/vault/contact/'=>'Vault',
        '/products.php'=>'Products',
        '/gdpr.php'=>'EU GDPR',
        '/aboutus.php'=>'About Us',
        '/index.php'=>'Home'
    ],
     '15'=>[ //for role 15
        '/modules/profile/co-broker/'=>'Profile',
        '/reconcile.html'=>'Reconcile',
        'pipeline.php'=>'Pipeline',
        '/modules/vault/co-broker/'=>'Vault',
        '/modules/people/co-broker/'=>'People'
    ],
];
$welcome_text="Hello {$_SESSION['firstname']}. Welcome back to the BOS Vault.";
if($_SESSION['role_id']==10){
    $welcome_text.=" The communication facility with Privacy at it's Core!";
}

?>
<nav>
    <img class="logo" src="/img/logo.png" alt="BOSL logo">
    <ul>
        <li class="main-li"><a href="/logout.php">Logout</a></li>
        <?php foreach($menus[getUserRole()] as $key=>$value){?>
        <li class="main-li <?=isset($current_page_name) && $current_page_name ==$value?'infocus':''?> "><a href="<?=$key?>"><?=$value?></a></li>
        <?php } ?>
        <li class= "device-li">
            <div class="dropdown">
                <button class="dropbtn" onclick="myDropdown()">Open Menu</button>
            </div>
        </li>
    </ul>
</nav>
<nav class="row mobile-nav">
    <div class="dropdown-content" id="myDropdown">
        <ul class="animate">
            <?php foreach($menus[getUserRole()] as $key=>$value){?>
                <li class="<?=isset($current_page_name) && $current_page_name ==$value?'infocus':''?>"><a href="<?=$key?>"><?=$value?></a></li>
            <?php } ?>
            <li><a href="/logout.php">Logout</a></li>
        </ul>
    </div>
</nav>
<p class="header_welcome_text">
    <?=$welcome_text?>
</p>
