<? require_once (dirname(__FILE__).'/../config.php'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo isset($meta_title)?$meta_title:'Back Office Solutions'?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Back Office Solutions allows you to securely encrypt sensitive information for delivery to your clients and customers">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/vendors/css/ionicons.min.css">
    <link href="https://unpkg.com/ionicons@4.1.2/dist/css/ionicons.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/css/grid.css">
    <link rel="stylesheet" type="text/css" href="/css/style.css">
    <link rel="stylesheet" type="text/css" href="/css/queries.css">
    <script src="/js/vendor/jquery-1.8.3-min.js"></script>
    <script src="/js/functions.js"></script>
    <?php if(isset($header_js_css)):?>
        <?=$header_js_css?>
    <?php endif;?>
</head>
<body>
<header>

    <div class="row main-nav">
        <nav>
            <img class="logo" src="img/logo.png" alt="BOSL logo">
            <ul>
                <? if(isLoggedIn()){?>
                    <li class="main-li"><a href="logout.php">Logout</a></li>
                    <? if(getUserRole()==10){?>
                        <li class="main-li <?=isset($current_page_name) && $current_page_name =='vault'?'infocus':''?>"><a href="/modules/vault/contact/">Vault</a></li>
                    <?php } ?>
                <? }else{?>
                <li class="main-li <?=isset($current_page_name) && $current_page_name =='login'?'infocus':''?>"><a href="login.php">Login</a></li>
                <li class="main-li <?=isset($current_page_name) && $current_page_name =='register'?'infocus':''?>"><a href="register.php">Register</a></li>
                <? } ?>

                <li class="main-li <?=isset($current_page_name) && $current_page_name =='products'?'infocus':''?>"><a href="products.php">Products</a></li>
                <li class="main-li <?=isset($current_page_name) && $current_page_name =='gdpr'?'infocus':''?>"><a href="gdpr.php">EU GDPR</a></li>
                <li class="main-li <?=isset($current_page_name) && $current_page_name =='aboutus'?'infocus':''?>"><a href="aboutus.php">About Us</a></li>
                <li class="main-li <?=isset($current_page_name) && $current_page_name =='home'?'infocus':''?>"><a href="index.php">Home</a></li>
                <!--<li class="device-li"><a type="" id="menu" onclick="textChange()">Open Menu</a></li> -->
                <li class= "device-li">
                    <div class="dropdown">
                        <button class="dropbtn" onclick="myDropdown()">Open Menu</button>
                    </div>
                </li>
            </ul>

        </nav>
        <nav class="row mobile-nav">
            <div class="dropdown-content" id="myDropdown">
                <ul class="animate">
                    <li class="<?=isset($current_page_name) && $current_page_name =='home'?'infocus':''?>"><a href="index.php">Home</a></li>
                    <li class="<?=isset($current_page_name) && $current_page_name =='aboutus'?'infocus':''?>"><a href="aboutus.php">About Us</a></li>
                    <li <?=isset($current_page_name) && $current_page_name =='products'?'infocus':''?>><a href="products.php">Products</a></li>
                    <li <?=isset($current_page_name) && $current_page_name =='gdpr'?'infocus':''?>><a href="gdpr.php">EU GDPR</a></li>
                    <? if(isLoggedIn()){?>
                        <? if(getUserRole()==10){?>
                            <li class="main-li <?=isset($current_page_name) && $current_page_name =='vault'?'infocus':''?>"><a href="/modules/vault/contact/">Vault</a></li>
                        <?php } ?>
                        <li class="main-li"><a href="logout.php">Logout</a></li>
                    <? }else{?>
                    <li <?=isset($current_page_name) && $current_page_name =='register'?'infocus':''?>><a href="register.php">Register</a></li>
                    <li <?=isset($current_page_name) && $current_page_name =='login'?'infocus':''?>><a href="login.php">Login</a></li>
                    <?php } ?>
                </ul>
            </div>
        </nav>
    </div>

</header>