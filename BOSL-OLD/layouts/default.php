<? require_once (dirname(__FILE__).'/../config.php'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo isset($meta_title)?$meta_title:'Back Office Solutions'?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Back Office Solutions allows you to securely encrypt sensitive information for delivery to your clients and customers">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/vendors/css/ionicons.min.css">
    <link href="https://unpkg.com/ionicons@4.1.2/dist/css/ionicons.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/css/grid.css">
    <link rel="stylesheet" type="text/css" href="/css/style.css">
    <link rel="stylesheet" type="text/css" href="/css/queries.css">
    <!--
Rock Hammer by Andy Clarke
Version: 0.1
URL: http://stuffandnonsense.co.uk/projects/rock-hammer/
Apache License: v2.0. http://www.apache.org/licenses/LICENSE-2.0
-->
    <!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
    <!--[if (IE 7)&!(IEMobile)]><html class="no-js lt-ie9 lt-ie8" lang="en"><![endif]-->
    <!--[if (IE 8)&!(IEMobile)]><html class="no-js lt-ie9" lang="en"><![endif]-->
    <!--[if gt IE 8]><!--> <html class="no-js" lang="en"><!--<![endif]-->

    <head>
        <title>Back Office Solution</title>

        <!-- Hammer reload -->
        <script>
            // setInterval(function(){
            //     try {
            //         if(typeof ws != 'undefined' && ws.readyState == 1){return true;}
            //         ws = new WebSocket('ws://'+(location.host || 'localhost').split(':')[0]+':35353')
            //         ws.onopen = function(){ws.onclose = function(){document.location.reload()}}
            //         window.onbeforeunload = function() { ws = null }
            //         ws.onmessage = function(){
            //             var links = document.getElementsByTagName('link');
            //             for (var i = 0; i < links.length;i++) {
            //                 var link = links[i];
            //                 if (link.rel === 'stylesheet' && !link.href.match(/typekit/)) {
            //                     href = link.href.replace(/((&|\?)hammer=)[^&]+/,'');
            //                     link.href = href + (href.indexOf('?')>=0?'&':'?') + 'hammer='+(new Date().valueOf());
            //                 }
            //             }
            //         }
            //     }catch(e){}
            // }, 1000)
        </script>
        <!-- /Hammer reload -->



        <meta http-equiv="cleartype" content="on">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

        <meta name="description" content="">
        <meta name="author" content="">

        <!-- For all browsers -->
        <link rel='stylesheet' href='/js/tablesaw/dependencies/qunit.css'>
        <link rel='stylesheet' href='/js/tablesaw/stackonly/tablesaw.stackonly.css'>
        <link rel='stylesheet' href='/css/rock-hammer.css'>

        <!-- JavaScript -->
        <script src='/js/vendor/head.load.min.js'></script>
        <script src='/js/vendor/modernizr-2.6.2-min.js'></script>
        <script src='/js/vendor/jquery-1.8.3-min.js'></script>
        <script src='/js/tablesaw/tablesaw.js'></script>
        <script src='/js/tablesaw/tablesaw-init.js'></script>

        <!--[if (lt IE 9) & (!IEMobile)]>
        <script src='/js/vendor/selectivizr-min.js'></script>
        <link rel='stylesheet' href='/css/lte-ie8.css'>
        <![endif]-->

        <link rel="shortcut icon" href="/img/favicon.ico">
        <link rel="shortcut icon" href="/img/favicon.png" sizes="32x32">
        <!-- <link rel="apple-touch-icon-precomposed" href="img/l/apple-touch-icon-precomposed.png"> -->
        <!-- <link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/m/apple-touch-icon-72x72-precomposed.png"> -->
        <!-- <link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/h/apple-touch-icon-114x114-precomposed.png"> -->
        <!-- <link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/h/apple-touch-icon-144x144-precomposed.png"> -->
        <!--iOS -->
        <!-- <meta name="apple-mobile-web-app-title" content="Rock Hammer"> -->
        <!-- <meta name="viewport" content="initial-scale=1.0"> (Use if apple-mobile-web-app-capable below is set to yes) -->
        <!-- <meta name="apple-mobile-web-app-capable" content="yes"> -->
        <!-- <meta name="apple-mobile-web-app-status-bar-style" content="black"> -->
        <!-- Startup images -->
        <!-- <link rel="apple-touch-startup-image" href="img/startup/startup-320x460.png" media="screen and (max-device-width:320px)"> -->
        <!-- <link rel="apple-touch-startup-image" href="img/startup/startup-640x920.png" media="(max-device-width:480px) and (-webkit-min-device-pixel-ratio:2)"> -->
        <!-- <link rel="apple-touch-startup-image" href="img/startup/startup-640x1096.png" media="(max-device-width:548px) and (-webkit-min-device-pixel-ratio:2)"> -->
        <!-- <link rel="apple-touch-startup-image" sizes="1024x748" href="img/startup/startup-1024x748.png" media="screen and (min-device-width:481px) and (max-device-width:1024px) and (orientation:landscape)"> -->
        <!-- <link rel="apple-touch-startup-image" sizes="768x1004" href="img/startup/startup-768x1004.png" media="screen and (min-device-width:481px) and (max-device-width:1024px) and (orientation:portrait)"> -->
        <!-- Windows 8 / RT -->
        <meta name="msapplication-TileImage" content="/img/h/apple-touch-icon-144x144-precomposed.png">
        <meta name="msapplication-TileColor" content="#000">

        <script src="/js/vendor/jquery-1.8.3-min.js"></script>
        <script src="/js/functions.js"></script>
        <?php if(isset($header_js_css)):?>
            <?=$header_js_css?>
        <?php endif;?>
    </head>
<body  class="default" id="bos">
<header>
    <div class="row main-nav">
        <?php require_once ('menu.php')?>
        <?php if(getUserRole()==333){?>
        <nav>
            <img class="logo" src="/img/logo.png" alt="BOSL logo">
            <ul>
                <li class="main-li"><a href="/logout.php">Logout</a></li>
                <li class="main-li"><a href="profile.php">Profile</a></li>
                <li class="main-li"><a href="reconcile.html">Reconcile</a></li>
                <li class="main-li"><a href="pipeline.html">Pipeline</a></li>
                <li class="main-li"><a href="vault.html">vault</a></li>
                <li class="main-li infocus"><a href="people.php">people</a></li>
                <!--<li class="main-li"><a href="index.html">home</a></li>
                <!--<li class="device-li"><a type="" id="menu" onclick="textChange()">Open Menu</a></li> -->
                <li class= "device-li">
                    <div class="dropdown">
                        <button class="dropbtn" onclick="myDropdown()">Open Menu</button>
                    </div>
                </li>
            </ul>
        </nav>
        <nav class="row mobile-nav">
            <div class="dropdown-content" id="myDropdown">
                <ul class="animate">
                    <!--<li><a href="index.html">Home</a></li>-->
                    <li class="infocus"><a href="people.php">People</a></li>
                    <li><a href="vault.html">vault</a></li>
                    <li><a href="pipeline.html">Pipeline</a></li>
                    <li><a href="reconcile.html">Reconcile</a></li>
                    <li><a href="profile.php">Profile</a></li>
                    <li><a href="/logout.php">Logout</a></li>
                </ul>
            </div>
        </nav>
        <?php } ?>
    </div>
</header>

<?= $appcontent ?>

<div class="container">
    <section>
        <div class="l-wrap">
            <footer role="contentinfo">
                <small>
                </small>
            </footer>
        </div>
    </section>
</div>
<script src="/js/functions.js" type="text/javascript"></script>
<?php if(getUserRole()==4){?>
    <script src="/js/broker.js" type="text/javascript"></script>
<?php }?>
<?php if(getUserRole()==10){?>
    <script src="/js/contact.js" type="text/javascript"></script>
<?php }?>
<?php if(getUserRole()==15){?>
    <script src="/js/co-broker.js" type="text/javascript"></script>
<?php }?>
</body>
</html>