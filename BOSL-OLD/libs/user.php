<?php
//require_once ('../config.php');
function isLoggedIn(){
    return isset($_SESSION['user_id'])?true:false;
}


function getLoggedInUser(){
    global $db;
    $user_id= $_SESSION['user_id'];
    return $db->fetchRow('select * from entity where entity_id='.$user_id);
}

function getUserRole(){
    return isset($_SESSION['role_id'])?$_SESSION['role_id']:null;
}


function getCoBrokers($user_id){
    global $db;
    return $db->fetchRows('select * from entity where parent_id='.$user_id.' and role_id=15');
}