<?php

class Message
{
    public $db;
    function __construct()
    {
        global $db;
        $this->db=$db;
    }

    public function getMessageParticipantsByFolder($folder_id)
    {
        $folder=$this->db->fetchRow('select * from folders where id ='.$folder_id);
        $rows = $this->db->fetchRows('SELECT * FROM user_folders WHERE folder_id=' . $folder_id);
        $ids=[];
        foreach($rows as $row){
            $ids[]=$row['user_id'];
        }      
        return $ids;
    }

    public function setParticipantDetails($message_id){
        $message=$this->getMessage($message_id);

        $folder_id=$message['folder_id'];
        $participants=$this->getMessageParticipantsByFolder($folder_id);
        
        foreach($participants as $participant){           
             $this->setMessageDetails($message_id,$participant);
        }

    }

    public function getMessage($message_id)
    {
        $row = $this->db->fetchRow('SELECT * FROM folders_items WHERE id=' . $message_id);
        return $row;
    }

    public function getMessageDetails($message_id, $user_id)
    {
        $message = $this->getMessage($message_id);
        if ($message) {
            $message_detail = $this->db->fetchRow('select * from folders_items_details where user_id =' . $user_id . ' and folder_item_id=' . $message_id);
            if (!$message_detail) {              
                $message=$this->getMessage($message_id);
                $message_detail = $this->db->insert(['folder_item_id' => $message_id, 'user_id' => $user_id,'folder_id'=>$message['folder_id'], 'created_at' => date('Y-m-d')], 'folders_items_details');
            }
            return $message_detail;
        }
        return null;
    }

    public function setMessageDetails($message_id, $user_id)
    {
        $message_detail = $this->getMessageDetails($message_id,$user_id);
        if (!$message_detail) {
                $message=$this->getMessage($message_id);
                $message_detail = $this->db->insert(['folder_item_id' => $message_id, 'user_id' => $user_id,'folder_id'=>$message['folder_id'], 'created_at' => date('Y-m-d')], 'folders_items_details');
        }
        return $message_detail;
    }

    public function markloaded($user_id, $message_id)
    {
        $message = $this->getMessage($message_id);
        if ($message) {
            $message_detail = $this->getMessageDetails($message_id, $user_id);
            if ($message_detail) {
                $this->db->update(['folder_item_id' => $message_id, 'user_id' => $user_id, 'loaded' => 1], 'folders_items_details', ['id' => $message_detail['id']]);
                return true;
            }
        }
        return false;
    }

    public function markread($user_id, $message_id)
    {
        $message = $this->getMessage($message_id);
        if ($message) {
            $message_detail = $this->getMessageDetails($message_id, $user_id);
            if ($message_detail) {
                $this->db->update(['folder_item_id' => $message_id, 'user_id' => $user_id, 'mesage_read' => 1], 'folders_items_details', ['id' => $message_detail['id']]);
                return true;
            }
        }
        return false;
    }

    public function markunread($user_id, $message_id)
    {
        $message = $this->getMessage($message_id);
        if ($message) {
            $message_detail = $this->getMessageDetails($message_id, $user_id);
            if ($message_detail) {
                $this->db->update(['folder_item_id' => $message_id, 'user_id' => $user_id, 'mesage_read' => 0], 'folders_items_details', ['id' => $message_detail['id']]);
                return true;
            }
        }
        return false;
    }


    public function hidemessage($user_id, $message_id)
    {
        $message = $this->getMessage($message_id);
        if ($message) {
            $message_detail = $this->getMessageDetails($message_id, $user_id);
            if ($message_detail) {
                $this->db->update(['folder_item_id' => $message_id, 'user_id' => $user_id, 'hide' => 1], 'folders_items_details', ['id' => $message_detail['id']]);
                return true;
            }
        }
        return false;
    }

    public function showmessage($user_id, $message_id)
    {
        $message = $this->getMessage($message_id);
        if ($message) {
            $message_detail = $this->getMessageDetails($message_id, $user_id);
            if ($message_detail) {
                $this->db->update(['folder_item_id' => $message_id, 'user_id' => $user_id, 'hide' => 0], 'folders_items_details', ['id' => $message_detail['id']]);

                return true;
            }
        }
        return false;
    }
}
$message_obj=new Message();

?>