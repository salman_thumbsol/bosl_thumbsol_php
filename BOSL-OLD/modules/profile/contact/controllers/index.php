<?php
require_once (dirname(__FILE__).'/../../../../config.php');
$meta_title='';

$user_id=$_SESSION['user_id'];


if(isset($_POST['submit'])){

    $firstname=$_POST['firstname'];
    $surname=$_POST['surname'];
    $time_zone=$_POST['time_zone'];
    $dob_day=$_POST['dob_day'];
    $dob_month=$_POST['dob_month'];
    $dob_year=$_POST['year'];
    $country=$_POST['country'];
    $address=$_POST['address'];
    $post_code=$_POST['post_code'];
    $email_primary=$_POST['email_primary'];
    $email_secondary=$_POST['email_secondary'];
    $telephone=$_POST['telephone'];
    $mobile=$_POST['mobile'];
    $db->update([
        'firstname'=>$firstname,
        'surname'=>$surname,
        'time_zone'=>$time_zone,
        'dob_day'=>$dob_day,
        'dob_month'=>$dob_month,
        'dob_year'=>$dob_year,
        'country'=>$country,
        'address'=>$address,
        'post_code'=>$post_code,
        'email_primary'=>$email_primary,
        'email_secondary'=>$email_secondary,
        'telephone'=>$telephone,
        'mobile'=>$mobile
    ],'entity',['entity_id'=>$user_id]);

}


$row=$db->fetchRow('select * from entity where entity_id='.$user_id);