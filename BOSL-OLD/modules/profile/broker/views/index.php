﻿<div class="container">
    <!-- Next ============================== -->

    <section>
        <div class="l-wrap">
            <div class="l-full-width">
                <div class="page-title">
                    <h5>Profile</h5>
                </div>

                <form class="profile" method="post">

                    <div class="row ">
                        <div class="col span-1-of-3">
                            <p><label for="title">Title</label></p>
                            <p>
                                <select name="title" required>
                                    <option value="Mr" <?=($row['title']=='Mr')?'selected="selected"':'' ?>>Mr</option>
                                    <option value="Miss" <?=($row['title']=='Miss')?'selected="selected"':'' ?>>Miss</option>
                                    <option value="Mrs" <?=($row['title']=='Mrs')?'selected="selected"':'' ?>>Mrs</option>
                                    <option value="Ms" <?=($row['title']=='Ms')?'selected="selected"':'' ?>>Ms</option>
                                </select>
                            </p>
                        </div>
                        <div class="col span-1-of-3">
                            <p><label for="first-name">First Name</label><br />
                                <input type="text" id="first-name" name="firstname" placeholder="David" value="<?=$row['firstname']?>"></p>
                        </div>
                        <div class="col span-1-of-3">
                            <p><label for="surname">Surname</label><br />
                                <input type="text" id="surname" name="surname" placeholder="Goffin" value="<?=$row['surname']?>"></p>

                        </div>






                    </div>
                    <div class="row">
                        <div class="col span-1-of-3">
                            <p><label for="email-primary">Email (Primary)</label><br />
                                <input type="email" name="email_primary" value="<?=$row['email_primary']?>" id="email-primary" placeholder="Email (Primary)"></p>
                        </div>
                        <div class="col span-1-of-3">
                            <p><label for="email-secondary">Email (Secondary)</label><br />
                                <input type="email" id="email-secondary" name="email_secondary" value="<?=$row['email_secondary']?>" placeholder="Email (Secondary)"></p>
                        </div>
                        <div class="col span-1-of-3">
                            <p><label for="tel">Telephone</label><br />
                                <input type="tel" id="tel" name="telephone"  value="<?=$row['telephone']?>" placeholder="Telephone"></p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col span-1-of-3">
                            <p><label for="tel">Mobile</label><br />
                                <input type="tel" id="tel" placeholder="Mobile" name="mobile"  value="<?=$row['mobile']?>"></p>
                        </div>
                        <div class="col span-1-of-3">
                            <p><label for="postcode">Postcode</label><br />
                                <input type="text" id="postcode" name="post_code" value="<?=$row['post_code']?>" placeholder="Postcode"></p>
                        </div>

                        <div class="col span-1-of-3">
                            <p><label for="Date Of Birth">Date Of Birth</label><br />

                                <div class="col span-1-of-4">
                                <select name="dob_day" id="day">
                                    <?php for($d=1;$d<32;$d++){?>
                                        <option value="<?=$d?>" <?=$d==$row['dob_day']?'selected="selected"':''?>><?=$d?></option>
                                    <?php }?>
                                </select>
                            </div>
                            <div class="col span-1-of-3">
                                <select name="dob_month" id="month">
                                    <?php foreach(getMonths() as $key=>$value){?>
                                        <option value="<?=$key?>" <?=$key==$row['dob_month']?'selected="selected"':''?>><?=$value?></option>
                                    <?php }?>
                                </select>
                            </div>
                                <div class="col span-1-of-3">
                                <select name="year" id="dob_year">
                                    <?php foreach(getYears() as $key=>$value){?>
                                        <option value="<?=$key?>" <?=$key==$row['dob_year']?'selected="selected"':''?>><?=$value?></option>
                                    <?php } ?>
                                </select>
                                </div>

                        </div>
                    </div>

                    <div class="row">
                        <div class="col span-1-of-3">

                            <p><label for="Country">Country</label></p>
                            <p>
                                <select name="country" id="country">
                                    <option value="">Please Select</option>
                                    <?php foreach(getCountries() as $key=>$value){?>
                                        <option value="<?=$key?>" <?=$key==$row['country']?'selected="selected"':''?>><?=$value?></option>
                                    <?php } ?>
                                </select>
                            </p>
                        </div>
                        <div class="col span-1-of-3">
                            <p><label for="time-zone">Time Zone</label></p>
                            <p>
                                <select name="time_zone" id="time-zone">
                                    <option value="">Please Select</option>
                                    <?php foreach(getTimeZones() as $key=>$value):?>
                                        <option value="<?=$key?>" <?=$key==$row['time_zone']?'selected="selected"':''?> ><?=$value?></option>
                                    <?php endforeach;?>
                                </select></p>


                        </div>
                    </div>









                    <p><label for="address">Address</code></label><br>

                        <textarea name="address" placeholder="Address"><?=$row['address']?></textarea>




                    <ul><li><label class="checkbox"><input type="checkbox" id="agree-policy"> I’ve read and agree to the <a href="#cookie-policy" data-toggle="modal">Cookie Policy</a>, <a href="#terms-conditions" data-toggle="modal">Terms & Conditions</a>, <a href="#privacy-policy" data-toggle="modal">Privacy Policy</a> and <a href="#security-policy" data-toggle="modal">Security Policy</a>.</label></li></ul>

                    <input type="submit" name="submit" id="submit-profile-btn" value="Save Profile" class="btn btn-primary display_none" />

                    <!-- Cookie Policy Modal -->
                    <div id="cookie-policy" class="modal hide fade modal-secondary" tabindex="-1" role="dialog" aria-labelledby="cookie-policy" aria-hidden="true">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h3 id="modal-title">Cookie Policy</h3>
                        </div>
                        <div class="modal-body">
                            <p>Cookie policy text...</p>

                        </div>
                        <div class="modal-footer">
                            <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                        </div>
                    </div> <!-- / Cookie Policy Modal -->
                    <!-- Terms & Conditions Modal -->
                    <div id="terms-conditions" class="modal hide fade modal-secondary" tabindex="-1" role="dialog" aria-labelledby="terms-conditions" aria-hidden="true">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h3 id="modal-title">Terms & Conditions</h3>
                        </div>
                        <div class="modal-body">
                            <p>Terms & Conditions text...</p>

                        </div>
                        <div class="modal-footer">
                            <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                        </div>
                    </div> <!-- / Terms & Conditions Modal -->
                    <!-- Privacy Policy Modal -->
                    <div id="privacy-policy" class="modal hide fade modal-secondary" tabindex="-1" role="dialog" aria-labelledby="privacy-policy" aria-hidden="true">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h3 id="modal-title">Privacy Policy</h3>
                        </div>
                        <div class="modal-body">
                            <p>Privacy policy text...</p>

                        </div>
                        <div class="modal-footer">
                            <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                        </div>
                    </div> <!-- / Privacy Policy Modal -->
                    <!-- Security Policy Modal -->
                    <div id="security-policy" class="modal hide fade modal-secondary" tabindex="-1" role="dialog" aria-labelledby="security-policy" aria-hidden="true">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h3 id="modal-title">Security Policy</h3>
                        </div>
                        <div class="modal-body">
                            <p>Security policy text...</p>

                        </div>
                        <div class="modal-footer">
                            <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                        </div>
                    </div> <!-- / Security Policy Modal -->

                </form>
            </div>

        </div><!-- l-wrap -->

    </section><!-- section -->


    <!-- Final ============================== -->


</div><!-- container -->

<!-- Load libraries -->
<script>
    head.js(
// Place scripts to be loaded here, in the order the need to be executed
// Typically libraries are needed first, such as jQuery (as the plugins
// depend on jQuery to execute). The below configuration is what Rock
// Hammer uses, but customise this for your own needs.
// Notice the use of @path here. This is because we need the file names,
// not the script tags being generated. Ensure all filenames you wish
// head.js to load are surrounded with quotes. Also, this is a list, so
// ensure that all but the last item are followed by a comma.

// Plugins
        "/js/vendor/jquery/jquery.scrollTo.min.js",

// Load bootstrap-transition first so that nice glides/fades
// etc for the other bootstrap plugins work
        "/js/vendor/bootstrap/bootstrap-transition.js",
// "js/vendor/bootstrap/bootstrap-carousel.js",
// "js/vendor/bootstrap/bootstrap-tooltip.js",

// Popover has a dependency on tooltip, so make sure and include
// bootstrap-tooltip regardless in order for popovers to work
// "js/vendor/bootstrap/bootstrap-popover.js",
        "/js/vendor/bootstrap/bootstrap-modal.js",
        "/js/vendor/bootstrap/bootstrap-collapse.js",
        "/js/vendor/bootstrap/bootstrap-tab.js",

// Responsive data tables
// "js/rwd-table.js",

// Remove any navigation patterns below that arent used
// "js/nav-patterns/nav-toggle.js",
// "js/nav-patterns/left-nav-flyout.js",
        "/js/nav-patterns/responsive-nav.min.js",
// "js/nav-patterns/responsive-breadcrumb.js",

// Functionality only used by Rock Hammer - this should be removed in production projects!
// "js/navigation-manager.js"
    );

    // Google Analytics code
    // Set your account ID appropriately by replacing UA-XXXXX-X
    // var _gaq=[['_setAccount','UA-XXXXX-X'],['_trackPageview']];
    // (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
    //     g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
    //     s.parentNode.insertBefore(g,s)}(document,'script'));
</script>
<script src="https://unpkg.com/ionicons@4.1.2/dist/ionicons.js"></script>

<script>
    $(function () {

        $('.nav-tabs a').on('click',function(e) {
            e.preventDefault();
            var link = $(this).attr('href').replace('#','');
            $('.nav-tabs a').each(function() {
                if($(this).attr('href')==='#'+link) {
                    $(this).parent().addClass('active');
                } else {
                    $(this).parent().removeClass('active');
                }
            })
            $('.tab-pane').each(function() {
                if($(this).attr('id')===link) {
                    $(this).addClass('active');
                } else {
                    $(this).removeClass('active');
                }
            })
        })
    });

    $(document).on('change','#agree-policy',function(){
        if($(this).is(":checked")){
            $('#submit-profile-btn').removeClass('display_none');
        }else{
            $('#submit-profile-btn').addClass('display_none');
        }
    });

</script>

