
<? 
error_reporting(E_ALL);
ini_set('display_errors', 1); 
// include generall setttings
include("../../../config.php");

try{
// security check
if($_SESSION['role_id']!=15){
	//header('Location: '.$root.'accessdenied.php');
    header('HTTP/1.0 403 Forbidden');
	exit();
}

$page_title ="Your people";
$current_page_name='Vault';
ob_start();

// GET ANY OF THE PROCESSING
include('controllers/index.php');

// get the informaton of the html to display
include('views/index.php');

		//assign the content to a var
		$appcontent = ob_get_contents();
		ob_end_clean();

// include the defulat layout
include('../../../layouts/default.php');
}catch (ErrorException $e){
print "<b>OOOPS Something went wrong</B><br><br>". $e->getMessage(). " In ". $e->getFile();

}

?>