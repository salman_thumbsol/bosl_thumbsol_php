<li class="note-single <?=$user_id!=$message['sender_id']?'note-single-reply':''?> <?=$user_id!=$message['sender_id'] && $message['mesage_read']==0?' unread':''?>" data-id="<?=$message['id']?>" id="message-<?=$message['id']?>">
    <div class="short-message">
        <h5 class="author-name"><?= $message['firstname'] ?> <?= $message['surname'] ?> <span class="message-time"><em>Added: <?=gmdate('h:i A',strtotime($message['created_at']))?> GMT</em></span></h5>
        <p class=""><?=substr($message['message'],0,200)?></p>

        <ul>
            <li><a href="#" class="toggle-long-message"  data-id="<?=$message['id']?>" data-status="<?=$message['mesage_read']?>">OPEN</a></li>
            <?php if($user_id!=$message['sender_id']){?>
            <li><a href="#" class="reply-message">REPLY</a></li>
                <?php if($message['mesage_read']==1){?>
                <li><a href="#" class="mark-unread-message" data-id="<?=$message['id']?>">MARK AS UNREAD</a></li>
                <?php }?>
                <?php }?>
            <?php if($message['hide']==0){?>
            <li><a href="#" class="hide-message" data-id="<?=$message['id']?>">HIDE</a></li>
            <?php }else{?>
                <li><a href="#" class="show-message" data-id="<?=$message['id']?>">SHOW</a></li>
            <?php }?>
        </ul>
    </div>

    <div class="long-message hide">
        <h4 class="author-name"><?= $message['firstname'] ?> <?= $message['surname'] ?> </h4>
        <p><span class="message-time"><em>Added: <?=gmdate('h:i A',strtotime($message['created_at']))?> GMT</em></span></p>
        <p class=""><?=$message['message']?></p>


        <?php if($message['document_path']!=''){?>
            <p>
                <a href="<?=ROOT_URL.'/uploads/'.$message['document_path']?>" >
                    <?php if(isImage(ROOT_URL.'/uploads/'.$message['document_path'])){?>
                        <img src="<?=ROOT_URL.'/uploads/'.$message['document_path']?>" width="200" />
                    <?php  }else{?>
                        <?=$message['document_path']?>
                    <?php } ?>
                </a>
            </p>
        <?php }?>

        <ul>
            <li><a href="#" class="toggle-long-message">CLOSE</a></li>
            <?php if($user_id!=$message['sender_id']){?>
                <li><a href="#" class="reply-message">REPLY</a></li>
                <li><a href="#" class="mark-unread-message" data-id="<?=$message['id']?>">MARK AS UNREAD</a></li>
            <?php }?>
            <?php if($message['hide']==0){?>
                <li><a href="#" class="hide-message" data-id="<?=$message['id']?>">HIDE</a></li>
            <?php }else{?>
                <li><a href="#" class="show-message" data-id="<?=$message['id']?>">SHOW</a></li>
            <?php }?>
        </ul>
    </div>

    <div class="reply-area hide">
        <form method="post" action="" name="reply-form-<?=$message['id']?>" onsubmit="return contact.reply(this);return false;">
            <input type="hidden" name="parent_id" value="<?=$message['id']?>" />
            <input type="hidden" name="action" value="reply" />
            <input type="hidden" name="folder_id" value="<?=$message['folder_id']?>" />
            <textarea class="form-control reply-textbox" name="message" placeholder="Please enter message here..." required></textarea>
            <div class="span-1-of-1 text-right" >
                <input type="submit" value="Send" class="btn btn-primary" />
            </div>

        </form>
        <div class="clearfix"></div>
    </div>
</li>