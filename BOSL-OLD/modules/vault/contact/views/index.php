<div class="l-wrap">
    <br>
    <p class="new-messages">
        You have <span class="message-count-new">0</span> new messages
    </p>
</div>
<nav id="subnav" role="subnav">
    <div class="subnav-header">
        <i class="icon ion-ios-key icon-big icon-secondary"></i>
   </div>
    <ul>
        <?php foreach($folders as $folder){?>
            <li class="<?=$folder['id']==$folder_id?'active':''?>">
                <a href="/modules/vault/contact/?folder_id=<?=$folder['id']?>"><?=$folder['name']?> </a>
            </li>
        <?php } ?>
        <li><a href="/modules/profile/contact/">My Profile</a></li>
    </ul>

</nav>

<div class="container">


    <section>
        <div class="row">
            <div class="col span-1-of-4 sidebar-menu">
                <ul class="sub-menu-contact">
                        <li><a href="#">Threads</a></li>
                        <li class="<?=$hide==0?'active':''?>"><a href="/modules/vault/contact/?folder_id=<?=$folder_id?>">News (<span class="message-count-new-<?=$folder_id?>">0</span> unread)</a></li>
                        <li class="<?=$hide==1?'active':''?>"><a href="/modules/vault/contact/?folder_id=<?=$folder_id?>&hide=1">Archived (<span class="message-count-archived-<?=$folder_id?>">0</span>)</a></li>
<!--                        <li><a href="javasript:void(0)">General</a></li>-->
<!--                        <li><a href="javasript:void(0)">Vacations</a></li>-->
<!--                        <li><a href="javasript:void(0)">Promotions (1 unread)</a></li>-->
                </ul>
            </div>
            <div class="col contact-span">
                <ul id="notes-log-<?=$folder_id?>" class="notes-log">
                    <?php foreach($messages as $message){?>
                       <?php
                        ob_start();
                        include('_message.php');
                        $content = ob_get_contents();
                        ob_end_clean();
                        echo $content;
                        ?>
                    <?php }?>
                </ul>
            </div>
        </div>
    </section>
</div>




<script>
    head.js(
// Place scripts to be loaded here, in the order the need to be executed
// Typically libraries are needed first, such as jQuery (as the plugins
// depend on jQuery to execute). The below configuration is what Rock
// Hammer uses, but customise this for your own needs.
// Notice the use of @path here. This is because we need the file names,
// not the script tags being generated. Ensure all filenames you wish
// head.js to load are surrounded with quotes. Also, this is a list, so
// ensure that all but the last item are followed by a comma.

// Plugins
        "/js/vendor/jquery/jquery.scrollTo.min.js",

// Load bootstrap-transition first so that nice glides/fades
// etc for the other bootstrap plugins work
        "/js/vendor/bootstrap/bootstrap-transition.js",
// "js/vendor/bootstrap/bootstrap-carousel.js",
// "js/vendor/bootstrap/bootstrap-tooltip.js",

// Popover has a dependency on tooltip, so make sure and include
// bootstrap-tooltip regardless in order for popovers to work
// "js/vendor/bootstrap/bootstrap-popover.js",
        "/js/vendor/bootstrap/bootstrap-modal.js",
        "/js/vendor/bootstrap/bootstrap-collapse.js",

// Responsive data tables
// "js/rwd-table.js",

// Remove any navigation patterns below that arent used
// "js/nav-patterns/nav-toggle.js",
// "js/nav-patterns/left-nav-flyout.js",
        "/js/nav-patterns/responsive-nav.min.js",
// "js/nav-patterns/responsive-breadcrumb.js",

// Functionality only used by Rock Hammer - this should be removed in production projects!
// "js/navigation-manager.js"
    );

    // Google Analytics code
    // Set your account ID appropriately by replacing UA-XXXXX-X
    var _gaq=[['_setAccount','UA-XXXXX-X'],['_trackPageview']];
    (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
        g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
        s.parentNode.insertBefore(g,s)}(document,'script'));
       
        $(document).ready(function(){
            $('#subnav li').on('click', function(){
                $('#subnav li').removeClass('active');
                $(this).addClass('active');
            });

            $('.sidebar-menu li').on('click', function(){
                $('.sidebar-menu li').removeClass('active');
                $(this).addClass('active');
            });

            setInterval(function(){
                contact.refresh_counter();
            },3000);

            setTimeout(function(){
                setInterval(function(){
                    contact.refresh_messages(<?=$folder_id?>)
                },3000);
            },1000);
        });


        $(document).ready(function(){
            contact.initContactVault();
            contact.refresh_counter();
            setInterval(function(){
                contact.refresh_counter();
            },3000);

            setTimeout(function(){
                setInterval(function(){
                    contact.refresh_messages(<?=$folder_id?>)
                },3000);
            },1000);
            
            var bw = new BOSLWayPoint(<?=$folder_id?>,<?=$user_id?>);
            bw.setElement('#notes-log-<?=$folder_id?>');
            bw.setUserType('contact');
            bw.setExtraParams({id:'hide',value:<?=$hide?>});
            bw.init();
        });

</script>