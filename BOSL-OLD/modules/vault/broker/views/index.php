<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="https://rawgit.com/enyo/dropzone/master/dist/dropzone.css">
<!--<link href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.css" rel="stylesheet"/>-->
<div class="container">
    <!-- Next ============================== -->
    <section>
        <div class="l-wrap">
            <div class="l-full-width">
               
               

                <div class="row">
                    <div class="col span-1-of-2">

                        
                      <label for="title">System Folders</label>
                      <table class="table folders-table">

                                <tr>
                                    <td>Messages</td>
                                    <td>Applied To All</td>
                                </tr>

                        </table>



                        <form class="profile" method="post">
                            <p><label for="title">Add New Folder</label><br />
                                <input type="text" name="folder_name" placeholder="New Folder Name Here" required>
                            </p>

                            <p class="error"><?=$folder_already_exists!=''?'Folder already exists. Try with another name':''?></p>
                            <p><label for="title"></label><br />
                                <input type="radio" name="permission" data-folder-id="0" value="apply_to_all" > Apply to All

                            </p>
                            <p>
                                <label for="title"></label><br />
                                <input type="radio" name="permission" data-folder-id="0" value="choose_in_file" > Choose In File
                            </p>

                            <p id="choose_in_file_section-0" class="choose_in_file_section hide">
                                <select id="select2-0" class="select2" name="contacts[]" multiple="multiple">
                                    <?php foreach($contacts as $contact){?>
                                        <option value="<?=$contact['entity_id']?>"><?=$contact['firstname'].' '.$contact['surname']?></option>
                                    <?php } ?>
                                </select>
                            </p>

                            <p>
                                <input type="submit" class="btn btn-primary display_none" id="submit-btn-0" value="Submit" />
                            </p>
                        </form>
                    </div>
                    

                    <div class="col span-1-of-2">
                        <label for="title">Folders</label>           
                        <table class="table folders-table">
                            <?php foreach($folders as $folder):?>
                                <tr>
                                    <td id="folder-name-<?=$folder['id']?>"><?=$folder['name']?></td>
                                    <td id="folder-status-<?=$folder['id']?>"><?=$folder['permission']=='apply_to_all'?'Applied To All':'Choose in file'?></td>
                                    <td id="folder-add-user-<?=$folder['id']?>">
                                        <?php if($folder['permission']!='apply_to_all'){?>
                                        <a href="" class="new-entry" data-folder-id="<?=$folder['id']?>">Add a User</a>
                                        <?php }?>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="folder_detail">

                                        <form class="profile" method="post" data-folder-id="<?=$folder['id']?>" onsubmit="return updatefolder(this);">
                                            <input type="hidden" name="id" value="<?=$folder['id']?>" />
                                            <input type="hidden" name="action" value="update" />
                                            <p><label for="title">Edit Folder</label><br />
                                                <input type="text" value="<?=$folder['name']?>" name="folder_name" placeholder="Folder Name Here" required>
                                            </p>
                                            <div class="resp mt-2"></div>
                                            <p><label for="title"></label><br />
                                                <input type="radio" name="permission" <?=$folder['permission']=='apply_to_all'?'checked':''?> data-folder-id="<?=$folder['id']?>" value="apply_to_all" > Apply to All
                                            </p>
                                            <p>
                                                <label for="title"></label><br />
                                                <input type="radio" name="permission" value="choose_in_file" data-folder-id="<?=$folder['id']?>" <?=$folder['permission']=='choose_in_file'?'checked':''?> > Choose In File
                                            </p>

                                            <p class="choose_in_file_section <?=$folder['permission']!='choose_in_file'?'hide':''?> " id="choose_in_file_section-<?=$folder['id']?>">
                                                <select id="select2-<?=$folder['id']?>" class="select2" name="contacts[]" multiple="multiple">
                                                    <?php foreach($contacts as $contact){?>
                                                        <option value="<?=$contact['entity_id']?>" <?=$folder['permission']=='choose_in_file' && in_array($contact['entity_id'],$folder['users'])?'selected':''?> ><?=$contact['firstname'].' '.$contact['surname']?></option>
                                                    <?php } ?>
                                                </select>
                                            </p>

                                            <p>
                                                <input type="submit" class="btn btn-primary" id="submit-btn-<?=$folder['id']?>" value="Submit" />
                                            </p>
                                        </form>

                                    </td>
                                </tr>
                            <?php endforeach;?>
                        </table>
                    </div>

                </div>
            </div>
        </div><!-- l-wrap -->
    </section><!-- section -->




    <!-- Final ============================== -->


</div><!-- container -->

<!-- Load libraries -->
<script>
    head.js(
// Place scripts to be loaded here, in the order the need to be executed
// Typically libraries are needed first, such as jQuery (as the plugins
// depend on jQuery to execute). The below configuration is what Rock
// Hammer uses, but customise this for your own needs.
// Notice the use of @path here. This is because we need the file names,
// not the script tags being generated. Ensure all filenames you wish
// head.js to load are surrounded with quotes. Also, this is a list, so
// ensure that all but the last item are followed by a comma.

// Plugins
        "/js/vendor/jquery/jquery.scrollTo.min.js",

// Load bootstrap-transition first so that nice glides/fades
// etc for the other bootstrap plugins work
        "/js/vendor/bootstrap/bootstrap-transition.js",
// "js/vendor/bootstrap/bootstrap-carousel.js",
// "js/vendor/bootstrap/bootstrap-tooltip.js",

// Popover has a dependency on tooltip, so make sure and include
// bootstrap-tooltip regardless in order for popovers to work
// "js/vendor/bootstrap/bootstrap-popover.js",
        "/js/vendor/bootstrap/bootstrap-modal.js",
        "/js/vendor/bootstrap/bootstrap-collapse.js",
        "/js/vendor/bootstrap/bootstrap-tab.js",
        "/js/vendor/bootstrap/bootstrap-select2.js",
// Responsive data tables
// "js/rwd-table.js",

// Remove any navigation patterns below that arent used
// "js/nav-patterns/nav-toggle.js",
// "js/nav-patterns/left-nav-flyout.js",
        "/js/nav-patterns/responsive-nav.min.js",

// "js/nav-patterns/responsive-breadcrumb.js",

// Functionality only used by Rock Hammer - this should be removed in production projects!
// "js/navigation-manager.js"
    );

    // Google Analytics code
    // Set your account ID appropriately by replacing UA-XXXXX-X
    // var _gaq=[['_setAccount','UA-XXXXX-X'],['_trackPageview']];
    // (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
    //     g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
    //     s.parentNode.insertBefore(g,s)}(document,'script'));
</script>
<script src="https://unpkg.com/ionicons@4.1.2/dist/ionicons.js"></script>

<script>

    $(document).ready(function () {
         initVault();
    });

</script>
<script type="text/javascript" src="/js/vendor/dropzone.js"></script>







