<?php
include_once('../../../config.php');

$user_id=$_SESSION['user_id'];
$contacts=$db->fetchAll('select entity_id, firstname,surname from entity where parent_id='.$user_id.' and role_id=10');
$folder_already_exists=false;
if(isset($_POST['folder_name'])){
    $folder_name=$_POST['folder_name'];
    $permission=$_POST['permission'];

    $check_folder=$db->fetchAll('select * from folders where name="'.$folder_name.'" and user_id='.$user_id);
    if(!$check_folder) {
        $folder_id = $db->insert(['name' => $folder_name, 'permission' => $permission, 'user_id' => $user_id, 'created_at' => date('Y-m-d H:i:s')], 'folders');
        if ($permission == 'choose_in_file') {
            $contacts_ids = $_POST['contacts'];
            foreach ($contacts_ids as $contacts_id) {
                $db->insert(['folder_id' => $folder_id, 'user_id' => $contacts_id, 'created_at' => date('Y-m-d H:i:s')], 'user_folders');
            }
        } else {
            foreach ($contacts as $contact) {
                $db->insert(['folder_id' => $folder_id, 'user_id' => $contact['entity_id'], 'created_at' => date('Y-m-d H:i:s')], 'user_folders');
            }
        }

        $co_brokers=$db->fetchAll('select * from entity where parent_id='.$user_id.' and role_id=15 order by entity_id desc');
       
        $db->insert(['folder_id' => $folder_id, 'user_id' => $user_id, 'created_at' => date('Y-m-d H:i:s')], 'user_folders');

        foreach($co_brokers as $co_broker){
            $db->insert(['folder_id' => $folder_id, 'user_id' => $co_broker['entity_id'], 'created_at' => date('Y-m-d H:i:s')], 'user_folders');
        }
    }else{

        $folder_already_exists=true;
    }
}

$dfolders=$db->fetchAll('select * from folders where user_id='.$user_id.' and is_default=0 order by id desc');

$folders=[];
foreach($dfolders as $folder){
    $user_folders=$db->fetchAll('select user_id from user_folders where folder_id='.$folder['id']);
    $user=[];
    foreach($user_folders as $user_folder){
        $users[]=$user_folder['user_id'];
    }
    $folder['users']=$users;
    $folders[]=$folder;
}

$messageFolder = $db->fetchAll('select * from folders where user_id='.$user_id.' and name="Messages" and is_default=1 order by id desc');

