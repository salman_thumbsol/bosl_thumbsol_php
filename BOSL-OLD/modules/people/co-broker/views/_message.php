<li class="vault-message <?=$message['parent_id']!=null?'vault-message-reply':''?>" data-id="<?=$message['id']?>" id="message-<?=$message['id']?>">

    <div class="secondary">
        <h4 class="author-name"><?=$message['firstname']?>&nbsp<?=$message['surname']?>
            <span class="time"><?=$message['created_at']?></span>
        </h4>
        <p class="message-text"><?=$message['message']?></p>
        <?php if($message['document_path']!=''){?>
            <p>
                <a href="<?=ROOT_URL.'/uploads/'.$message['document_path']?>" >
                    <?php if(isImage(ROOT_URL.'/uploads/'.$message['document_path'])){?>
                        <img src="<?=ROOT_URL.'/uploads/'.$message['document_path']?>" width="40" />
                    <?php  }else{?>
                        <?=$message['document_path']?>
                    <?php } ?>
                </a>
            </p>
        <?php }?>
    </div>

</li>