<?php include_once(dirname(__FILE__).'/../controllers/edit_contact.php'); ?>

<link rel="stylesheet" href="https://rawgit.com/enyo/dropzone/master/dist/dropzone.css">
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

    <h3 id="modal-title-client">Client Details: <?=$row['firstname']?> <?=$row['surname']?></h3>
</div>
<div class="vault modal-body">
    <div class="tabbable">
        <ul class="nav nav-tabs" id="contact-tabs">
            <li><a href="#tab1" data-toggle="tab">Details</a></li>
            <li class="active"><a href="#tab2" data-toggle="tab">Vault</a></li>
            <li><a href="#tab3" data-toggle="tab">Tasks</a></li>
            <li><a href="#tab4" data-toggle="tab">Activity</a></li>
            <li><a href="#tab5" data-toggle="tab">Notes</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane" id="tab1">
                <p>Use the form below to update details.</p>
                <form method="post" id="update-contact-form" onsubmit="return updatecontact();">
                    <div class="form-response"></div>
                    <input type="hidden" name="entity_id" id="update_entity_id" value="<?=$row['entity_id']?>" />
                    <input type="hidden" name="action" value="update" />
                    <div class="row ">
                        <div class="col span-1-of-4">
                            <p>Title </p>
                            <p>
                                <select name="title" required>
                                    <option value="Mr" <?=($row['title']=='Mr')?'selected="selected"':'' ?>>Mr</option>
                                    <option value="Miss" <?=($row['title']=='Miss')?'selected="selected"':'' ?>>Miss</option>
                                    <option value="Mrs" <?=($row['title']=='Mrs')?'selected="selected"':'' ?>>Mrs</option>
                                    <option value="Ms" <?=($row['title']=='Ms')?'selected="selected"':'' ?>>Ms</option>
                                </select>
                            </p>
                        </div>

                        <div class="col span-1-of-4">
                            <p><label for="time-zone">Time Zone</label></p>
                            <p>
                                <select name="time_zone" id="time-zone">
                                    <option value="">Please Select</option>
                                    <?php foreach(getTimeZones() as $key=>$value):?>
                                        <option value="<?=$key?>" <?=$key==$row['time_zone']?'selected="selected"':''?> ><?=$value?></option>
                                    <?php endforeach;?>
                                </select>
                            </p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col span-1-of-2">
                            <p><label  for="text">First Name</label><br>
                                <input type="text" class="firstname" name="firstname" value="<?=$row['firstname']?>" placeholder="Novak" required></p>
                        </div>
                        <div class="col span-1-of-2">
                            <p><label for="text">Surname</label><br>
                                <input type="text" class="surname" name="surname" value="<?=$row['surname']?>" placeholder="Djokovic" required></p>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col span-1-of-2">
                            <p><label for="email">Email Address</label><br>
                                <input type="email" class="email" name="email" disabled="disabled" value="<?=$row['email']?>" placeholder="novak@novakdjokovic.com" required></p>
                        </div>
                        <div class="col span-1-of-2">
                            <p><label for="tel">Telephone Number</label><br>
                                <input type="tel" class="mobile" name="mobile" value="<?=$row['mobile']?>" placeholder="000-00-000-000" required></p>
                        </div>
                    </div>





                    <p><label for="textarea">Client Notes</code></label><br>
                        <textarea id="textarea" name="clients_notes"><?=$row['clients_notes']?></textarea></p>
                    <!--<button class="btn" type="reset" data-dismiss="modal" aria-hidden="true">Clear</button>-->
                    <button type="submit" id="submit" class="btn btn-primary">Update</button>
                </form>
            </div><!-- tab-pane -->
            <!-- Next -->
            <div class="tab-pane active" id="tab2">
                <div class="row vault">

                    <div class="tabbable tabs-left">
                        <ul class="nav nav-tabs" id="vault-tabs">
                            <?php foreach($folders as $folder){?>
                                <li class="<?=$folder['is_default']==1?'active':''?>">
                                    <a data-folder-id="<?=$folder['id']?>" data-entity-id="<?=$row['entity_id']?>" href="#sub_tab<?=$folder['id']?>" class="<?=$folder['is_default']==1?'waypoint':''?>" data-toggle="tab"><?=$folder['name']?></a>
                                </li>
                            <?php }?>
                        </ul>
                        <div class="tab-content">
                            <?php foreach($folders as $folder){?>
                                <div class="tab-pane <?=$folder['is_default']==1?'active':''?>" id="sub_tab<?=$folder['id']?>">

                                    <form method="post" action="" data-folder-id="<?=$folder['id']?>" onsubmit="return savemessage(this)" enctype="multipart/form-data">
                                        <div class="row">

                                                <p><label for="textarea"></code></label><br>
                                                    <input type="hidden" name="folder_id" value="<?=$folder['id']?>" />
                                                    <input type="hidden" name="entity_id" value="<?=$row['entity_id']?>" />
                                                    <input type="hidden" name="action" value="send_vault_message" />
                                                    <textarea name="message" id="message-text-<?=$folder['id']?>" placeholder="Enter message here..." required></textarea></p>

                                            <?php if($folder['is_default']!=1){?>
                                                 <p>
                                                  <div id="id_dropzone_<?=$folder['id']?>" data-folder_id="<?=$folder['id']?>" class="dropzone document_dropzone" name="document"></div>
<!--                                                     <input type="file" id="file---><?//=$folder['id']?><!--" name="document" />-->
                                                </p>
                                            <?php } ?>
                                        </div>


                                        <!--<button class="btn" type="reset" data-dismiss="modal" aria-hidden="true">Clear</button>-->
                                        <button type="<?=$folder['is_default']!=1?'submit':'submit'?>" id="save-in-folder-btn1-<?=$folder['id']?>" class="btn btn-primary"><?=$folder['is_default']!=1?'Save':'Send'?></button>
                                    </form>

                                    <ul class="messages-list vault-messsage-list" id="vault-messsage-list-<?=$folder['id']?>">
                                        <?php foreach ($messages as $message):?>
                                        <?php if($message['folder_id']==$folder['id']){?>
                                                <?php
                                                ob_start();
                                                include('_message.php');
                                                $content = ob_get_contents();
                                                ob_end_clean();
                                                echo $content;
                                                ?>
                                        <?php }?>
                                        <?php endforeach;?>

                                    </ul>


                                </div>
                            <?php }?>

                        </div>
                    </div>

                </div>
            </div><!-- tab-pane -->
            <!-- Next -->
            <div class="tab-pane" id="tab3">
                <p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec varius ante sed mi malesuada, ac sollicitudin nisi vulputate. Mauris scelerisque, odio id cursus finibus, nulla diam condimentum nulla, nec condimentum neque velit a justo.</p>
            </div><!-- tab-pane -->
            <!-- Next -->
            <div class="tab-pane" id="tab4">
                <p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec varius ante sed mi malesuada, ac sollicitudin nisi vulputate. Mauris scelerisque, odio id cursus finibus, nulla diam condimentum nulla, nec condimentum neque velit a justo.</p>
            </div><!-- tab-pane -->
            <!-- Next -->
            <div class="tab-pane" id="tab5">

                    <div class="notes-log" id="notes-container">
                        <?php if(count($notes)>0){?>
                            <?php foreach($notes as $note){?>
                                <div class="note-single">
                                    <p><?=$note['firstname']?></p>
                                    <p><em><?=$note['created_at']?></em></p>
                                    <p><?=$note['notes']?></p>
                                </div>
                            <?php } ?>
                        <?php } ?>
                    </div>
                <div>
                    <form onsubmit="return savenotes()" id="notes-form">
                        <input type="hidden" name="action" value="save_notes" />
                        <input type="hidden" name="entity_id" value="<?=$row['entity_id']?>" />
                        <textarea name="notes" placeholder="Type Note here" required></textarea>
                        <input type="submit" value="Add Notes" class="btn btn-primary pull-right" />
                    </form>
                </div>

            </div><!-- tab-pane -->
        </div><!-- tab-content -->
    </div><!-- tabbable -->
</div>
<script>

    initeditfolder(<?=$row['entity_id']?>);
    new BOSLWayPoint(<?=$system_folder_id?>,<?=$row['entity_id']?>).init();

</script>

<div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
<!--   <button type="submit" class="btn btn-primary" form="update-contact-form">Send</button>-->
    <!-- <button class="btn btn-error">Delete</button> -->
</div>

