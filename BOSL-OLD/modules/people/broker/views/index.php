<a href="#navigation-toggle" class="navigation-toggle">Menu</a>

<nav id="subnav" role="subnav">
    <div class="subnav-header">
        <ion-icon name="people"></ion-icon>
    </div>
    <ul>
        <li><a href="#add-contact" data-toggle="modal">Add Contact</a></li>

        <li><a href="<?=$total_co_brokers <2?'#add-level-1-user':'#level-one-user-exceeded-modal'?>" title="<?=$total_co_brokers <2?'':'You have reached the limit'?>" data-toggle="modal">Add Level One User </a></li>

        <li><a href="#">Third Link</a></li>
        <li><a href="#">Fourth Link</a></li>
    </ul>
    <!-- Add Contact Form -->

    <div id="add-contact" class="modal hide fade modal-secondary" tabindex="-1" role="dialog" aria-labelledby="add-contact" aria-hidden="true">
        <form method="post" id="contact-form" onsubmit="return savecontact();">
            <input type="hidden" name="action"  value="insert" />
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="modal-title">Add Contact</h3>
            </div>
            <div class="modal-body">
                <p>Use the form below to add a contact.</p>
                <div class="form-response"></div>

                <div class="row ">
                    <div class="col span-1-of-4">
                        <p>Title </p>
                        <p>
                            <select name="title" required>
                                <option value="Mr">Mr</option>
                                <option value="Miss">Miss</option>
                                <option value="Mrs">Mrs</option>
                                <option value="Ms">Ms</option>
                            </select>
                        </p>
                    </div>
                    <div class="col span-1-of-4">
                        <p><label for="time-zone">Time Zone</label></p>
                        <p>
                            <select name="time_zone" id="time-zone">
                                <option value="">Please Select</option>
                                <?php foreach(getTimeZones() as $key=>$value):?>
                                    <option value="<?=$key?>" <?=$key=='Europe/London'?'selected':''?> ><?=$value?></option>
                                <?php endforeach;?>
                            </select>
                        </p>
                    </div>
                </div>

                <div class="row">
                    <div class="col span-1-of-2">
                        <p><label for="first_name">First Name</label><br>
                            <input type="text" id="first_name" name="firstname" placeholder="First Name" required></p>
                    </div>
                    <div class="col span-1-of-2">
                        <p><label for="surname">Surname</label><br>
                            <input type="text" id="surname" name="surname" placeholder="Surname" required></p>
                    </div>
                </div>

                <div class="row">
                    <div class="col span-1-of-2">
                        <p><label for="email">Email Address</label><br>
                            <input type="email" id="email" name="email" placeholder="Email Address" required></p>
                    </div>
                    <div class="col span-1-of-2">
                        <p><label for="tel">Telephone Number</label><br>
                            <input type="tel" id="mobile" name="mobile" placeholder="000-00-000-000" required></p>
                    </div>
                </div>


                <p><label for="clients_notes">Client Notes</code></label><br>
                    <textarea id="clients_notes" name="clients_notes"> </textarea></p>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn" id="close-add-contact-modal">Close</button>
                <button type="submit" name="submit" id="submit-addc-contact-form" class="btn btn-primary">Save</button>
                <!-- <button class="btn btn-error">Delete</button> -->
            </div>
        </form>
    </div>

    <?php if($total_co_brokers <2){?>
    <div id="add-level-1-user" class="modal hide fade modal-secondary" tabindex="-1" role="dialog" aria-labelledby="add-level-1-user" aria-hidden="true">
        <form method="post" id="add-level-1-user-form" onsubmit="return saveleveloneuser();">
            <input type="hidden" name="action"  value="add-level-one-user" />
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="modal-title">Add Level One User</h3>
            </div>
            <div class="modal-body">
                <p>Use the form below to add a contact.</p>
                <div class="form-response"></div>

                <div class="row ">
                    <div class="col span-1-of-4">
                        <p>Title </p>
                        <p>
                            <select name="title" required>
                                <option value="Mr">Mr</option>
                                <option value="Miss">Miss</option>
                                <option value="Mrs">Mrs</option>
                                <option value="Ms">Ms</option>
                            </select>
                        </p>
                    </div>
                    <div class="col span-1-of-4">
                        <p><label for="time-zone">Time Zone</label></p>
                        <p>
                            <select name="time_zone" id="time-zone">
                                <option value="">Please Select</option>
                                <?php foreach(getTimeZones() as $key=>$value):?>
                                    <option value="<?=$key?>" <?=$key=='Europe/London'?'selected':''?> ><?=$value?></option>
                                <?php endforeach;?>
                            </select>
                        </p>
                    </div>
                </div>

                <div class="row">
                    <div class="col span-1-of-2">
                        <p><label for="first_name">First Name</label><br>
                            <input type="text" id="level_first_name" name="firstname" placeholder="First Name" required></p>
                    </div>
                    <div class="col span-1-of-2">
                        <p><label for="surname">Surname</label><br>
                            <input type="text" id="surname" name="surname" placeholder="Surname" required></p>
                    </div>
                </div>

                <div class="row">
                    <div class="col span-1-of-2">
                        <p><label for="email">Email Address</label><br>
                            <input type="email" id="email" name="email" placeholder="Email Address" required></p>
                    </div>
                    <div class="col span-1-of-2">
                        <p><label for="tel">Telephone Number</label><br>
                            <input type="tel" id="mobile" name="mobile" placeholder="000-00-000-000" required></p>
                    </div>
                </div>


                <p><label for="clients_notes">Client Notes</code></label><br>
                    <textarea id="clients_notes" name="clients_notes"> </textarea></p>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn" id="close-add-level-modal">Close</button>
                <button type="submit" name="submit" id="submit-add-level1-form" class="btn btn-primary">Save</button>
                <!-- <button class="btn btn-error">Delete</button> -->
            </div>
        </form>
    </div>
<?php } ?>

    <div id="level-one-user-exceeded-modal" class="modal hide fade modal-secondary" tabindex="-1" role="dialog" aria-labelledby="level-one-user-exceeded-modal" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="modal-title">Limit Exceeded</h3>
        </div>
        <div class="modal-body">
            <p>You cannot create more than 2 Level One Users</p>
        </div>
    </div>
</nav>

<div class="container">

    <!-- Next ============================== -->

    <section>

        <?php if(count($co_brokers)>0){?>
        <div class="l-wrap">
            <div class="l-full-width">
                <table class="table" >

                    <thead>
                    <tr>
                        <th scope="col" data-tablesaw-sortable-col data-tablesaw-sortable-default-col data-tablesaw-priority="persist">Name</th>
                        <th scope="col" data-tablesaw-sortable-col data-tablesaw-sortable-default-col data-tablesaw-priority="persist">Surname</th>
                        <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="2">Email</th>
                        <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="3">Phone</th>
                        <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="5">Contract</th>


                    </tr>
                    </thead>
                    <tbody id="contacts-tbody">
                    <?php foreach ($co_brokers as $row):?>
                        <tr id="row-<?=$row['entity_id']?>">
                            <td class="first_name">
                                <!-- <a href="javascript:void(0)" data-id="<?=$row['entity_id']?>" class="client-detail-btn"> -->
                                    <?php echo $row['firstname'];?>
                                <!-- </a> -->
                            </td>
                            <td class="surname">
                                <!-- <a href="javascript:void(0)" data-id="<?=$row['entity_id']?>" class="client-detail-btn"> -->
                                    <?php echo $row['surname'];?>
                                <!-- </a> -->
                            </td>
                            <td class="email"><?php echo $row['email']; ?></td>
                            <td class="phone"><?php echo $row['mobile']; ?></td>
                            <td class="send-access-btns">
                                <?php if($row['password_hash']==''){?>
                                <a href="javascript:void(0)" id="send-invite-btn-<?=$row['entity_id']?>" onclick="openaccessmodal(<?=$row['entity_id']?>,'<?=$row['email']?>','<?=$row['firstname'].' '.$row['surname']?>')" class="btn btn-primary btn-small">Send Invite</a>
                                <?php }?>
                            </td>


                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
        <?php }?>

        <div class="l-wrap">
            <div class="l-full-width">

                <div class="table-wrapper">

                    <div class="tablesaw-overflow">



                        <?php if(count($rows)>0){?>
                       <table class="tablesaw" id="table-saw" data-tablesaw-mode="columntoggle" data-tablesaw-sortable data-tablesaw-sortable-switch data-tablesaw-minimap data-tablesaw-mode-switch>

                            <thead>
                            <tr>
                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-sortable-default-col data-tablesaw-priority="persist">Name</th>
                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-sortable-default-col data-tablesaw-priority="persist">Surname</th>
                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="2">Email</th>
                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="3">Phone</th>
                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="5">Contract</th>

                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4">Archive</th>
                            </tr>
                            </thead>
                            <tbody id="contacts-tbody">
                            <?php foreach ($rows as $row):?>
                                    <tr id="row-<?=$row['entity_id']?>">
                                    <td class="first_name">
                                        <a href="javascript:void(0)" data-id="<?=$row['entity_id']?>" class="client-detail-btn">
                                        <?php echo $row['firstname'];?>
                                        </a>
                                    </td>
                                    <td class="surname">
                                        <a href="javascript:void(0)" data-id="<?=$row['entity_id']?>" class="client-detail-btn">
                                            <?php echo $row['surname'];?>
                                        </a>
                                    </td>
                                    <td class="email"><?php echo $row['email']; ?></td>
                                    <td class="phone"><?php echo $row['mobile']; ?></td>
                                    <td class="send-access-btns">
                                        <?php if($row['password_hash']==''){?>
                                        <a href="javascript:void(0)" id="send-invite-btn-<?=$row['entity_id']?>" onclick="openaccessmodal(<?=$row['entity_id']?>,'<?=$row['email']?>','<?=$row['firstname'].' '.$row['surname']?>')" class="btn btn-primary btn-small">Send Invite</a>
                                        <?php } ?>
                                    </td>

                                    <td><a href="#" class="btn btn-small">Archive</a></td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                        <?php } ?>
                    </div><!-- /tablesaw-overflow -->

                    <!-- Send Access -->
                <div id="send-access" class="modal hide fade modal-secondary" tabindex="-1" role="dialog" aria-labelledby="send-access" aria-hidden="true">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h3 id="modal-title">Send Access</h3>
                        </div>
                        <div class="modal-body">
                            <div class="form-response"></div>
                            <p>You're about to send access to <strong id="access_modal_name"></strong> at the email address <strong id="access_modal_email"></strong>. Are you sure?</p>
                        </div>
                        <div class="modal-footer">
                            <button class="btn" data-dismiss="modal" aria-hidden="true">No, cancel</button>
                            <a href="#" id="send_access_yes" class="btn btn-primary"><i class="icon ion-refreshing"></i>Yes, please send</a>
                        </div>
                    </div>

                    <!-- Client Details -->
                    <div id="client-details" class="modal hide fade modal-secondary modal-lg client-details" tabindex="-1" role="dialog" aria-labelledby="client-details" aria-hidden="true">

                    </div>
                </div>

            </div><!-- l-summary -->

        </div><!-- l-wrap -->

    </section><!-- section -->


    <!-- Final ============================== -->


</div><!-- container -->

<!-- Load libraries -->
<script>
    head.js(
// Place scripts to be loaded here, in the order the need to be executed
// Typically libraries are needed first, such as jQuery (as the plugins
// depend on jQuery to execute). The below configuration is what Rock
// Hammer uses, but customise this for your own needs.
// Notice the use of @path here. This is because we need the file names,
// not the script tags being generated. Ensure all filenames you wish
// head.js to load are surrounded with quotes. Also, this is a list, so
// ensure that all but the last item are followed by a comma.

// Plugins
        "/js/vendor/jquery/jquery.scrollTo.min.js",

// Load bootstrap-transition first so that nice glides/fades
// etc for the other bootstrap plugins work
        "/js/vendor/bootstrap/bootstrap-transition.js",
// "js/vendor/bootstrap/bootstrap-carousel.js",
// "js/vendor/bootstrap/bootstrap-tooltip.js",

// Popover has a dependency on tooltip, so make sure and include
// bootstrap-tooltip regardless in order for popovers to work
// "js/vendor/bootstrap/bootstrap-popover.js",
        "/js/vendor/bootstrap/bootstrap-modal.js",
        "/js/vendor/bootstrap/bootstrap-collapse.js",
        "/js/vendor/bootstrap/bootstrap-tab.js",

// Responsive data tables
// "js/rwd-table.js",

// Remove any navigation patterns below that arent used
// "js/nav-patterns/nav-toggle.js",
// "js/nav-patterns/left-nav-flyout.js",
        "/js/nav-patterns/responsive-nav.min.js",
// "js/nav-patterns/responsive-breadcrumb.js",

// Functionality only used by Rock Hammer - this should be removed in production projects!
// "js/navigation-manager.js"
    );

    // Google Analytics code
    // Set your account ID appropriately by replacing UA-XXXXX-X
    // var _gaq=[['_setAccount','UA-XXXXX-X'],['_trackPageview']];
    // (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
    //     g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
    //     s.parentNode.insertBefore(g,s)}(document,'script'));
</script>
<script src="https://unpkg.com/ionicons@4.1.2/dist/ionicons.js"></script>

<script>
$(document).ready(function () {
    initProfile();
});

</script>
<script type="text/javascript" src="/js/vendor/dropzone.js"></script>
