<!doctype html>

<!-- 
Rock Hammer by Andy Clarke
Version: 0.1
URL: http://stuffandnonsense.co.uk/projects/rock-hammer/
Apache License: v2.0. http://www.apache.org/licenses/LICENSE-2.0
-->
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html class="no-js lt-ie9 lt-ie8" lang="en"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html class="no-js lt-ie9" lang="en"><![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"><!--<![endif]-->

<head>
<title>Back Office Solution</title>

        <!-- Hammer reload -->
          <script>
          /*
            setInterval(function(){
              try {
                if(typeof ws != 'undefined' && ws.readyState == 1){return true;}
                ws = new WebSocket('ws://'+(location.host || 'localhost').split(':')[0]+':35353')
                ws.onopen = function(){ws.onclose = function(){document.location.reload()}}
                window.onbeforeunload = function() { ws = null }
                ws.onmessage = function(){
                  var links = document.getElementsByTagName('link');
                    for (var i = 0; i < links.length;i++) {
                    var link = links[i];
                    if (link.rel === 'stylesheet' && !link.href.match(/typekit/)) {
                      href = link.href.replace(/((&|\?)hammer=)[^&]+/,'');
                      link.href = href + (href.indexOf('?')>=0?'&':'?') + 'hammer='+(new Date().valueOf());
                    }
                  }
                }
              }catch(e){}
            }, 1000)
            */
          </script>
        <!-- /Hammer reload -->
      
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="cleartype" content="on">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<meta name="description" content="">
<meta name="author" content="">

<!-- For all browsers -->
<link rel='stylesheet' href='<?=$root?>/js/tablesaw/dependencies/qunit.css'>
<link rel='stylesheet' href='<?=$root?>/js/tablesaw/stackonly/tablesaw.stackonly.css'>
<link rel='stylesheet' href='<?=$root?>/css/rock-hammer.css'>

<!-- JavaScript -->
<script src='<?=$root?>/js/vendor/head.load.min.js'></script>
<script src='<?=$root?>/js/vendor/modernizr-2.6.2-min.js'></script>
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<!--<script src='<?=$root?>/js/vendor/jquery-1.8.3-min.js'></script>-->
<script src='<?=$root?>/js/tablesaw/tablesaw.js'></script>
<script src='<?=$root?>/js/tablesaw/tablesaw-init.js'></script>

<!--[if (lt IE 9) & (!IEMobile)]>
<script src='js/vendor/selectivizr-min.js'></script>
<link rel='stylesheet' href='css/lte-ie8.css'>
<![endif]-->

<link rel="shortcut icon" href="<?=$root?>/img/favicon.ico">
<link rel="shortcut icon" href="<?=$root?>/img/favicon.png" sizes="32x32">
<!-- <link rel="apple-touch-icon-precomposed" href="img/l/apple-touch-icon-precomposed.png"> -->
<!-- <link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/m/apple-touch-icon-72x72-precomposed.png"> -->
<!-- <link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/h/apple-touch-icon-114x114-precomposed.png"> -->
<!-- <link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/h/apple-touch-icon-144x144-precomposed.png"> -->
<!--iOS -->
<!-- <meta name="apple-mobile-web-app-title" content="Rock Hammer"> -->
<!-- <meta name="viewport" content="initial-scale=1.0"> (Use if apple-mobile-web-app-capable below is set to yes) -->
<!-- <meta name="apple-mobile-web-app-capable" content="yes"> -->
<!-- <meta name="apple-mobile-web-app-status-bar-style" content="black"> -->
<!-- Startup images -->
<!-- <link rel="apple-touch-startup-image" href="img/startup/startup-320x460.png" media="screen and (max-device-width:320px)"> -->
<!-- <link rel="apple-touch-startup-image" href="img/startup/startup-640x920.png" media="(max-device-width:480px) and (-webkit-min-device-pixel-ratio:2)"> -->
<!-- <link rel="apple-touch-startup-image" href="img/startup/startup-640x1096.png" media="(max-device-width:548px) and (-webkit-min-device-pixel-ratio:2)"> -->
<!-- <link rel="apple-touch-startup-image" sizes="1024x748" href="img/startup/startup-1024x748.png" media="screen and (min-device-width:481px) and (max-device-width:1024px) and (orientation:landscape)"> -->
<!-- <link rel="apple-touch-startup-image" sizes="768x1004" href="img/startup/startup-768x1004.png" media="screen and (min-device-width:481px) and (max-device-width:1024px) and (orientation:portrait)"> -->
<!-- Windows 8 / RT -->
<meta name="msapplication-TileImage" content="<?=$root?>/img/h/apple-touch-icon-144x144-precomposed.png">
<meta name="msapplication-TileColor" content="#000">

</head>

<body class="default" id="bos">
<a href="#navigation-toggle" class="navigation-toggle">Menu</a>
<nav id="navigation-toggle" role="navigation">
    <div class="nav-section">
        <a href="/" class="logo" title="Back Office Solutions"><img src="<?=$root?>/img/logo-white.png" /></a>
        <ul>
        <li><a href="<?=$root?>/modules/people/broker/" class="active" id='nav-people'><ion-icon name="people"></ion-icon> BOS People</a></li>
        <li><a href="<?=$root?>/modules/vault/broker/" id='nav-vault'><ion-icon name="key"></ion-icon> BOS Vault</a></li>
        <li><a href="#"><ion-icon name="list-box" ></ion-icon> BOS Pipeline</a></li>
        <li><a href="#"><ion-icon name="calculator"></ion-icon> BOS Reconciler</a></li>
        <li><a href="<?=$root?>/modules/profile/broker" class="profile">Profile</a></li>
        </ul>
    </div>
</nav>


<?= $appcontent ?>


<!-- Final ============================== -->

<footer role="contentinfo">
<small>
</small>
</footer>
</div><!-- container -->

<!-- Load libraries -->
<script>
head.js(
// Place scripts to be loaded here, in the order the need to be executed
// Typically libraries are needed first, such as jQuery (as the plugins 
// depend on jQuery to execute). The below configuration is what Rock 
// Hammer uses, but customise this for your own needs.
// Notice the use of @path here. This is because we need the file names, 
// not the script tags being generated. Ensure all filenames you wish
// head.js to load are surrounded with quotes. Also, this is a list, so
// ensure that all but the last item are followed by a comma.

// Plugins
"<?=$root?>/js/vendor/jquery/jquery.scrollTo.min.js",

// Load bootstrap-transition first so that nice glides/fades 
// etc for the other bootstrap plugins work
"<?=$root?>/js/vendor/bootstrap/bootstrap-transition.js",
// "js/vendor/bootstrap/bootstrap-carousel.js",
// "js/vendor/bootstrap/bootstrap-tooltip.js",

// Popover has a dependency on tooltip, so make sure and include 
// bootstrap-tooltip regardless in order for popovers to work
// "js/vendor/bootstrap/bootstrap-popover.js",
"<?=$root?>/js/vendor/bootstrap/bootstrap-modal.js",
"<?=$root?>/js/vendor/bootstrap/bootstrap-collapse.js",

// Responsive data tables
// "js/rwd-table.js",

// Remove any navigation patterns below that arent used
// "js/nav-patterns/nav-toggle.js",
// "js/nav-patterns/left-nav-flyout.js",
"<?=$root?>/js/nav-patterns/responsive-nav.min.js",
// "js/nav-patterns/responsive-breadcrumb.js",

// Functionality only used by Rock Hammer - this should be removed in production projects!
// "js/navigation-manager.js"
);

 
</script>
<script src="https://unpkg.com/ionicons@4.1.2/dist/ionicons.js"></script>

<script>
$(function () {


   function loadModalData(div, page){
     alert(2);
       $('#'+div).load(page);
    
  }





   $('.nav-tabs a').on('click',function(e) {
    e.preventDefault();
    var link = $(this).attr('href').replace('#','');
    $('.nav-tabs a').each(function() {
      if($(this).attr('href')==='#'+link) {
        $(this).parent().addClass('active');
      } else {
        $(this).parent().removeClass('active');
      }
    })
    $('.tab-pane').each(function() {
      if($(this).attr('id')===link) {
        $(this).addClass('active');
      } else {
        $(this).removeClass('active');
      }
    })
  })
})
</script>
</body>
</html>