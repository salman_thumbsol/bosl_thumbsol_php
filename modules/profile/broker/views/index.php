<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>

<style>
	.container:after { /*clear float*/
  content: "";
  display: table;
  clear: both;
}

.section {
  float: left;
  width: 45%;

  }
 
@media (max-width: 768px) { /*breakpoint*/
  .section {
    float: none;
    width: auto;
  }
}
</style>

<nav id="subnav" role="subnav">
	<div class="subnav-header">
		<ion-icon name="person"></ion-icon>
	</div>
	<ul>
		<li><a href="#add-contact" data-toggle="modal">Profile and settings</a></li>

		<?
			if($_GET["id"]){
		?>
		<li><a href="#add-contact" data-toggle="modal">Add Notes</a></li>
		<?
			}
		?>
		<!--<li><a href="?archive=1">Valutations</a></li>-->

		 
		<!-- <li><a href="#addForm-modal" data-toggle="modal">Add Folder</a></li>-->
	</ul>
  
</nav>


<div class="container">

<!-- Next ============================== -->

<section>


<div class="l-wrap">
 
 <div class="container">
  <div class="section" style="margin-right: 30px;">
  	<div class="page-title">
		<h2>Profile</h2>
	</div>
		<form class="profile" name='profile' id='profile'>

		
		<p><label for="title">Title</label><br />
		<input type="text" id="title" name='title' placeholder="Mr" value="<?=$profile["title"]?>"></p>
		<input type="hidden" id="User_id" name='User_id'  value="<?=$_SESSION['User_id']?>">
		<p><label for="first-name">First Name</label><br />
		<input type="text" id="first-name" name='Firstname' value="<?=$profile["Firstname"]?>"></p>

		<p><label for="surname">Surname</label><br />
		<input type="text" id="surname" name='Surname' value="<?=$profile["Surname"]?>"></p>

		<p><label for="time-zone">Time Zone</label><br /> 
		<?=timezones("Timezone", $profile["Timezone"])?></p>

		<p><label for="Date Of Birth" name='dob-day'>Date Of Birth</label><br />
			<div class="dob">
				<select name="day" id="day">
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
					<option value="6">6</option>
					<option value="7">7</option>
					<option value="8">8</option>
					<option value="9">9</option>
					<option value="10">10</option>
					<option value="11">11</option>
					<option value="12">12</option>
					<option value="13">13</option>
					<option value="14">14</option>
					<option value="15">15</option>
					<option value="16">16</option>
					<option value="17">17</option>
					<option value="18">18</option>
					<option value="19">19</option>
					<option value="20">20</option>
					<option value="21">21</option>
					<option value="22">22</option>
					<option value="23">23</option>
					<option value="24">24</option>
					<option value="25">25</option>
					<option value="26">26</option>
					<option value="27">27</option>
					<option value="28">28</option>
					<option value="29">29</option>
					<option value="30">30</option>
					<option value="31">31</option>
				</select>

				<script>
					$('#day').val('<?=$dob[2]?>');

				</script>
				<select name="month" id="month" name='dob-month'>
					<option value="01">January</option>
					<option value="02">February</option>
					<option value="03">March</option>
					<option value="04">April</option>
					<option value="05">May</option>
					<option value="06">June</option>
					<option value="07">July</option>
					<option value="08">August</option>
					<option value="09">September</option>
					<option value="10">November</option>
					<option value="11">October</option>
					<option value="12">December</option>
				</select>

				<script>
					$('#month').val('<?=$dob[1]?>');

				</script>
				<select name="year" id="year" name='dob-year'>
					<option value="1950">1950</option>
					<option value="1951">1951</option>
					<option value="1952">1952</option>
					<option value="1953">1953</option>
					<option value="1954">1954</option>
					<option value="1955">1955</option>
					<option value="1956">1956</option>
					<option value="1957">1957</option>
				</select>
							<script>
					$('#year').val('<?=$dob[0]?>');

				</script>
			</div>

		<p><label for="address">Address</code></label><br>
		<textarea id="address" placeholder="Address" name='Address'> <?=$profile["Address"]?></textarea>

		<p><label for="postcode">Postcode</label><br />
		<input type="text" id="postcode" placeholder="Postcode" name='postcode' value="<?=$profile["postcode"]?>"></p>

		<p><label for="email-primary">Email (Primary)</label><br />
		<input type="email" id="email-primary" placeholder="Email (Primary)" name='Email' value="<?=$profile["Email"]?>"></p>

		<p><label for="email-secondary">Email (Secondary)</label><br />
		<input type="email" id="email-secondary" placeholder="Email (Secondary)" name='email_private' value="<?=$profile["email_private"]?>"></p>

		<p><label for="tel">Telephone</label><br />
		<input type="tel" id="tel" placeholder="Telephone" name='Telephone' value="<?=$profile["Telephone"]?>"></p>

		<p><label for="tel">Mobile</label><br />
		<input type="tel" id="tel" placeholder="Mobile" name='Mobile' value="<?=$profile["Mobile"]?>"></p>

		<p><label for="Country" name='country'>Country</label><br />
			<div class="country">
				<?=countries("country", $profile["country"])?>
			</div>

		<p><label for="" name=''>Client contract email</label><br />
			<div class="country">
				<textarea id="client_invite_email"   name='client_invite_email' cols="30" rows="5"><?=$profile["client_invite_email"]?></textarea>
 			</div>

	<p><label for="" name=''>Client contract email signature</label><br />
			<div class="country">
				<textarea id="email_signoff"   name='email_signoff' rows="5"><?=$profile["email_signoff"]?></textarea>
 			</div>


		<ul><li><label class="checkbox"><input type="checkbox" name='tandc' id='tandc' value='1' <?=( $profile["tandc"]) ? "checked" : ""?>> I’ve read and agree to the <a href="#cookie-policy" data-toggle="modal">Cookie Policy</a>, <a href="#terms-conditions" data-toggle="modal">Terms & Conditions</a>, <a href="#privacy-policy" data-toggle="modal">Privacy Policy</a> and <a href="#security-policy" data-toggle="modal">Security Policy</a>.</label></li></ul>

		<button class="btn btn-primary">Save Profile</button>

		<!-- Cookie Policy Modal -->
			<div id="cookie-policy" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="cookie-policy" aria-hidden="true">
			<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			<h3 id="modal-title">Cookie Policy</h3>
			</div>
			<div class="modal-body">
			<p>Cookie policy text...</p>

			</div>
			<div class="modal-footer">
			<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
			</div>
		</div> <!-- / Cookie Policy Modal -->
		<!-- Terms & Conditions Modal -->
			<div id="terms-conditions" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="terms-conditions" aria-hidden="true">
			<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			<h3 id="modal-title">Terms & Conditions</h3>
			</div>
			<div class="modal-body">
			<p>Terms & Conditions text...</p>

			</div>
			<div class="modal-footer">
			<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
			</div>
		</div> <!-- / Terms & Conditions Modal -->
		<!-- Privacy Policy Modal -->
			<div id="privacy-policy" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="privacy-policy" aria-hidden="true">
			<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			<h3 id="modal-title">Privacy Policy</h3>
			</div>
			<div class="modal-body">
			<p>Privacy policy text...</p>

			</div>
			<div class="modal-footer">
			<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
			</div>
		</div> <!-- / Privacy Policy Modal -->
		<!-- Security Policy Modal -->
			<div id="security-policy" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="security-policy" aria-hidden="true">
			<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			<h3 id="modal-title">Security Policy</h3>
			</div>
			<div class="modal-body">
			<p>Security policy text...</p>

			</div>
			<div class="modal-footer">
			<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
			</div>
		</div> <!-- / Security Policy Modal -->

		</form>
  </div>

  <div class="section">
  	  	<div class="page-title">
		<h2>Settings</h2>
	</div>

Pease select a client: 
<select name="client" id="client">
	<option value="">Please select a client</option>
	<?=$bosl->clientDropdown($_GET["id"])?>
</select>

  	<div class="folder-names">
  		<?
  		if(isset($_GET["id"])){
			// loop through folders
			$i=1;

	 		foreach ($folders as &$finfo) {
	  		?>
	  		<form action="" class='folderForm' id='form-<?=$finfo["uf_id"]?>'>
				 <label for="folder-1">Folder <?=$i?></label><input id="" name='name' type="text" class="folder-1" value='<?=$finfo["name"]?>' style='width: 70%'> 
				 <input id="uf_id" name='uf_id' type="hidden" class="folder-1" value='<?=$finfo["uf_id"]?>'> <button class="btn">Update</button>
			</form>	 
			<?

			$i++;
			}
		}
			?>
 		</div>
 		
  </div>

 </div>
</div><!-- l-wrap -->

</section><!-- section -->


<script>
	$(function() {
 

// update folders
$('.folderForm').on('submit', function() {
	event.preventDefault();

			dataString = $(this).serialize();
 
  		$.ajax({
			type: "POST",
			url: "<?= $root ?>/api/folder/update/",
			data: dataString,
			success: function(data) {
 				 location.reload();
			},
			error: function() {
				alert("There was a problems with your request. Please reload the page and try again ");
			},
			complete: function() {
				 
			}
		});

})



		$('#profile').on('submit', function() {


// format the DOB

dob = $('#year').val()+"-"+$('#month').val()+"-"+$('#day').val();
$('#year').val('');
$('#month').val('');
$('#day').val('');

			event.preventDefault();

if($('#tandc').prop("checked") == true){
			dataString = $('#profile').serialize()+"&dob="+dob;

  			 $.ajax({
			type: "POST",
			url: "<?= $root ?>/api/user/update/",
			data: dataString,
			success: function(data) {
 				 	location.reload();
			},
			error: function() {
				alert("There was a problems with your request. Please reload the page and try again ");
			},
			complete: function() {
				 
			}
		});
}else{

	alert('Please accept the terms and conditions');
}
		});


	});


$('#client').change(function(event) {
	location.href='?id='+this.value;
});

// chnge the nav class
$('.nav-section li').removeClass('active');
$('#nav-vault').addClass('active');
</script>