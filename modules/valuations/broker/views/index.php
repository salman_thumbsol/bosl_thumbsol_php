<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>

<nav id="subnav" role="subnav">
	<div class="subnav-header">
		<ion-icon name="key"></ion-icon>
	</div>
	<ul>
		<li><a href="?id=<?=$_GET["id"]?>" data-toggle="modal">Notes</a></li>

		<?
			if($_GET["id"]){
		?>
		<li><a href="#add-contact" data-toggle="modal">Add Notes</a></li>
		<?
			}
		?>
		<!--<li><a href="?archive=1">Valutations</a></li>-->

		<!-- FOLDERS ony displayed when there is a client selected-->
		<?
			if(isset($_GET["id"])){

 					// loop through folders
 						foreach ($folders as &$finfo) {
  
					 
		?>
		<li><a href="?id=<?=$_GET["id"]?>&folder=<?=$finfo["uf_id"]?>&fname=<?=$finfo["name"]?>" data-toggle="modal"><?=$finfo["name"]?></a></li>
		<?
			}
		}
		?>
		<!-- <li><a href="#addForm-modal" data-toggle="modal">Add Folder</a></li>-->
	</ul>
 

<!-- Add document Form -->
	<form action="../../../libs/upload.php?folder=<?=$_GET["folder"]?>&id=<?=$_GET["id"]?>"   method="post" enctype="multipart/form-data">
	<div id="add-doc" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="add-contact" aria-hidden="true">
		<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<p><label for="textarea">Add a document</code></label><br>
	 	<input type="file" name="fileToUpload"  class='btn' id="fileToUpload">
 		<input type="text" name='description' value="" placeholder="Document description" required="">	
		<button class="btn btn-primary">Add Document</button>
	</div>
</div>

	</form>



	<!-- Add note Form -->
	<form action="" id='noteAdd-form'>
	<div id="add-contact" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="add-contact" aria-hidden="true">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<p><label for="textarea">Add a note</code></label><br>
		<textarea id="textarea" name='Note'></textarea></p>
		<input type="hidden" value='<?=$_SESSION["Company_ID"]?>' name='comp_id'>
		<input type="hidden" value='<?=time()?>' name='Date_Added'>
		<input type="hidden" value='<?=$bosl->checkId($_GET["id"])?>' name='Client_ID'>
		<ul><li><label class="checkbox"><input type="checkbox" name='client_view' value='1'> Share with client</label></li></ul>

		<button class="btn btn-primary">Add Note</button>
	</div>
</div>
	</form>

		<!-- add folder form -->
	<form action="" id='folderAdd-form'>
	<div id="addForm-modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="add-contact" aria-hidden="true">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<p><label for="textarea">Add a folder</code></label><br>
		<input value='' placeholder="Folder name" name='name'></p>
		<input type="hidden" value='<?=$_SESSION["Company_ID"]?>' name='compid'>
		<input type="hidden" name='type' value='client'>
		<input type="hidden" value='<?=$bosl->checkId($_GET["id"])?>' name='sys_id'>
 
		<button class="btn btn-primary">Add Folder</button>
	</div>
</div>
	</form>



</nav>


<div class="container">

<!-- Next ============================== -->

<section>


<div class="l-wrap">
<div class="l-full-width">
	


	<div class="page-title">
		<h2>Valuations</h2>
		<div class="title-button"><a href="#add-contact" data-toggle="modal" class="btn btn-primary" >Add valuation</a></div>
	</div>
<table class="tablesaw" data-tablesaw-mode="columntoggle" data-tablesaw-sortable data-tablesaw-sortable-switch data-tablesaw-minimap data-tablesaw-mode-switch>
				<thead>
					<tr>
						<th scope="col" data-tablesaw-sortable-col data-tablesaw-sortable-default-col data-tablesaw-priority="persist">Reference</th>
						<th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="6">Product Provider</th>
						<th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="5">Product</th>
						<th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="3">Valuation Date</th>
						<th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="2">Currency</th>
						<th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="1">Value</th>
						<th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4">PDF</th>
					</tr>
				</thead>
				<tbody>
					<?
 			// loop through records
 			foreach ($valu as &$info) {
  			?>
					<tr>
						<td class="title">Test Trust</td>
						<td>Investors Trust</td>
						<td>Access Plus</td>
						<td>1st June 2018</td>
						<td>GBP</td>
						<td>250,000</td>
						<td><a href="#" class="btn btn-small">View</a></td>
					</tr>
 <?
}
?>
				</tbody>
			</table>
		 
 			 
</div><!-- l-summary -->

</div><!-- l-wrap -->

</section><!-- section -->


<script>
	$(function() {

			$('#folderAdd-form').on('submit', function() {

			event.preventDefault();

			dataString = $('#folderAdd-form').serialize();

  			 $.ajax({
			type: "POST",
			url: "<?= $root ?>/api/folder/insert/",
			data: dataString,
			success: function(data) {
				 
					alert('New folder added');
				 	location.reload();
			},
			error: function() {
				alert("There was a problems with your request. Please reload the page and try again ");
			},
			complete: function() {
				 
			}
		});

		});



// add documnts
		$('#docAdd-form').on('submit', function() {

			event.preventDefault();

			dataString = "fileToUpload="+ $('#fileToUpload').val();
			alert(dataString);
  			 $.ajax({
			type: "POST",
			url: "<?= $root ?>/libs/upload.php",
			data: dataString,
			success: function(data) {
				 alert(data);
					alert('New document added');
				 	location.reload();
			},
			error: function() {
				alert("There was a problems with your request. Please reload the page and try again ");
			},
			complete: function() {
				 
			}
		});

		});





		$('#noteAdd-form').on('submit', function() {

			event.preventDefault();

			dataString = $('#noteAdd-form').serialize();
  			 $.ajax({
			type: "POST",
			url: "<?= $root ?>/api/vault/insert/",
			data: dataString,
			success: function(data) {
				 
					alert('New message added');
				 	location.reload();
			},
			error: function() {
				alert("There was a problems with your request. Please reload the page and try again ");
			},
			complete: function() {
				 
			}
		});

		});


	});

// chnge the nav class
$('.nav-section li').removeClass('active');
$('#nav-vault').addClass('active');
</script>