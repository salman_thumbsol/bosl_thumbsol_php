<?
// include controller
include('../controllers/clientDetails.php');
 ?>
<form action="" id='clientDetails-form'>
<h3 id="modal-title">Profile: <?=$details["Firstname"]?> <?=$details["Surname"]?></h3>
	</div>
	<div class="modal-body">

		<div class="tabbable">
		<ul class="nav nav-tabs">
		<li class="active"><a href="#tab1" data-toggle="tab">Details</a></li>
		<li><a href="#tab2" data-toggle="tab">Projects</a></li>
		<!--<li><a href="#tab3" data-toggle="tab">Tasks</a></li>-->
		<li><a href="#tab4" data-toggle="tab">Activity</a></li>
		<li><a href="#tab5" data-toggle="tab">Notes</a></li>
		</ul>

		<div class="tab-content">
		<div class="tab-pane active" id="tab1">
    			<p>Use the form below to update details.</p>
    			<form>

    				<p>Title</p>
    				<p>
    				<select name="Title" id="Title"  style="width:80px" >
    				<? if ($details["Title"]) {?> 
    						<option><?=$details["Title"]?></option>	
<?}?>
    				<option>Mr</option>	
    				<option>Mr</option>
    				<option>Miss</option>
    				<option>Mrs</option>
    				<option>Ms</option>
    				</select>
    				</p>

    			<p><label for="text">First Name</label><br>
    			<input type="text" id="first-name" name="Firstname" value='<?=$details["Firstname"]?>' required></p>

    			<p><label for="text">Surname</label><br>
    			<input type="text" name="Surname" id="surname" value="<?=$details["Surname"]?>" required></p>
				<input type="hidden" name="Client_ID" id="Client_ID" value="<?=$details["Client_ID"]?>" required>
    			<p><label for="email">Email Address</label><br>
    			<input type="email" name="Email" id="email" value='<?=$details["Email"]?>'></p>

    			<p><label for="tel">Telephone Number</label><br>
    			<input type="tel" name="Telephone" id="tel" value="<?=$details["Telephone"]?>"></p>

                    <p><label for="mob">Mobile Number</label><br>
                        <input type="tel" name="Mobile" id="mob" value="<?=$details["Mobile"]?>"></p>
                    <p><label for="Date Of Birth">Date Of Birth</label><br />
                    <div class="dob">

                        <select name="dob_day"  style="width:80px">
                            <option></option> <?
                            for($i = 1; $i <= 31; $i++){
                                $i = str_pad($i, 2, 0, STR_PAD_LEFT);
                                ?> <option <? if ($details['dob_day'] == $i) { ?>selected="selected"<? }?>   value='<?=$i?>'><?=$i?></option>
                            <? } ?>
                        </select>
                        <select name="dob_month"  style="width:120px">';
                        <option></option>
                        <option <? if ($details['dob_month'] == 1) { ?>selected="selected"<? }?>  value="1">January</option>
                           <option <? if ($details['dob_month'] == 2) { ?>selected="selected"<? }?> value="2">February</option>
                            <option <? if ($details['dob_month'] == 3) { ?>selected="selected"<? }?> value="3">March</option>
                            <option <? if ($details['dob_month'] == 4) { ?>selected="selected"<? }?> value="4">April</option>
                            <option <? if ($details['dob_month'] == 5) { ?>selected="selected"<? }?> value="5">May</option>
                            <option <? if ($details['dob_month'] == 6) { ?>selected="selected"<? }?> value="6">June</option>
                            <option <? if ($details['dob_month'] == 7) { ?>selected="selected"<? }?> value="7">July</option>
                            <option<? if ($details['dob_month'] == 8) { ?>selected="selected"<? }?>value="8">August</option>
                            <option <? if ($details['dob_month'] == 9) { ?>selected="selected"<? }?>value="9">September</option>
                            <option <? if ($details['dob_month'] == 10) { ?>selected="selected"<? }?> value="10">October</option>
                            <option <? if ($details['dob_month'] == 11) { ?>selected="selected"<? }?> value="11">November</option>
                            <option <? if ($details['dob_month'] == 12) { ?>selected="selected"<? }?> value="12">December</option>

                        </select>

                        <select name="dob_year"  style="width:80px">
                        <option></option>
                        <? for($i = date('Y'); $i >= date('Y', strtotime('-80 years')); $i--){ ?>
                            echo "<option <? if ($details['dob_year'] == $i) { ?>selected=\"selected\"<? }?> value='<?=$i?>'><?=$i?></option>";
                        <?}?>
                        </select>

                    </div>
    			 
    			<button class="btn btn-primary">Update</button>
    		</div><!-- tab-pane -->
</form>
		<!-- Next -->

		<div class="tab-pane" id="tab2">
            <div class="folder-names">
                <?

                    // loop through folders
                    $i=1;

                    foreach ($folders as &$finfo) {
                        ?>
                        <form action="" id="folder-form<?=$i?>" >
                            <label for="folder-1">Project <?=$i?></label><input id="name" name='name' type="text" class="folder-1" value='<?=$finfo["name"]?>' style='width: 50%'><input id="uf_id" name='uf_id' type="hidden" class="folder-1" value='<?=$finfo["uf_id"]?>'> <button class="btn">Update</button>

                        </form>
                        <?

                        $i++;
                    }

                ?>
            </div>
		</div><!-- tab-pane -->
		<!-- Next -->

		<div class="tab-pane" id="tab3">
		Tasks
		</div><!-- tab-pane -->
		<!-- Next -->

		<div class="tab-pane" id="tab4">
		<p>Login Activity</p>
		</div><!-- tab-pane -->
		<!-- Next -->

		<div class="tab-pane" id="tab5">
            <div class="tab-pane" id="tab2">

                <section>


                    <div class="l-wrap">
                        <div class="l-full-width">
                            <div class="table-wrapper">
                                <div class="tablesaw-overflow">
                                    <table class="tablesaw" data-tablesaw-mode="columntoggle" data-tablesaw-sortable data-tablesaw-sortable-switch data-tablesaw-minimap data-tablesaw-mode-switch>
                                        <thead>
                                        <tr>
                                            <th scope="col" data-tablesaw-sortable-col data-tablesaw-sortable-default-col data-tablesaw-priority="persist">Date Added</th>
                                            <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="2">Author</th>
                                            <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="3">Note</th>

                                        </tr>
                                        </thead>
                                        <tbody>

                                        <?
                                        // loop through records
                                        foreach ($notes as $note) {

                                            ?>

                                            <tr id='list-<?= $note["Note_id"]?>'>
                                                <td class="title"><?= $note["dateadded"]?></td>
                                                <td><?= $note["Author"]?></td>
                                                <td><?= $note["Note"]?></td>
</tr>
<? }?>
                                        </tbody>
                                    </table>
                                </div><!-- /tablesaw-overflow -->

                            </div>
		</div><!-- tab-pane -->
		</div><!-- tab-content -->
		</div><!-- tabbable -->
	</div>
	<div class="modal-footer">
	<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
	<button class="btn btn-primary" type="submit">Save</button>
	<!-- <button class="btn btn-error">Delete</button> -->
	</div>
	</div>

	<script>


        $('#folder-form1').submit(function(event) {

            event.preventDefault();

            dataString = $('#folder-form1').serialize();

            $.ajax({
                type: "POST",
                url: "<?= $root ?>/api/folder/update/",
                data: dataString,
                success: function(data) {
                    if(data==1){
                        alert("Folder Updated");
                        location.reload();
                    }else{
                        alert("There was a problems with your request. Please reload the page and try again ");
                    }
                },
                error: function() {
                    alert("There was a problems with your request. Please reload the page and try again ");
                },
                complete: function() {

                }
            });


            /* Act on the event */
        });





        $('#folder-form2').submit(function(event) {

            event.preventDefault();

            dataString = $('#folder-form2').serialize();

            $.ajax({
                type: "POST",
                url: "<?= $root ?>/api/folder/update/",
                data: dataString,
                success: function(data) {
                    if(data==1){
                        alert("Folder Updated");
                        location.reload();
                    }else{
                        alert("There was a problems with your request. Please reload the page and try again ");
                    }
                },
                error: function() {
                    alert("There was a problems with your request. Please reload the page and try again ");
                },
                complete: function() {

                }
            });


            /* Act on the event */
        });

        $('#folder-form3').submit(function(event) {

            event.preventDefault();

            dataString = $('#folder-form3').serialize();

            $.ajax({
                type: "POST",
                url: "<?= $root ?>/api/folder/update/",
                data: dataString,
                success: function(data) {
                    if(data==1){
                        alert("Folder Updated");
                        location.reload();
                    }else{
                        alert("There was a problems with your request. Please reload the page and try again ");
                    }
                },
                error: function() {
                    alert("There was a problems with your request. Please reload the page and try again ");
                },
                complete: function() {

                }
            });


            /* Act on the event */
        });


        $('#folder-form4').submit(function(event) {

            event.preventDefault();

            dataString = $('#folder-form4').serialize();

            $.ajax({
                type: "POST",
                url: "<?= $root ?>/api/folder/update/",
                data: dataString,
                success: function(data) {

                    if(data==1){
                        alert("Folder Updated");
                        location.reload();
                    }else{
                        alert("There was a problems with your request. Please reload the page and try again ");
                    }
                },
                error: function() {
                    alert("There was a problems with your request. Please reload the page and try again ");
                },
                complete: function() {

                }
            });


            /* Act on the event */
        });


        $('#folder-form5').submit(function(event) {

            event.preventDefault();

            dataString = $('#folder-form5').serialize();

            $.ajax({
                type: "POST",
                url: "<?= $root ?>/api/folder/update/",
                data: dataString,
                success: function(data) {
                    if(data==1){
                        alert("Folder Updated");
                        location.reload();
                    }else{
                        alert("There was a problems with your request. Please reload the page and try again ");
                    }
                },
                error: function() {
                    alert("There was a problems with your request. Please reload the page and try again ");
                },
                complete: function() {

                }
            });


            /* Act on the event */
        });





        $('#clientDetails-form2').submit(function(event) {

event.preventDefault();

dataString = $('#clientDetails-form').serialize();

		 $.ajax({
			type: "POST",
			url: "<?= $root ?>/api/clients/update/",
			data: dataString,
			success: function(data) {
				if(data==1){
					location.reload();
				}else{
					alert("There was a problems with your request. Please reload the page and try again ");	
				}
			},
			error: function() {
				alert("There was a problems with your request. Please reload the page and try again ");
			},
			complete: function() {
				 
			}
		});


		 	/* Act on the event */
});



	$(function () {
   $('.nav-tabs a').on('click',function(e) {
    e.preventDefault();
    var link = $(this).attr('href').replace('#','');
    $('.nav-tabs a').each(function() {
      if($(this).attr('href')==='#'+link) {
        $(this).parent().addClass('active');
      } else {
        $(this).parent().removeClass('active');
      }
    })
    $('.tab-pane').each(function() {
      if($(this).attr('id')===link) {
        $(this).addClass('active');
      } else {
        $(this).removeClass('active');
      }
    })
  })
})
	</script>