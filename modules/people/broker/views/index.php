<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>

<nav id="subnav" role="subnav">
	<div class="subnav-header">
		<ion-icon name="people"> </ion-icon>
	</div>	<div class="subnav-header"> Welcome 
<?=$user['Firstname']?>
	</div>
	<ul>
		<li><a href="#add-contact" data-toggle="modal">Add Contact</a></li>
		<li><a href="?archive=1">View Archived</a></li>
		<li><a href="#search-modal" data-toggle="modal">Search</a></li>
		 
	</ul>
 
	<!-- Add Contact Form -->
	<form action="" id='clientAdd-form'>
	<div id="add-contact" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="add-contact" aria-hidden="true">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			<h3 id="modal-title">Add Contact</h3>
		</div>
	
		<div class="modal-body">
			<p>Use the form below to add a contact.</p>
		

				<p>Title</p>
				<p>
				<select name="Title" id="Title" style="width:80px">
				<option>Mr</option>
				<option>Miss</option>
				<option>Mrs</option>
				<option>Ms</option>
				</select>
				</p>

			<p><label for="text">First Name</label><br>
			<input type="text" id="first-name" name='Firstname'></p>

			<p><label for="text">Surname</label><br>
			<input type="text" id="surname" name='Surname'></p>
			<input type="hidden" value='<?=$_SESSION['Company_ID']?>' id='Company_ID' name='Company_ID'>

			<p><label for="email">Email Address</label><br>
			<input type="email" id="email" name='Email'></p>

			<p><label for="tel">Telephone Number</label><br>
			<input type="tel" id="tel" name='Mobile'></p>

			<p><label for="textarea">Client Notes</code></label><br>
			<textarea id="textarea" name='notes'> </textarea></p>

	
		</div>
		<div class="modal-footer">
		<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
		<button class="btn btn-primary" type="submit">Save</button>
		<!-- <button class="btn btn-error">Delete</button> -->
		</div>
	</div>
	</form>
</nav>


<div class="container">

<!-- Next ============================== -->

<section>


<div class="l-wrap">
<div class="l-full-width">
 <div class="table-wrapper">
 <div class="tablesaw-overflow">
			<table class="tablesaw" data-tablesaw-mode="columntoggle" data-tablesaw-sortable data-tablesaw-sortable-switch data-tablesaw-minimap data-tablesaw-mode-switch>
				<thead>
					<tr>
						<th scope="col" data-tablesaw-sortable-col data-tablesaw-sortable-default-col data-tablesaw-priority="persist">Name</th>
						<th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="2">Email</th>
						<th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="3">Phone</th>
						<th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="0">Contract</th>
						<th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="1">Vault</th>
						<th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="0" class=" tablesaw-priority-0">Archive</th>
					</tr>
				</thead>
				<tbody>

					<?
 					// loop through records
						foreach ($people as $info) {
  
					?>

					<tr id='list-<?= $info["Client_ID"]?>'>
						<td class="title"><a href="#client-details" data-toggle="modal" class="clientdetails" id='<?=$info["Client_ID"]?>'><?= $info["Firstname"]?> <?= $info["Surname"]?></a></td>
						<td><?= $info["Email"]?></td>
						<td><?= $info["Mobile"]?></td>
						<td><a href="#send-access" data-toggle="modal" id='<?= $info["Client_ID"]?>' class="btn btn-primary btn-small sendInvite">Send Access</a></td>
						<td><a href='../../vault/broker/?id=<?= $info["Client_ID"]?>' class='btn btn-small'>Vault</a></td>
						<?
							if($info["hide"] !=1){
						?>
						<td id='<?= $info["Client_ID"]?>' class='archieveme'><a href="#"  class="btn btn-small">Archive</a></td>
						<?
							}else{
						?>
						<td id='<?= $info["Client_ID"]?>' class='restoreme'><a href="#"  class="btn btn-small">Restore</a></td>
						<?
							}
						?>
					</tr>
					
					<?
						 }
					?>
				</tbody>
			</table>
		</div><!-- /tablesaw-overflow -->


			<!-- Search -->
	<div id="search-modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="send-access" aria-hidden="true">
	<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	<h3 id="modal-title" >Search</h3>
	</div>
<form action="" method="_GET">
	<div class="modal-body">
	<input type="text" name='search' value='' placeholder="Search item">
	<select name="field" id="">
		<option value="">Which Field </option>
		<option value='Firstname'>Firstname</option>
		<option value='Surname'>Surname</option>
		<option value='Email'>Email</option>
		<option value='Mobile'>Phone</option>
		<option value='Address'>Address</option>

	</select>
	</div>
	<div class="modal-footer">
	<button class="btn" data-dismiss="modal" aria-hidden="true">cancel</button>
	<button class="btn btn-primary" type="Submit">Search</button>

	</div>
</form>
	</div>

	<!-- Send Access -->
	<div id="send-access" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="send-access" aria-hidden="true">
	<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	<h3 id="modal-title" >Send Access</h3>
	</div>

	<div class="modal-body">
	<div id='sendInvite-modal'></div>
	</div>
	<div class="modal-footer">
	<button class="btn" data-dismiss="modal" aria-hidden="true">No, cancel</button>
	<button class="btn btn-primary">Yes, please send</button>
	</div>
	</div>

	<!-- Client Details modal-->
	<div id="client-details" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="client-details" aria-hidden="true">
	<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<div id='clientDetails-modal'>
	Loading client details
		</div>
</div>

</div><!-- l-summary -->

</div><!-- l-wrap -->

</section><!-- section -->


<script>
	$(function() {
   		// load client details
		$('.clientdetails').on('click', function() {
 			$('#clientDetails-modal').load('views/clientDetails.php?id='+this.id);

		});


		// load send invite
		$('.sendInvite').on('click', function() {
 			$('#sendInvite-modal').load('views/sendInvite.php?id='+this.id);

		});




		// archive cllients
		$('.archieveme').on('click', function() {
			id = this.id
			dataString= "hide=1&Client_ID="+id
 			$.ajax({
				type: "POST",
				url: "<?= $root ?>/api/clients/update/",
				data: dataString,
				success: function(data) {
					 
					 $( "#list-"+id ).hide();
						alert('This contact has been archived. To view them in the future please click "View Archived"');
					 
				},
				error: function() {
					alert("There was a problems with your request. Please reload the page and try again ");
				},
				complete: function() {
					 
				}
			});

		});

		// restore cllients
		$('.restoreme').on('click', function() {
			id = this.id
			dataString= "hide=0&Client_ID="+id
 			$.ajax({
				type: "POST",
				url: "<?= $root ?>/api/clients/update/",
				data: dataString,
				success: function(data) {
					 
					 $( "#list-"+id ).hide();
						alert('This contact has been restored to the main list');
					 
				},
				error: function() {
					alert("There was a problems with your request. Please reload the page and try again ");
				},
				complete: function() {
					 
				}
			});

		});




		$('#clientAdd-form').on('submit', function(event) {

			event.preventDefault();

			dataString = $('#clientAdd-form').serialize();
  			 $.ajax({
			type: "POST",
			url: "<?=$root?>/api/clients/insert/",
			data: dataString,
          

			success: function(data) {
					location.reload();
				 
			},
			error: function() {
				alert("There was a problems with your request. Please reload the page and try again ");
			},
			complete: function() {
				 
			}
		});

		});

 
	});


</script>