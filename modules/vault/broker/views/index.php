<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>

<nav id="subnav" role="subnav">
	<div class="subnav-header">
		<ion-icon name="key"></ion-icon>
	</div>
	<ul>
		<li><a href="?id=<?=$_GET["id"]?>" data-toggle="modal">Notes</a></li>

		<?
			if(isset($_GET["id"])){
		?>
		<li><a href="#add-contact" data-toggle="modal">Add Notes</a></li>
		<?
			}
		?>
		<!--<li><a href="?archive=1">Valutations</a></li>-->

		<!-- FOLDERS ony displayed when there is a client selected-->
		<?
			if(isset($_GET["id"])){

 					// loop through folders
 						foreach ($folders as &$finfo) {
  
					 
		?>
		<li><a href="?id=<?=$_GET["id"]?>&folder=<?=$finfo["uf_id"]?>&fname=<?=$finfo["name"]?>" data-toggle="modal"><?=$finfo["name"]?></a></li>
		<?
			}
		}
		?>
		<!-- <li><a href="#addForm-modal" data-toggle="modal">Add Folder</a></li>-->
	</ul>
 

<!-- Add document Form -->
	<form action="../../../libs/upload.php?folder=<?=$_GET["folder"]?>&id=<?=$_GET["id"]?>"   method="post" enctype="multipart/form-data">
	<div id="add-doc" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="add-contact" aria-hidden="true">
		<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<p><label for="textarea">Add a document</code></label><br>
	 	<input type="file" name="fileToUpload"  class='btn' id="fileToUpload">
 		<input type="text" name='description' value="" placeholder="Document description" required="">	
		<button class="btn btn-primary">Add Document</button>
	</div>
</div>

	</form>



	<!-- Add note Form -->
	<form action="../../../libs/newnote.php?folder=<?=$_GET["folder"]?>&id=<?=$_GET["id"]?>" id='noteAdd-form1' method='post' enctype="multipart/form-data">
	<div id="add-contact" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="add-contact" aria-hidden="true">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<p><label for="textarea">Add a note</code></label><br>
		<textarea id="textarea" name='Note'></textarea></p>
		<input type="hidden" value='<?=$_SESSION["Company_ID"]?>' name='comp_id'>
		<input type="hidden" value='<?=time()?>' name='Date_Added'>
		<input type="hidden" value='<?=$bosl->checkId($_GET["id"])?>' name='Client_ID'>
		<ul><li><label class="checkbox"><input type="checkbox" name='client_view' value='1'> Share with client</label></li></ul>
 		<div class="modal-header">
	 	<input type="file" name="fileToUpload"  class='btn' id="fileToUpload">
  
</div>

	</form>
		<button class="btn btn-primary">Add Note</button>
	</div>
</div>
	</form>

		<!-- add folder form -->
	<form action="" id='folderAdd-form'>
	<div id="addForm-modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="add-contact" aria-hidden="true">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<p><label for="textarea">Add a folder</code></label><br>
		<input value='' placeholder="Folder name" name='name'></p>
		<input type="hidden" value='<?=$_SESSION["Company_ID"]?>' name='compid'>
		<input type="hidden" name='type' value='client'>
		<input type="hidden" value='<?=$bosl->checkId($_GET["id"])?>' name='sys_id'>
 
		<button class="btn btn-primary">Add Folder</button>
	</div>
</div>
	</form>



</nav>


<div class="container">

<!-- Next ============================== -->

<section>


<div class="l-wrap">
<div class="l-full-width">
	


<?
if(!isset($_GET["folder"])){
?>
	<div class="page-title">
		<h2>Notes</h2>
		<?
			if(isset($_GET["id"])){
		?>
		<div class="title-button"><a href="#add-contact" data-toggle="modal" class="btn btn-primary" >Add note</a></div>
		<?
			}
		?>
	</div>

			<?
 			// loop through records
 			foreach ($notes as &$info) {
  			?>
			<div class="note-single" id='list-<?= $info["Client_ID"]?>'>
				<h5>Author: <?= $info["uf"]?> <?= $info["us"]?></h5>
				Client: <?= $info["Firstname"]?> <?= $info["Surname"]?> <br>
				<p><em><?= $bosl->BBOdate($info["Date_Added"])?></em></p>
				<p><?= nl2br($info["Note"])?></p>
		<p><?=($info["File_Name"]) ? "<b>Attachment:</b> <a href='".$root."/checkfile.php?file=".$info["Upload_ID"]."' target='_blank'>". $info["File_Name"]."</a>" : ""?></p>
			</div>
 			<?
			}
			}else{
 			?>
<div class="page-title">
		<h2><div contenteditable="true" id='editme'><?=$_GET["fname"]?>  <a href=''><ion-icon name="create"></ion-icon></a></div><button  style='display:none' class="btn" id="editmeSave">Save</button></h2>
		<div class="title-button"><a href="#add-doc" data-toggle="modal" class="btn btn-primary" >Add document</a></div>
	</div>
	
<table class="tablesaw" data-tablesaw-mode="columntoggle" data-tablesaw-sortable data-tablesaw-sortable-switch data-tablesaw-minimap data-tablesaw-mode-switch>
				<thead>
					<tr>
						<th scope="col" data-tablesaw-sortable-col data-tablesaw-sortable-default-col data-tablesaw-priority="persist">Description</th>
						<th scope="col" data-tablesaw-sortable-col data-tablesaw-sortable-default-col data-tablesaw-priority="persist">Added</th>
						<th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4">View</th>
					</tr>
				</thead>
				<tbody>

					<?
			foreach ($docs  as &$file) {

					?>
					<tr>
						<td class="title"><?=$file["Description"]?></td>
						<td class="title"><?=$bosl->BBOdate($file["datetime"])?></td>
						<td><a href="#" class="btn btn-small">View</a></td>
					</tr>
					<?
					}
					?>
				</tbody>
			</table>



<?

}
?>
</div><!-- l-summary -->

</div><!-- l-wrap -->

</section><!-- section -->


<script>
	$(function() {


$('#editme').on('click', function() {

$('#editmeSave').show();



})
$('#editmeSave').on('click', function() {



event.preventDefault();

			dataString = "name="+$('#editme').text()+ '&uf_id=<?=$_GET["folder"]?>';
   			 $.ajax({
			type: "POST",
			url: "<?= $root ?>/api/folder/update/",
			data: dataString,
			success: function(data) {
				 
					alert('Folder name updated');
					location.href='?id=<?=$_GET["id"]?>&folder=<?=$_GET["folder"]?>&fname='+$('#editme').text();
 			},
			error: function() {
				alert("There was a problems with your request. Please reload the page and try again ");
			},
			complete: function() {
				 
			}
		});

})


			$('#folderAdd-form').on('submit', function() {

			event.preventDefault();

			dataString = $('#folderAdd-form').serialize();

  			 $.ajax({
			type: "POST",
			url: "<?= $root ?>/api/folder/insert/",
			data: dataString,
			success: function(data) {
				 
					alert('New folder added');
				 	location.reload();
			},
			error: function() {
				alert("There was a problems with your request. Please reload the page and try again ");
			},
			complete: function() {
				 
			}
		});

		});



// add documnts
		$('#docAdd-form').on('submit', function() {

			event.preventDefault();

			dataString = "fileToUpload="+ $('#fileToUpload').val();
			alert(dataString);
  			 $.ajax({
			type: "POST",
			url: "<?= $root ?>/libs/upload.php",
			data: dataString,
			success: function(data) {
				 alert(data);
					alert('New document added');
				 	location.reload();
			},
			error: function() {
				alert("There was a problems with your request. Please reload the page and try again ");
			},
			complete: function() {
				 
			}
		});

		});





		$('#noteAdd-form').on('submit', function() {

			event.preventDefault();

			dataString = $('#noteAdd-form').serialize();
  			 $.ajax({
			type: "POST",
			url: "<?= $root ?>/api/vault/insert/",
			data: dataString,
			success: function(data) {
				 
					alert('New message added');
				 	location.reload();
			},
			error: function() {
				alert("There was a problems with your request. Please reload the page and try again ");
			},
			complete: function() {
				 
			}
		});

		});


	});

<?php

	if($_GET["error"]){
		print "alert('".$_GET["error"]."')";
	}
?>


// chnge the nav class
$('.nav-section > ul > li > a').removeClass('active');
$('#nav-vault').addClass('active');
</script>