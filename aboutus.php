<?php  $current_page_name='aboutus'; ?>
<?php require_once ('layouts/header.php') ?>
    <section>
            <div class="row">
                <div class="strapline">
                    <h2 class="">All our Directors have spent most of their working lives in the Financial Services Industry</h2>
                </div>
            </div> 
            <div class="row wearebos">
                <p>Back Office Solutions (BOSL) was incorporated in the Isle of Man in early 2011 to bring together a management team that had been working together for well over a decade in various areas such as broking, fund administration, life company adminstration, asset management and information technology.</p>
                <p>The company purchased the web domain <a href="https://broker-backoffice.com" title="BBO">broker-backoffice.com</a>, associated intellectual property and customer base from the BVI based company Portfolio Builder Holdings Ltd. The roots of the system go back to 1999 but since 2011 it is has been the sole property of BOSL.</p>
                <p>Back Office Solutions Ltd is a company incorporated in Isle of Man under registration number 006549V. Most of the BOSL Directors live in the Isle of Man and work from our offices in Douglas.</p>
               
            </div>
    </section> 
    <section>
            <div class="row aboutus animate">
                <div class="col span-1-of-3">
                    <div class="col span-1-of-3 mugshot">
                        <img src="img/tony.jpg">
                    </div>
                    <div class="col span-2-of-3">
                        <strong>Tony Preece</strong><br>
                        <em>Managing Director</em>
                    </div>
                    <div class="col span-3-of-3 cv-text">
                    <p>Tony was born in Doncaster in South Yorkshire and is also the owner of BOSL. He joined the Financial Services industry in the UK in 1982. Tony has a Business Studies Degree and extensive experience in business management, usually at board level</p>
                    </div>
                </div>
                <div class="col span-1-of-3">
                    <div class="col span-1-of-3 mugshot">
                        <img src="img/steve.jpg">
                    </div>
                    <div class="col span-2-of-3">
                        <strong>Steve Barrett</strong><br>
                        <em>IT Director</em>
                    </div>
                    <div class="col span-3-of-3 cv-text">
                    <p>Steve is also a Yorkshireman. Born in Sheffield and BT trained, Steve worked on computer hardware before switching to coding back in 1999. Steve has worked on the IP of all the BOS systems since their embrionic conception over 20 years ago</p>
                    </div>
                </div>
                <div class="col span-1-of-3">
                    <div class="col span-1-of-3 mugshot">
                        <img src="img/sophy.png">
                    </div>
                    <div class="col span-2-of-3">
                    <strong>Sophy Blakemore</strong><br>
                    <em>Admin Director</em>     
                    </div>
                    <div class="col span-3-of-3 cv-text">
                    <p>Sophy is from the Isle of Man. Born in Peel Sophy started working life at Hansard International in the late 1990's. Sophy has worked with Tony since 1998 and Steve since 1999. She is an administrative professional and also the companys' Data Protection Officer (DPO)</p>
                    </div>
                </div>
                                     
             </div>
            
    </section>
    <section class="aboutus1">
            <button class="director" type="button" onclick="document.getElementById('myImage1').style.display='block'">Tony Preece - Managing Director
            </button>
                <div id="myImage1" class="mugshot1" style="display: none">
                  <img src="img/tony.jpg"><p>Tony was born in Doncaster in South Yorkshire and is also the owner of BOSL. He joined the Financial Services industry in the UK in 1982. Tony has a Business Studies Degree and extensive experience in business management, usually at board level.</p> 
                    <button class="director" type="button" onclick="document.getElementById('myImage1').style.display='none'">Close</button>
                </div>
                
            <button class="director" type="button" onclick="document.getElementById('myImage2').style.display='block'">Steve Barrett - IT Director
            </button>
                <div id="myImage2" class="mugshot1" style="display: none">
                  <img src="img/steve.jpg"><p>Steve is also a Yorkshireman. Born in Sheffield and BT trained, Steve worked on computer hardware before switching to coding back in 1999. Steve has worked on the IP of all the BOS systems since their embrionic conception over 20 years ago.  </p>
                    <button class="director" type="button" onclick="document.getElementById('myImage2').style.display='none'">Close</button>
                </div>
                
            <button class="director" type="button" onclick="document.getElementById('myImage3').style.display='block'">Sophy Blakemore - Admin Director
            </button>
                <div id="myImage3" class="mugshot1" style="display: none">
                  <img src="img/sophy.png"><p>Sophy is from the Isle of Man. Born in Peel Sophy started working life at Hansard International in the late 1990's. Sophy has worked with Tony since 1998 and Steve since 1999. She is an administrative professional and also the companys' Data Protection Officer (DPO).</p>
                    <button class="director" type="button" onclick="document.getElementById('myImage3').style.display='none'">Close</button>
                </div>    
    </section>
    
<?php require_once ('layouts/footer.php') ?>