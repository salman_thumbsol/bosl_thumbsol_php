<!DOCTYPE html>
<html lang="en">
    <head>
        <title>EU GDPR</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Back Office Solutions allows you to securely encrypt sensitive information for delivery to your clients and customers">
        <link rel="stylesheet" type="text/css" href="vendors/css/normalize.css">
        <link rel="stylesheet" type="text/css" href="vendors/css/grid.css">
        <link rel="stylesheet" type="text/css" href="vendors/css/ionicons.min.css">
        <link href="https://unpkg.com/ionicons@4.1.2/dist/css/ionicons.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="vendors/css/animate.css">
        <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,300italic' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" type="text/css" href="resources/css/style.css">
        <link rel="stylesheet" type="text/css" href="resources/css/queries.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        
        
        
    </head>
    <body>
        <header>
            <nav>
                <div class="row">
                    <img src="resources/img/logo-white.png" alt="BOSL logo" class="logo">
                    <img src="resources/img/logo-black.jpg" alt="BOSL logo" class="logo-black">
                        <ul class="main-nav js--main-nav">
                        <li><a href="index.html">Home</a></li>    
                        <li><a href="aboutus.html">About Us</a></li>
                        <li><a href="products.html">Products</a></li>
                        <li><a href="eugdpr.html">EU GDPR</a></li>
                        <li><a href="register.html">Register</a></li>
                        <li><a href="login.html">Login</a></li>
                    </ul>
                    <a class="mobile-nav-icon js--nav-icon"><i class="ion-navicon-round"></i></a>
                </div>
            </nav>
           <div class="row eudpr" id="bbo">
                <!--<div class="aboutus">
                    <h2 class= "white-text">European Union - GDPR</h2>
               </div>-->
               <div class="row">
                <div class="col span-1-of-3">
                    <div class="bbo">EU GDPR Compliance</div>
                    <p class= "white-text">Is required by any organisation that either does business in the EU or collects, processes and stores personal information of EU citizens. The Rules become enforceable from May 2018. The rules are more stringent than preceding legislation and compliance is compulsory, each member state does not have to ratify them into its own law.
                    </p>
                    <br>
                    <p class= "white-text">The Rules are not that prescriptive in terms of the technology controls required, rather stating that appropriate organisational and technological controls must be in place to protect sensitive data. Encryption is of course essential but organisations must understand their business operations and their data movement to best determine which controls, either technical or procedural can deliver the most effective method for ensuring compliance.
                    </p>
                    <br>
                </div>
                <div class="col span-1-of-3">
                    <div class="bbo">Privacy by Design</div>
                    <p class= "white-text">The Rules also introduce the concept of "Privacy by Design". This should ensure that all strategies and controls put in place for ensuring compliance, must take the need for data privacy at the core of the system design.</p>
                    <br>
                    <p class= "white-text">Whilst not an exhaustive list companies need to appoint a Data Protection Officer to ensure that everyone in their organisation understands their responsibilities regarding the use of personal data. They must also and keep a Breach Register to record any breaches of the Rules.
                    </p>
                    <br>
                    <p class= "white-text">The Rules define the difference between Data Processors (system designers and providers of software) and Data Controllers (those that use the data within the systems).
                    </p>
                    <br>
                </div>
                <div class="col span-1-of-3">
                    <div class="bbo">Controllers and Processors</div>
                    <p class= "white-text">The Rules define the difference between Data Processors (system designers and providers of software) and Data Controllers (those that use the data within the systems).
                    </p>
                     <p class= "white-text">Back Office Solutions Ltd (BOSL) is by definition, a Data Processor. The responsibility of BOSL is to ensure that the technical controls are in place such as encryption, firewalls, password security etc and Data Controllers are the individual users within our client companies.
                     </p><br>
                     <p class= "white-text">It is important that each user understands their responsibilites under the Rules to keep client data safe and then act accordingly when using the systems. 
                     </p>
                </div>
            </div>
        </div>
    </header>
    <footer>
        <div class="row">
            <div class="footer">
                <h4>
                        Copyright &copy; 2018 by Back Office Solutions Ltd. All rights reserved.
                </h4>
            </div>
                
        </div>
    </footer>
        
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="//cdn.jsdelivr.net/respond/1.4.2/respond.min.js"></script>
    <script src="//cdn.jsdelivr.net/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="//cdn.jsdelivr.net/selectivizr/1.0.3b/selectivizr.min.js"></script>
    <script src="vendors/js/jquery.waypoints.min.js"></script> 
    <script src="resources/js/script.js"></script>
     <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-61026110-2', 'auto');
      ga('send', 'pageview');

    </script>
    
    
    
</body>
</html>    
    
   
    
    
    
    
     
    
