<?php  $current_page_name='login'; ?>
<? require_once ('action_login.php') ?>
<?php

$header_js_css=<<< HTML
    
    <script>
    function openSubmit() {
        // Get the checkbox
        var submitCheck = document.getElementById("submitCheck");
        // Get the output element
        var submitBtn = document.getElementById("submitBtn");
    
        // If the checkbox is checked, display the output checkBox
        if (submitCheck.checked == true){
            submitBtn.style.display = "block";
        } else {
            submitBtn.style.display = "none";
        }
    }
    </script>

HTML;
?>
<? require_once ('layouts/header.php') ?>
        <section class="row log-in">
                <div class="col span-1-of-4 box">
                    <i class="icon ion-ios-key icon-big black-bgrd"></i><br>
                    <h3>Login to open your BOS Vault</h3>
                </div>
              <form class="animate" method="post" action="/login.php" style="clear: both">
                  <?php if($error!=''){?>
                      <p><?=$error?></p>
                  <?php } ?>
                <p><label for="email-primary">Email Address</label><br>
                    <input type="email" id="email" name="email" placeholder="Email Address" required></p>
                
                <p><label for="password">Password</label><br>
                    <input type="password" placeholder="Enter Password" name="password" required></p>
                    
                <p><input type="checkbox" id="submitCheck" onclick="openSubmit()">  I confirm that I have read and agree to the Terms and Conditions, Cookie Policy and Privacy Policy.</p>
                <br>
                <button class= "submit-btn hide" id="submitBtn" type="submit">Login</button>
              </form><br>

                <div class="row" style="background-color:#f1f1f1">
                  <button class="submit-btn" onclick="document.getElementById().style.display='none'" >Forgot Password</button>
                </div>

        </section>





<? require_once ('layouts/footer.php') ?>