<?
if(!isset($_SESSION)){session_start();}
require_once('layouts/header.php'); ?>
<div class="row">
        <section id="verify">

            <div class="col span-1-of-5">
            </div>
            
            <div class="row col span-3-of-5">
                <p >Congratulations <?= isset($_SESSION['__register_first_name'])?$_SESSION['__register_first_name']:''?> you have successfully registered your profile! You will shortly receive an encrypted email link to login for the first time.</p> <p>If you dont receive it please check your junk folder, it may have found its way there. If so, please add the address to your safe senders list.</p>
                <p><a href="index.php">Click to close and return to the home page</a></p>
            </div>

        </section>
</div>
<? require_once('layouts/footer.php'); ?>




