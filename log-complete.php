<?php

$meta_title='BOSL Login';
$header_js_css=<<< HTML
        <link rel="stylesheet" type="text/css" href="css/vault.css">

        <script>
            function verifyText(){
                document.getElementById('verify').style.display='block';
            }
            function logIn() {
                document.getElementById('door').style.display='block';
            }
        </script>

HTML;
require_once ('layouts/header.php');

$items=['glass','boat','bicycle','flower','calculator','beer'];
$random= array_rand($items,1);
$rand_item=$items[$random];
?>
        <section class="row animate">
            <div class="col span-1-of-5">
            </div>
            <div class="col span-3-of-5">
                <a class="col span-1-of-6 verify-icon" <?=$rand_item=='glass'?'onclick="logIn()"':''?> >
                    <i class="icon ion-ios-glasses icon-big black-bgrd"></i></a>
                
                <a class="col span-1-of-6 verify-icon" <?=$rand_item=='boat'?'onclick="logIn()"':''?>>
                    <i class="icon ion-ios-boat icon-big black-bgrd"></i></a>
                
                <a class="col span-1-of-6 verify-icon" <?=$rand_item=='bicycle'?'onclick="logIn()"':''?>>
                    <i class="icon ion-ios-bicycle icon-big black-bgrd"></i></a>
                
                <a class="col span-1-of-6 verify-icon" <?=$rand_item=='flower'?'onclick="logIn()"':''?>>
                    <i class="icon ion-ios-flower icon-big black-bgrd"></i></a>
                
                <a class="col span-1-of-6 verify-icon" <?=$rand_item=='calculator'?'onclick="logIn()"':''?>>
                    <i class="icon ion-ios-calculator icon-big black-bgrd"></i></a>
                <a class="col span-1-of-6 verify-icon" <?=$rand_item=='beer'?'onclick="logIn()"':''?>>
                    <i class="icon ion-ios-beer icon-big black-bgrd"></i></a>
            </div>
        </section>
        <section class="row">
            <div class="col span-1-of-5">
            </div>
            <div class="row col span-3-of-5">
                <h3 style="text-align: center" >To open your vault please select the <?=$rand_item?>
                </h3>
            </div>
        </section>
        
        <section class="row">
            <div class="col span-1-of-3">
            </div>
        <div class ="col span-1-of-3" id="door">
            <img class="animate" src="img/vault-door.png" id="vault-door">
        </div>
        
        </section>


    </body>

</html>



